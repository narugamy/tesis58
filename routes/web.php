<?php
	/*
	|--------------------------------------------------------------------------
	| Web Routes
	|--------------------------------------------------------------------------
	|
	| Here is where you can register web routes for your application. These
	| routes are loaded by the RouteServiceProvider within a group which
	| contains the "web" middleware group. Now create something great!
	|
	*/
	Route::get('/', function () {
		return view('welcome');
	});
	Route::namespace('Admin')->prefix('admin')->group(function () {
		Route::namespace('Auth')->group(function () {
			Route::middleware('guest:admin')->group(function () {
				Route::get('login', 'LoginController@showLoginForm')->name('admin.login');
				Route::post('login', 'LoginController@login');
			});
			Route::middleware('auth:admin')->group(function () {
				Route::get('logout', 'LoginController@destroy')->name('admin.logout');
			});
		});
		Route::middleware('auth:admin')->group(function () {
			Route::namespace('Home')->group(function () {
				Route::get('/', 'HomeController@index')->name('admin.index');
			});
			Route::namespace('User')->prefix('usuario')->group(function () {
				Route::get('/', 'UserController@index')->name('admin.user.index');
				Route::get('table', 'UserController@table')->name('admin.user.table');
				Route::get('create', 'UserController@create')->name('admin.user.create');
				Route::post('create', 'UserController@store');
				Route::get('{user}', 'UserController@show')->name('admin.user.update');
				Route::put('{user}', 'UserController@update');
				Route::get('{user}/delete', 'UserController@delete')->name('admin.user.delete');
				Route::get('{user}/destroy', 'UserController@destroy')->name('admin.user.destroy');
			});
			Route::namespace('Resource')->prefix('recurso')->group(function () {
				Route::get('/', 'ResourceController@index')->name('admin.resource.index');
				Route::get('table', 'ResourceController@table')->name('admin.resource.table');
				Route::get('create', 'ResourceController@create')->name('admin.resource.create');
				Route::post('create', 'ResourceController@store');
				Route::get('{resource}', 'ResourceController@show')->name('admin.resource.update');
				Route::put('{resource}', 'ResourceController@update');
				Route::get('{resource}/delete', 'ResourceController@delete')->name('admin.resource.delete');
				Route::get('{resource}/destroy', 'ResourceController@destroy')->name('admin.resource.destroy');
			});
			Route::namespace('Company')->prefix('empresa')->group(function () {
				Route::get('/', 'CompanyController@index')->name('admin.company.index');
				Route::get('table', 'CompanyController@table')->name('admin.company.table');
				Route::get('create', 'CompanyController@create')->name('admin.company.create');
				Route::post('create', 'CompanyController@store');
				Route::get('{company}', 'CompanyController@show')->name('admin.company.update');
				Route::put('{company}', 'CompanyController@update');
				Route::get('{company}/delete', 'CompanyController@delete')->name('admin.company.delete');
				Route::get('{company}/destroy', 'CompanyController@destroy')->name('admin.company.destroy');
			});
			Route::namespace('Role')->prefix('rol')->group(function () {
				Route::get('/', 'RoleController@index')->name('admin.role.index');
				Route::get('table', 'RoleController@table')->name('admin.role.table');
				Route::get('create', 'RoleController@create')->name('admin.role.create');
				Route::post('create', 'RoleController@store');
				Route::get('{role}', 'RoleController@show')->name('admin.role.update');
				Route::put('{role}', 'RoleController@update');
				Route::get('{role}/delete', 'RoleController@delete')->name('admin.role.delete');
				Route::get('{role}/destroy', 'RoleController@destroy')->name('admin.role.destroy');
			});
			Route::namespace('Config')->prefix('configuracion')->group(function () {
				Route::get('/', 'ConfigController@index')->name('admin.config.index');
				Route::get('table', 'ConfigController@table')->name('admin.config.table');
				Route::get('create', 'ConfigController@create')->name('admin.config.create');
				Route::post('create', 'ConfigController@store');
				Route::get('{config}', 'ConfigController@show')->name('admin.config.update');
				Route::put('{config}', 'ConfigController@update');
				Route::get('{config}/delete', 'ConfigController@delete')->name('admin.config.delete');
				Route::get('{config}/destroy', 'ConfigController@destroy')->name('admin.config.destroy');
			});
		});
	});
	Route::namespace('Manager')->prefix('jefe')->group(function () {
		Route::namespace('Auth')->group(function () {
			Route::middleware('guest:manager')->group(function () {
				Route::get('login', 'LoginController@showLoginForm')->name('manager.login');
				Route::post('login', 'LoginController@login');
			});
			Route::middleware('auth:manager')->group(function () {
				Route::get('logout', 'LoginController@destroy')->name('manager.logout');
			});
		});
		Route::middleware('auth:manager')->group(function () {
			Route::namespace('Home')->group(function () {
				Route::get('/', 'HomeController@index')->name('manager.index');
				Route::get('notification', 'HomeController@notification')->name('manager.notification');
			});
			Route::namespace('History')->prefix('historia')->group(function () {
				Route::get('/', 'HistoryController@index')->name('manager.history.index');
				Route::get('table', 'HistoryController@table')->name('manager.history.table');
				Route::get('{project}', 'HistoryController@show')->name('manager.history.update');
			});
			Route::namespace('Project')->prefix('proyecto')->group(function () {
				Route::get('/', 'ProjectController@index')->name('manager.project.index');
				Route::get('{project}/cronograma', 'ProjectController@gant')->name('manager.project.gant.index');
				Route::get('{project}/schedule', 'ProjectController@schedule')->name('manager.project.schedule.index');
				Route::get('{project}/cost', 'ProjectController@cost')->name('manager.project.cost.index');
				Route::get('{project}/recursos', 'ProjectController@resources')->name('manager.project.resource.index');
				Route::get('table', 'ProjectController@table')->name('manager.project.table');
				Route::get('create', 'ProjectController@create')->name('manager.project.create');
				Route::post('create', 'ProjectController@store');
				Route::get('{project}', 'ProjectController@show')->name('manager.project.update');
				Route::put('{project}', 'ProjectController@update');
				Route::get('{project}/finish', 'ProjectController@finish')->name('manager.project.finish');
				Route::get('{project}/delete', 'ProjectController@delete')->name('manager.project.delete');
				Route::get('{project}/destroy', 'ProjectController@destroy')->name('manager.project.destroy');
			});
			Route::namespace('Phase')->prefix('proyecto/{project}/fase')->group(function () {
				Route::get('/', 'PhaseController@index')->name('manager.phase.index');
				Route::get('table', 'PhaseController@table')->name('manager.phase.table');
				Route::get('create', 'PhaseController@create')->name('manager.phase.create');
				Route::post('create', 'PhaseController@store');
				Route::get('{phase}', 'PhaseController@show')->name('manager.phase.update');
				Route::put('{phase}', 'PhaseController@update');
				Route::get('{phase}/delete', 'PhaseController@delete')->name('manager.phase.delete');
				Route::get('{phase}/destroy', 'PhaseController@destroy')->name('manager.phase.destroy');
			});
			Route::namespace('Activity')->prefix('activity/{activity}/mensaje')->group(function () {
				Route::get('/', 'MessageController@index')->name('manager.activity.message.index');
				Route::get('create', 'MessageController@create')->name('manager.activity.message.create');
				Route::post('create', 'MessageController@store');
				Route::get('{message}', 'MessageController@show')->name('manager.activity.message.update');
				Route::put('{message}', 'MessageController@update');
				Route::get('{message}/delete', 'MessageController@delete')->name('manager.activity.message.delete');
				Route::get('{message}/destroy', 'MessageController@destroy')->name('manager.activity.message.destroy');
			});
			Route::namespace('Activity')->prefix('fase/{phase}/actividad')->group(function () {
				Route::get('/', 'ActivityController@index')->name('manager.activity.index');
				Route::get('table', 'ActivityController@table')->name('manager.activity.table');
				Route::get('create', 'ActivityController@create')->name('manager.activity.create');
				Route::post('create', 'ActivityController@store');
				Route::get('{activity}', 'ActivityController@show')->name('manager.activity.update');
				Route::put('{activity}', 'ActivityController@update');
				Route::get('{activity}/delete', 'ActivityController@delete')->name('manager.activity.delete');
				Route::get('{activity}/destroy', 'ActivityController@destroy')->name('manager.activity.destroy');
				Route::get('{activity}/activate', 'ActivityController@activate')->name('manager.activity.activate');
			});
			Route::namespace('Task')->prefix('actividad/{activity}/tarea')->group(function () {
				Route::get('/', 'TaskController@index')->name('manager.task.index');
				Route::get('table', 'TaskController@table')->name('manager.task.table');
				Route::get('create', 'TaskController@create')->name('manager.task.create');
				Route::post('create', 'TaskController@store');
				Route::get('{task}', 'TaskController@show')->name('manager.task.update');
				Route::put('{task}', 'TaskController@update');
				Route::get('{task}/updated', 'TaskController@updated');
				Route::get('{task}/delete', 'TaskController@delete')->name('manager.task.delete');
				Route::get('{task}/destroy', 'TaskController@destroy')->name('manager.task.destroy');
			});
			Route::namespace('Task')->prefix('tarea/{task}/historia')->group(function () {
				Route::get('create', 'HistoryController@create')->name('manager.task.history.create');
				Route::post('create', 'HistoryController@store');
			});
			Route::namespace('Task')->prefix('tarea/{task}/mensaje')->group(function () {
				Route::get('/', 'MessageController@index')->name('manager.task.message.index');
				Route::get('create', 'MessageController@create')->name('manager.task.message.create');
				Route::post('create', 'MessageController@store');
				Route::get('{message}', 'MessageController@show')->name('manager.task.message.update');
				Route::put('{message}', 'MessageController@update');
				Route::get('{message}/delete', 'MessageController@delete')->name('manager.task.message.delete');
				Route::get('{message}/destroy', 'MessageController@destroy')->name('manager.task.message.destroy');
			});
			Route::namespace('Task')->prefix('tarea/{task}/recurso')->group(function () {
				Route::get('/', 'ResourceController@index')->name('manager.task.resource.index');
				Route::get('table', 'ResourceController@table')->name('manager.task.resource.table');
				Route::get('create', 'ResourceController@create')->name('manager.task.resource.create');
				Route::post('create', 'ResourceController@store');
				Route::get('{resource}', 'ResourceController@show')->name('manager.task.resource.update');
				Route::put('{resource}', 'ResourceController@update');
				Route::get('{resource}/delete', 'ResourceController@delete')->name('manager.task.resource.delete');
				Route::get('{resource}/destroy', 'ResourceController@destroy')->name('manager.task.resource.destroy');
			});
			Route::namespace('Task')->prefix('tarea/{task}/progresos')->group(function () {
				Route::get('/', 'ProgressController@index')->name('manager.task.progress.index');
				Route::get('table', 'ProgressController@table')->name('manager.task.progress.table');
				Route::get('{approved}/approved', 'ProgressController@store')->name('manager.task.progress.approved');
			});
		});
	});
	Route::namespace('Developer')->prefix('developer')->group(function () {
		Route::namespace('Auth')->group(function () {
			Route::middleware('guest:developer')->group(function () {
				Route::get('login', 'LoginController@showLoginForm')->name('developer.login');
				Route::post('login', 'LoginController@login');
			});
			Route::middleware('auth:developer')->group(function () {
				Route::get('logout', 'LoginController@destroy')->name('developer.logout');
			});
		});
		Route::middleware('auth:developer')->group(function () {
			Route::namespace('Home')->group(function () {
				Route::get('/', 'HomeController@index')->name('developer.index');
				Route::get('notification', 'HomeController@notification')->name('developer.notification');
			});
			Route::namespace('Activity')->prefix('actividad')->group(function () {
				Route::get('/', 'ActivityController@index')->name('developer.activity.index');
				Route::get('table', 'ActivityController@table')->name('developer.activity.table');
				Route::get('create', 'ActivityController@create')->name('developer.activity.create');
				Route::post('create', 'ActivityController@store');
				Route::get('{activity}', 'ActivityController@show')->name('developer.activity.update');
				Route::put('{activity}', 'ActivityController@update');
				Route::get('{activity}/delete', 'ActivityController@delete')->name('developer.activity.delete');
				Route::get('{activity}/destroy', 'ActivityController@destroy')->name('developer.activity.destroy');
			});
			Route::namespace('Task')->prefix('actividad/{activity}/tarea')->group(function () {
				Route::get('/', 'TaskController@index')->name('developer.task.index');
				Route::get('table', 'TaskController@table')->name('developer.task.table');
				Route::get('create', 'TaskController@create')->name('developer.task.create');
				Route::post('create', 'TaskController@store');
				Route::get('{task}', 'TaskController@show')->name('developer.task.update');
				Route::put('{task}', 'TaskController@update');
				Route::get('{task}/assign', 'TaskController@assign')->name('developer.task.assign');
				Route::put('{task}/assign', 'TaskController@assign_update');
				Route::get('{task}/delete', 'TaskController@delete')->name('developer.task.delete');
				Route::get('{task}/destroy', 'TaskController@destroy')->name('developer.task.destroy');
			});
			Route::namespace('My_Tasks')->prefix('mis_tareas')->group(function () {
				Route::get('/', 'TaskController@index')->name('developer.my_tasks.index');
				Route::get('table', 'TaskController@table')->name('developer.my_tasks.table');
				Route::get('create', 'TaskController@create')->name('developer.my_tasks.create');
				Route::post('create', 'TaskController@store');
				Route::get('{my_tasks}', 'TaskController@show')->name('developer.my_tasks.update');
				Route::put('{my_tasks}', 'TaskController@update');
				Route::get('{my_tasks}/delete', 'TaskController@delete')->name('developer.my_tasks.delete');
				Route::get('{my_tasks}/destroy', 'TaskController@destroy')->name('developer.my_tasks.destroy');
			});
			Route::namespace('My_Tasks')->prefix('mis_tareas/{my_tasks}/mensaje')->group(function () {
				Route::get('/', 'MessageController@index')->name('developer.my_tasks.message.index');
				Route::get('create', 'MessageController@create')->name('developer.my_tasks.message.create');
				Route::post('create', 'MessageController@store');
				Route::get('{message}', 'MessageController@show')->name('developer.my_tasks.message.update');
				Route::put('{message}', 'MessageController@update');
				Route::get('{message}/delete', 'MessageController@delete')->name('developer.my_tasks.message.delete');
				Route::get('{message}/destroy', 'MessageController@destroy')->name('developer.my_tasks.message.destroy');
			});
			Route::namespace('My_Tasks')->prefix('mis_tareas/{my_tasks}/progreso')->group(function () {
				Route::get('create', 'ProgressController@create')->name('developer.my_tasks.progress.create');
				Route::post('create', 'ProgressController@store');
			});
		});
	});
	Route::namespace('Client')->prefix('cliente')->group(function () {
		Route::namespace('Auth')->group(function () {
			Route::middleware('guest:client')->group(function () {
				Route::get('login', 'LoginController@showLoginForm')->name('client.login');
				Route::post('login', 'LoginController@login');
			});
			Route::middleware('auth:client')->group(function () {
				Route::get('logout', 'LoginController@destroy')->name('client.logout');
			});
		});
		Route::middleware('auth:client')->group(function () {
			Route::namespace('Home')->group(function () {
				Route::get('/', 'HomeController@index')->name('client.index');
				Route::get('notification', 'HomeController@notification')->name('client.notification');
			});
			Route::namespace('Project')->prefix('proyecto')->group(function () {
				Route::get('/', 'ProjectController@index')->name('client.project.index');
				Route::get('table', 'ProjectController@table')->name('client.project.table');
				Route::get('{project}', 'ProjectController@show')->name('client.project.update');
				Route::get('{project}/cronograma', 'ProjectController@gant')->name('client.project.gant.index');
			});
			Route::namespace('Phase')->prefix('proyecto/{project}/fase')->group(function () {
				Route::get('/', 'PhaseController@index')->name('client.phase.index');
				Route::get('table', 'PhaseController@table')->name('client.phase.table');
				Route::get('{phase}', 'PhaseController@show')->name('client.phase.update');
			});
			Route::namespace('Activity')->prefix('fase/{phase}/activity')->group(function () {
				Route::get('/', 'ActivityController@index')->name('client.activity.index');
				Route::get('table', 'ActivityController@table')->name('client.activity.table');
				Route::get('{activity}', 'ActivityController@show')->name('client.activity.update');
			});
			Route::namespace('Activity')->prefix('activity/{activity}/mensaje')->group(function () {
				Route::get('/', 'MessageController@index')->name('client.activity.message.index');
				Route::get('create', 'MessageController@create')->name('client.activity.message.create');
				Route::post('create', 'MessageController@store');
				Route::get('{message}', 'MessageController@show')->name('client.activity.message.update');
				Route::put('{message}', 'MessageController@update');
				Route::get('{message}/delete', 'MessageController@delete')->name('client.activity.message.delete');
				Route::get('{message}/destroy', 'MessageController@destroy')->name('client.activity.message.destroy');
			});
		});
	});
