-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 13-07-2019 a las 04:06:33
-- Versión del servidor: 5.7.26-0ubuntu0.18.04.1
-- Versión de PHP: 7.2.19-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tesis`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `activity`
--

CREATE TABLE `activity` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `activity_id` bigint(20) UNSIGNED DEFAULT NULL,
  `father_id` bigint(20) UNSIGNED DEFAULT NULL,
  `phase_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `structure` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `start` date NOT NULL,
  `end` date NOT NULL,
  `start_new` date NOT NULL,
  `end_new` date NOT NULL,
  `approved` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `activity`
--

INSERT INTO `activity` (`id`, `activity_id`, `father_id`, `phase_id`, `user_id`, `structure`, `code`, `name`, `description`, `start`, `end`, `start_new`, `end_new`, `approved`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, NULL, NULL, 1, 4, '1', 'A000001', 'Actividad I', 'Actividad I', '2019-05-01', '2019-05-08', '2019-05-01', '2019-05-07', '2019-05-07', '2019-04-22 14:15:00', '2019-05-07 23:00:00', NULL),
(2, 1, 1, 1, 5, '1.1', 'A000002', 'Actividad II', 'Actividad II', '2019-05-09', '2019-05-14', '2019-05-08', '2019-05-10', '2019-05-10', '2019-04-22 14:25:00', '2019-05-13 23:00:00', NULL),
(3, 2, 1, 1, 4, '1.1.1', 'A000003', 'Actividad III', 'Actividad III', '2019-05-15', '2019-05-21', '2019-05-14', '2019-05-20', '2019-05-20', '2019-04-22 15:00:00', '2019-05-20 23:00:00', NULL),
(4, 3, 1, 1, 5, '1.1.1.1', 'A000004', 'Actividad IV', 'Actividad IV', '2019-05-22', '2019-05-27', '2019-05-21', '2019-05-24', '2019-05-24', '2019-04-22 15:10:00', '2019-05-22 23:00:00', NULL),
(7, NULL, NULL, 3, 5, '2', 'A000005', 'Actividad 1', 'Actividad 1', '2019-07-26', '2019-08-09', '2019-07-26', '2019-08-09', NULL, '2019-07-12 22:48:35', '2019-07-12 22:49:22', NULL),
(8, NULL, NULL, 3, 5, '3', 'A000006', 'Actividad 2', 'Actividad 2', '2019-08-12', '2019-08-26', '2019-08-12', '2019-08-26', NULL, '2019-07-12 22:49:45', '2019-07-12 22:49:45', NULL),
(9, NULL, NULL, 3, 5, '4', 'A000007', 'Actividad 3', 'Actividad 3', '2019-08-27', '2019-09-26', '2019-08-27', '2019-09-26', NULL, '2019-07-12 22:50:13', '2019-07-12 22:50:13', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `archive`
--

CREATE TABLE `archive` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `route` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `encrypt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `archive`
--

INSERT INTO `archive` (`id`, `name`, `route`, `encrypt`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Practicas.rar', 'archive/message/1560730845Practicas.rar', '1560730845Practicas.rar', '2019-06-17 00:20:46', '2019-06-17 00:20:46', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `company`
--

CREATE TABLE `company` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `company`
--

INSERT INTO `company` (`id`, `name`, `slug`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Mood', 'mood', '2019-03-10 22:23:18', '2019-03-12 05:46:12', NULL),
(2, 'Ishop', 'ishop', '2019-03-12 06:57:05', '2019-06-08 09:05:08', NULL),
(3, 'Madam Tusan', 'madam-tusan', '2019-07-12 22:37:19', '2019-07-12 22:37:19', NULL),
(4, 'Chilis', 'chilis', '2019-07-12 22:37:38', '2019-07-12 22:37:38', NULL),
(5, 'PuntaSal', 'puntasal', '2019-07-12 22:38:07', '2019-07-12 22:38:07', NULL),
(6, 'Brenta', 'brenta', '2019-07-12 22:38:17', '2019-07-12 22:38:17', NULL),
(7, 'Jeep', 'jeep', '2019-07-12 22:38:46', '2019-07-12 22:38:46', NULL),
(8, 'Dive Motor', 'dive-motor', '2019-07-12 22:39:04', '2019-07-12 22:39:04', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `config`
--

CREATE TABLE `config` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `config`
--

INSERT INTO `config` (`id`, `name`, `key`, `value`, `slug`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Dias antes de finalizar requerimientos', 'end_date_first', '3', 'end_date_first', '2019-03-10 05:00:00', '2019-03-25 08:29:53', NULL),
(2, 'Dias despues de finalizar requerimientos', 'end_date_last', '0', 'end_date_last', '2019-03-10 05:00:00', '2019-03-10 05:00:00', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `container`
--

CREATE TABLE `container` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `task_id` bigint(20) UNSIGNED NOT NULL,
  `resource_id` bigint(20) UNSIGNED NOT NULL,
  `number` int(11) NOT NULL,
  `price` decimal(12,2) NOT NULL,
  `total` decimal(12,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `container`
--

INSERT INTO `container` (`id`, `task_id`, `resource_id`, `number`, `price`, `total`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 2, 5, '1.00', '5.00', '2019-04-23 03:32:14', '2019-04-23 03:32:14', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `history`
--

CREATE TABLE `history` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `task_id` bigint(20) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `history`
--

INSERT INTO `history` (`id`, `task_id`, `code`, `name`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 7, 'H000001', 'Modulo de Pasarela', 'Implementacion de API', '2019-05-08 15:00:00', '2019-05-08 15:00:00', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `message`
--

CREATE TABLE `message` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `archive_id` bigint(20) UNSIGNED DEFAULT NULL,
  `employee_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `table_id` bigint(20) UNSIGNED NOT NULL,
  `table_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `message`
--

INSERT INTO `message` (`id`, `archive_id`, `employee_id`, `user_id`, `table_id`, `table_type`, `name`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, NULL, 3, 2, 4, 'activity', 'Mensaje', 'Mensaje', '2019-06-12 04:53:35', '2019-06-12 04:53:35', NULL),
(4, NULL, 2, 3, 4, 'activity', 'Mensaje 2', 'Mensaje 2', '2019-06-17 00:03:45', '2019-06-17 00:03:45', NULL),
(5, NULL, 3, 2, 4, 'activity', 'Mensaje 3', 'Mensaje 3', '2019-06-17 00:17:47', '2019-06-17 00:17:47', NULL),
(6, 1, 3, 2, 4, 'activity', 'Mensaje 3', 'Mensaje 3', '2019-06-17 00:20:45', '2019-06-17 00:20:46', NULL),
(7, NULL, 3, 4, 19, 'task', 'Mensaje', 'Mensaje', '2019-06-17 00:28:52', '2019-06-17 00:28:52', NULL),
(8, NULL, 4, 3, 19, 'task', 'Mensaje Recivido', 'Mensaje Recivido', '2019-06-17 00:31:10', '2019-06-17 00:31:10', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_company_table', 1),
(2, '2014_10_12_000000_create_role_table', 1),
(3, '2014_10_12_000000_create_user_table', 1),
(4, '2014_10_12_100000_create_password_resets_table', 1),
(5, '2019_03_10_230211_create_config_table', 2),
(6, '2019_03_11_015049_create_type_table', 3),
(7, '2019_03_11_023708_create_project_table', 4),
(8, '2019_03_15_022117_create_resource_table', 5),
(9, '2019_03_16_174916_create_phase_table', 5),
(10, '2019_03_16_175314_create_activity_table', 5),
(11, '2019_03_16_175415_create_task_table', 5),
(12, '2019_03_23_222830_create_archive_table', 6),
(13, '2019_03_24_232013_create_message_table', 6),
(14, '2019_03_24_232348_create_notification_table', 6),
(15, '2019_03_25_025211_create_container_table', 6),
(16, '2019_03_31_124348_create_progress_table', 7),
(17, '2019_05_11_191229_create_history_table', 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notification`
--

CREATE TABLE `notification` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `message_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `route` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `notification`
--

INSERT INTO `notification` (`id`, `message_id`, `user_id`, `route`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 3, 3, 'http://tesis58.loc/jefe/activity/A000001/mensaje', '2019-06-12 04:53:35', '2019-06-12 04:53:35', '2019-06-12 04:53:35'),
(2, 4, 2, 'http://tesis58.loc/cliente/activity/A000004/mensaje', '2019-06-17 00:03:46', '2019-06-17 00:17:29', '2019-06-17 00:17:29'),
(3, 5, 3, 'http://tesis58.loc/jefe/activity/A000004/mensaje', '2019-06-17 00:17:47', '2019-06-17 00:19:37', '2019-06-17 00:19:37'),
(4, 6, 3, 'http://tesis58.loc/jefe/activity/A000004/mensaje', '2019-06-17 00:20:45', '2019-06-17 00:21:09', '2019-06-17 00:21:09'),
(5, 7, 3, 'http://tesis58.loc/jefe/tarea/T000019/mensaje', '2019-06-17 00:28:52', '2019-06-17 00:29:05', '2019-06-17 00:29:05'),
(6, 8, 4, 'http://tesis58.loc/developer/mis_tareas/T000019/mensaje', '2019-06-17 00:31:10', '2019-06-17 00:31:19', '2019-06-17 00:31:19');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `phase`
--

CREATE TABLE `phase` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `project_id` bigint(20) UNSIGNED NOT NULL,
  `code` varchar(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `start` date NOT NULL,
  `end` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `phase`
--

INSERT INTO `phase` (`id`, `project_id`, `code`, `name`, `description`, `start`, `end`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'F000001', 'Fase de Desarrollo', 'Fase de Desarrollo', '2019-05-01', '2019-05-27', '2019-04-22 14:10:00', '2019-04-22 14:10:00', NULL),
(3, 4, 'F000002', 'Fase I', 'Fase I', '2019-07-26', '2019-09-26', '2019-07-12 22:47:39', '2019-07-12 22:47:39', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `progress`
--

CREATE TABLE `progress` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `task_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `progress` decimal(5,2) NOT NULL,
  `approved` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `progress`
--

INSERT INTO `progress` (`id`, `task_id`, `user_id`, `name`, `description`, `progress`, `approved`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 4, 'Progreso 100%', 'Progreso 100%', '100.00', '2019-05-02', '2019-05-02 05:00:00', '2019-05-02 05:00:00', NULL),
(2, 2, 5, 'Tarea Terminada', 'Tarea Terminada', '100.00', '2019-05-01', '2019-05-01 23:00:00', '2019-05-01 23:00:00', NULL),
(3, 3, 4, 'Tarea Finalizada', 'Tarea Finalizada', '100.00', '2019-05-06', '2019-05-06 21:00:00', '2019-05-06 21:00:00', NULL),
(4, 4, 5, 'Tarea Realizada', 'Tarea Realizada', '100.00', '2019-05-01', '2019-05-01 23:00:00', '2019-05-01 23:00:00', NULL),
(5, 5, 5, 'Tarea Terminada', 'Tarea Terminada', '100.00', '2019-05-08', '2019-05-08 20:00:00', '2019-05-08 20:00:00', NULL),
(6, 6, 5, 'Tarea finalizada', 'Tarea finalizada', '100.00', '2019-05-09', '2019-05-09 21:17:42', '2019-05-09 21:17:42', NULL),
(7, 7, 4, 'Finalizado', 'Finalizado', '100.00', '2019-05-09', '2019-05-09 20:00:00', '2019-05-09 20:00:00', NULL),
(8, 8, 6, 'Tarea Finalizada', 'Tarea Finalizada', '100.00', '2019-05-08', '2019-05-08 21:00:00', '2019-05-08 21:00:00', NULL),
(9, 9, 6, 'Finalizado', 'Finalizado', '100.00', '2019-05-10', '2019-05-10 19:00:00', '2019-05-10 19:00:00', NULL),
(10, 10, 6, 'Finalizado', 'Finalizado', '100.00', '2019-05-13', '2019-05-13 19:00:00', '2019-05-13 19:00:00', NULL),
(11, 11, 4, 'Finalizado', 'Finalizado', '100.00', '2019-05-14', '2019-05-14 20:00:00', '2019-05-14 20:00:00', NULL),
(12, 12, 4, 'Finalizado', 'Finalizado', '100.00', '2019-05-15', '2019-05-15 20:00:00', '2019-05-15 20:00:00', NULL),
(13, 13, 6, 'Finalizado', 'Finalizado', '100.00', '2019-05-15', '2019-05-14 20:00:00', '2019-05-14 20:00:00', NULL),
(14, 14, 6, 'Finalizado', 'Finalizado', '100.00', '2019-05-16', '2019-05-16 20:00:00', '2019-05-16 20:00:00', NULL),
(15, 15, 4, 'Finalizado', 'Finalizado', '100.00', '2019-05-16', '2019-05-16 20:00:00', '2019-05-16 20:00:00', NULL),
(16, 16, 5, 'Finalizado', 'Finalizado', '100.00', '2019-05-15', '2019-05-15 20:00:00', '2019-05-15 20:00:00', NULL),
(17, 17, 6, 'Finalizado', 'Finalizado', '100.00', '2019-05-15', '2019-05-15 20:00:00', '2019-05-15 20:00:00', NULL),
(18, 18, 5, 'Finalizado', 'Finalizado', '100.00', '2019-05-20', '2019-05-20 20:00:00', '2019-05-20 20:00:00', NULL),
(19, 19, 4, 'Finalizado', 'Finalizado', '100.00', '2019-05-20', '2019-05-20 20:00:00', '2019-05-20 20:00:00', NULL),
(20, 20, 5, 'Finalizado', 'Finalizado', '100.00', '2019-05-20', '2019-05-20 20:00:00', '2019-05-20 20:00:00', NULL),
(21, 21, 6, 'Finalizado', 'Finalizado', '100.00', '2019-05-17', '2019-05-17 20:00:00', '2019-05-17 20:00:00', NULL),
(22, 22, 4, 'Finalizado', 'Finalizado', '100.00', '2019-05-21', '2019-05-21 20:00:00', '2019-05-21 20:00:00', NULL),
(23, 23, 6, 'Finalizado', 'Finalizado', '100.00', '2019-05-21', '2019-05-21 20:00:00', '2019-05-21 20:00:00', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `project`
--

CREATE TABLE `project` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `manager_id` bigint(20) UNSIGNED NOT NULL,
  `type_id` bigint(20) UNSIGNED NOT NULL,
  `code` varchar(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `start` date NOT NULL,
  `end` date NOT NULL,
  `start_new` date NOT NULL,
  `end_new` date NOT NULL,
  `price_base` decimal(12,2) NOT NULL,
  `price_real` decimal(12,2) DEFAULT NULL,
  `finalized` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `project`
--

INSERT INTO `project` (`id`, `client_id`, `manager_id`, `type_id`, `code`, `name`, `description`, `start`, `end`, `start_new`, `end_new`, `price_base`, `price_real`, `finalized`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 3, 1, 'P000001', 'Proyecto de creación de sistema personalizado', 'Proyecto de creación de sistema personalizado', '2019-05-01', '2019-05-27', '2019-05-24', '2019-05-27', '35000.00', '28823.48', '2019-06-29', '2019-04-22 14:00:00', '2019-06-29 23:03:58', NULL),
(3, 2, 3, 1, 'P000002', 'Landing Page', 'Landing Page para campaña de Fiestas patrias', '2019-07-19', '2019-08-09', '2019-07-19', '2019-08-09', '5500.00', '5500.00', NULL, '2019-07-12 22:46:06', '2019-07-12 22:46:06', NULL),
(4, 7, 3, 1, 'P000003', 'Pagina Web', 'Pagina web para chilis', '2019-07-26', '2019-09-26', '2019-07-26', '2019-09-26', '10000.00', '10000.00', NULL, '2019-07-12 22:47:16', '2019-07-12 22:47:16', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resource`
--

CREATE TABLE `resource` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `resource`
--

INSERT INTO `resource` (`id`, `name`, `description`, `slug`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Papel Bond', 'Papel Bond', 'papel-bond', '2019-06-08 08:48:32', '2019-06-08 08:48:32', NULL),
(2, 'Lapicero', 'Lapicero', 'lapicero', '2019-06-08 08:49:21', '2019-06-08 08:49:21', NULL),
(3, 'Transporte', 'Transporte', 'transporte', '2019-06-08 08:50:45', '2019-06-08 08:50:45', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role`
--

CREATE TABLE `role` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `role`
--

INSERT INTO `role` (`id`, `name`, `slug`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'admin', 'admin', '2019-03-10 22:25:59', '2019-03-10 22:00:22', NULL),
(2, 'Jefe de Proyecto', 'management', '2019-03-10 22:25:59', '2019-03-10 22:00:22', NULL),
(3, 'Desarrollador', 'developer', '2019-03-10 22:25:59', '2019-03-10 22:00:22', NULL),
(4, 'Cliente', 'client', '2019-03-10 22:25:59', '2019-03-10 22:29:03', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `task`
--

CREATE TABLE `task` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `activity_id` bigint(20) UNSIGNED NOT NULL,
  `father_id` bigint(20) UNSIGNED DEFAULT NULL,
  `task_id` bigint(20) UNSIGNED DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `structure` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `start` date NOT NULL,
  `end` date NOT NULL,
  `start_new` date DEFAULT NULL,
  `end_new` date DEFAULT NULL,
  `approved` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `task`
--

INSERT INTO `task` (`id`, `activity_id`, `father_id`, `task_id`, `user_id`, `structure`, `code`, `name`, `description`, `start`, `end`, `start_new`, `end_new`, `approved`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, NULL, NULL, 4, '1', 'T000001', 'Tarea 1', 'Tarea 1', '2019-05-01', '2019-05-03', '2019-05-01', '2019-05-02', '2019-05-02', '2019-04-22 15:20:00', '2019-05-02 15:20:00', NULL),
(2, 1, NULL, NULL, 5, '2', 'T000002', 'Tarea 2', 'Tarea 2', '2019-05-01', '2019-05-01', '2019-05-01', '2019-05-01', '2019-05-01', '2019-04-22 15:25:00', '2019-05-01 15:25:00', NULL),
(3, 1, 1, 1, 4, '1.1', 'T000003', 'Tarea 3', 'Tarea 3', '2019-05-06', '2019-05-08', '2019-05-03', '2019-05-06', '2019-05-06', '2019-04-22 15:27:00', '2019-05-06 15:26:00', NULL),
(4, 1, NULL, NULL, 5, '3', 'T000004', 'Tarea 4', 'Tarea 4', '2019-05-01', '2019-05-02', '2019-05-01', '2019-05-01', '2019-05-01', '2019-04-22 15:25:00', '2019-05-01 15:25:00', NULL),
(5, 2, NULL, NULL, 5, '4', 'T000005', 'Tarea 5', 'Tarea 5', '2019-05-09', '2019-05-10', '2019-05-07', '2019-05-08', '2019-05-08', '2019-04-22 15:29:00', '2019-05-09 19:00:00', NULL),
(6, 2, 5, 5, 5, '4.1', 'T000006', 'Tarea 6', 'Tarea 6', '2019-05-13', '2019-05-14', '2019-05-09', '2019-05-09', '2019-05-09', '2019-04-22 15:31:00', '2019-05-09 19:00:00', NULL),
(7, 2, NULL, NULL, 4, '5', 'T000007', 'Tarea 7', 'Tarea 7', '2019-05-09', '2019-05-10', '2019-05-07', '2019-05-07', '2019-05-08', '2019-04-22 15:33:00', '2019-05-08 19:00:00', NULL),
(8, 2, NULL, NULL, 6, '6', 'T000008', 'Tarea 8', 'Tarea 8', '2019-05-09', '2019-05-09', '2019-05-07', '2019-05-07', '2019-05-07', '2019-04-22 15:35:00', '2019-05-07 19:00:00', NULL),
(9, 3, 6, NULL, 6, '7', 'T000009', 'Tarea 9', 'Tarea 9', '2019-05-15', '2019-05-15', '2019-05-10', '2019-05-10', '2019-05-10', '2019-04-22 15:37:00', '2019-05-10 19:00:00', NULL),
(10, 3, 9, 9, 6, '7.1', 'T000010', 'Tarea 10', 'Tarea 10', '2019-05-16', '2019-05-16', '2019-05-13', '2019-05-13', '2019-05-13', '2019-04-22 15:39:00', '2019-05-13 19:00:00', NULL),
(11, 3, NULL, NULL, 4, '8', 'T000011', 'Tarea 11', 'Tarea 11', '2019-05-17', '2019-05-17', '2019-05-14', '2019-05-14', '2019-05-14', '2019-04-22 15:41:00', '2019-05-14 19:00:00', NULL),
(12, 3, 11, 11, 4, '8.1', 'T000012', 'Tarea 12', 'Tarea 12', '2019-05-20', '2019-05-20', '2019-05-15', '2019-05-15', '2019-05-15', '2019-04-22 15:43:00', '2019-05-15 19:00:00', NULL),
(13, 3, NULL, NULL, 6, '9', 'T000013', 'Tarea 13', 'Tarea 13', '2019-05-17', '2019-05-17', '2019-05-14', '2019-05-14', '2019-05-14', '2019-04-22 15:45:00', '2019-05-14 19:00:00', NULL),
(14, 3, 11, 12, 6, '8.1.1', 'T000014', 'Tarea 14', 'Tarea 14', '2019-05-21', '2019-05-21', '2019-05-16', '2019-05-16', '2019-05-16', '2019-04-22 15:47:00', '2019-05-16 19:00:00', NULL),
(15, 3, 11, 12, 4, '8.1.2', 'T000015', 'Tarea 15', 'Tarea 15', '2019-05-21', '2019-05-21', '2019-05-16', '2019-05-16', '2019-05-16', '2019-04-22 15:49:00', '2019-05-16 19:00:00', NULL),
(16, 3, 13, 13, 5, '9.1', 'T000016', 'Tarea 16', 'Tarea 16', '2019-05-20', '2019-05-20', '2019-05-15', '2019-05-15', '2019-05-15', '2019-04-22 15:51:00', '2019-05-15 19:00:00', NULL),
(17, 3, 13, 13, 6, '9.2', 'T000017', 'Tarea 17', 'Tarea 17', '2019-05-20', '2019-05-20', '2019-05-15', '2019-05-15', '2019-05-15', '2019-04-22 15:53:00', '2019-05-15 19:00:00', NULL),
(18, 4, NULL, NULL, 5, '10', 'T000018', 'Tarea 18', 'Tarea 18', '2019-05-22', '2019-05-23', '2019-05-17', '2019-05-20', '2019-05-20', '2019-04-22 15:55:00', '2019-05-20 21:00:00', NULL),
(19, 4, NULL, NULL, 4, '11', 'T000019', 'Tarea 19', 'Tarea 19', '2019-05-22', '2019-05-22', '2019-05-17', '2019-05-17', '2019-05-17', '2019-04-22 15:55:00', '2019-05-17 21:00:00', NULL),
(20, 4, NULL, NULL, 5, '12', 'T000020', 'Tarea 20', 'Tarea 20', '2019-05-22', '2019-05-23', '2019-05-20', '2019-05-20', '2019-05-20', '2019-04-22 15:55:00', '2019-05-20 21:00:00', NULL),
(21, 4, NULL, NULL, 6, '13', 'T000021', 'Tarea 21', 'Tarea 21', '2019-05-22', '2019-05-22', '2019-05-17', '2019-05-17', '2019-05-17', '2019-04-22 15:55:00', '2019-05-17 21:00:00', NULL),
(22, 4, 18, 18, 4, '10.1', 'T000022', 'Tarea 22', 'Tarea 22', '2019-05-24', '2019-05-24', '2019-05-21', '2019-05-21', '2019-05-21', '2019-04-22 15:55:00', '2019-05-21 21:00:00', NULL),
(23, 4, 18, 18, 6, '10.2', 'T000023', 'Tarea 23', 'Tarea 23', '2019-05-24', '2019-05-27', '2019-05-21', '2019-05-21', '2019-05-21', '2019-04-22 15:55:00', '2019-05-21 21:00:00', NULL),
(24, 7, NULL, NULL, 5, '14', 'T000024', 'Tarea 1', 'Tarea 1', '2019-07-26', '2019-08-02', '2019-07-26', '2019-08-02', NULL, '2019-07-12 23:05:06', '2019-07-12 23:05:06', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `type`
--

CREATE TABLE `type` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `type`
--

INSERT INTO `type` (`id`, `name`, `slug`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Proyecto', 'project', NULL, NULL, NULL),
(2, 'Tarea', 'work', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `company_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `surname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `company_id`, `role_id`, `name`, `surname`, `address`, `email`, `email_verified_at`, `username`, `password`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 'Admin', 'Admin', 'Lima', 'admin@admin.com', NULL, 'admin', '$2y$10$oRCuK4.81mL3jM3V4AMD.eSiZNEwM8x9PB1kZDDThxN/sMLTrcYd.', NULL, '2019-03-10 05:00:00', '2019-03-10 05:00:00', NULL),
(2, 2, 4, 'Veronica', 'Flores', 'Lima', 'm.flores@ishopperu.com.pe', NULL, 'ishop', '$2y$10$souEECkB1Rqd2v3HXw.8cu2tE1fCtRGPztLLm/zTYFKai79IGOdEK', NULL, '2019-03-12 07:30:15', '2019-07-12 22:40:19', NULL),
(3, 1, 2, 'Marco', 'Cristobal Depaz', 'Lima', 'mcristobal@mood.pe', NULL, 'jefe', '$2y$10$l7cPP3ojNxRXRLI.FzTqOer6wpF1b5i9zrA5xHnaVmy1eKRCwh5Gy', NULL, '2019-03-13 06:49:08', '2019-06-08 08:45:07', NULL),
(4, 1, 3, 'Brayan Domingo', 'Sayan Gutierrez', 'Lima', 'magaxanime@gmail.com', NULL, 'narugamy', '$2y$10$yBAAlBK0Tb8FqEdtn1.aLuIugD7fM3/ywvylm741Fk0/POr5EmbeW', NULL, '2019-03-12 07:30:15', '2019-06-08 08:10:45', NULL),
(5, 1, 3, 'Geraldine', 'Peña Mota', 'developer', 'geeboox@gmail.com', NULL, 'geeboox', '$2y$10$mX1Zv0mDM6ghsmZ96dGBfu.Z8lgQ7Qq7FkkWhdKruCmzmh.58YBZy', NULL, '2019-03-25 02:36:36', '2019-07-12 22:59:12', NULL),
(6, 1, 3, 'Diego', 'Nancay Cruz', 'developer', 'dnancay@mood.pe', NULL, 'nancay', '$2y$10$3NN8k2uw5ndLFkOvZDNA0e8n4XPUIkViJptyBif.u3zYUe.eM/Dle', NULL, '2019-03-25 02:36:36', '2019-06-08 08:43:09', NULL),
(7, 4, 4, 'Fabiola', 'Quintero', 'Lima', 'fquintero@mod.pe', NULL, 'chilis', '$2y$10$oZczYEkk2B0.w4.XyLsBUOX2.uVPXVmccB6zbIWV61vlryPcErFUy', NULL, '2019-07-12 22:42:13', '2019-07-12 22:42:13', NULL),
(8, 8, 4, 'Omar', 'Salas', 'Lima', 'osala@divemotor.com.pe', NULL, 'divemotor', '$2y$10$j2fT2gb70NnVS5Fril/uDu08H1GDRJBLg9TC1nrnofKhguvqrtJEC', NULL, '2019-07-12 22:43:59', '2019-07-12 22:43:59', NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `activity`
--
ALTER TABLE `activity`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `activity_code_unique` (`code`),
  ADD KEY `activity_phase_id_foreign` (`phase_id`),
  ADD KEY `activity_user_id_foreign` (`user_id`),
  ADD KEY `fk_activity_activity1_idx` (`activity_id`),
  ADD KEY `fk_activity_activity2_idx` (`father_id`);

--
-- Indices de la tabla `archive`
--
ALTER TABLE `archive`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `company_name_unique` (`name`),
  ADD UNIQUE KEY `company_slug_unique` (`slug`);

--
-- Indices de la tabla `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `config_name_unique` (`name`),
  ADD UNIQUE KEY `config_key_unique` (`key`),
  ADD UNIQUE KEY `config_slug_unique` (`slug`);

--
-- Indices de la tabla `container`
--
ALTER TABLE `container`
  ADD PRIMARY KEY (`id`),
  ADD KEY `container_resource_id_foreign` (`resource_id`),
  ADD KEY `container_task_id_foreign_idx` (`task_id`);

--
-- Indices de la tabla `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `history_code_unique` (`code`),
  ADD KEY `history_task_id_foreign` (`task_id`);

--
-- Indices de la tabla `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`),
  ADD KEY `message_archive_id_foreign` (`archive_id`),
  ADD KEY `message_employee_id_foreign` (`employee_id`),
  ADD KEY `message_user_id_foreign` (`user_id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notification_message_id_foreign` (`message_id`),
  ADD KEY `notification_user_id_foreign` (`user_id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `phase`
--
ALTER TABLE `phase`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `phase_code_unique` (`code`),
  ADD KEY `phase_project_id_foreign` (`project_id`);

--
-- Indices de la tabla `progress`
--
ALTER TABLE `progress`
  ADD PRIMARY KEY (`id`),
  ADD KEY `progress_task_id_foreign` (`task_id`),
  ADD KEY `progress_user_id_foreign` (`user_id`);

--
-- Indices de la tabla `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `project_code_unique` (`code`),
  ADD UNIQUE KEY `project_name_unique` (`name`),
  ADD KEY `project_client_id_foreign` (`client_id`),
  ADD KEY `project_manager_id_foreign` (`manager_id`),
  ADD KEY `project_type_id_foreign` (`type_id`);

--
-- Indices de la tabla `resource`
--
ALTER TABLE `resource`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `resource_name_unique` (`name`),
  ADD UNIQUE KEY `resource_slug_unique` (`slug`);

--
-- Indices de la tabla `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `role_name_unique` (`name`),
  ADD UNIQUE KEY `role_slug_unique` (`slug`);

--
-- Indices de la tabla `task`
--
ALTER TABLE `task`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `task_code_unique` (`code`),
  ADD KEY `task_activity_id_foreign` (`activity_id`),
  ADD KEY `task_user_id_foreign` (`user_id`),
  ADD KEY `fk_task_task1_idx` (`task_id`),
  ADD KEY `fk_task_task2_idx` (`father_id`);

--
-- Indices de la tabla `type`
--
ALTER TABLE `type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `type_name_unique` (`name`),
  ADD UNIQUE KEY `type_slug_unique` (`slug`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_email_unique` (`email`),
  ADD UNIQUE KEY `user_username_unique` (`username`),
  ADD KEY `user_company_id_foreign` (`company_id`),
  ADD KEY `user_role_id_foreign` (`role_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `activity`
--
ALTER TABLE `activity`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `archive`
--
ALTER TABLE `archive`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `company`
--
ALTER TABLE `company`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `config`
--
ALTER TABLE `config`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `container`
--
ALTER TABLE `container`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `history`
--
ALTER TABLE `history`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `message`
--
ALTER TABLE `message`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT de la tabla `notification`
--
ALTER TABLE `notification`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `phase`
--
ALTER TABLE `phase`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `progress`
--
ALTER TABLE `progress`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT de la tabla `project`
--
ALTER TABLE `project`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `resource`
--
ALTER TABLE `resource`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `role`
--
ALTER TABLE `role`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `task`
--
ALTER TABLE `task`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT de la tabla `type`
--
ALTER TABLE `type`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `activity`
--
ALTER TABLE `activity`
  ADD CONSTRAINT `activity_phase_id_foreign` FOREIGN KEY (`phase_id`) REFERENCES `phase` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `activity_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_activity_activity1` FOREIGN KEY (`activity_id`) REFERENCES `activity` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_activity_activity2` FOREIGN KEY (`father_id`) REFERENCES `activity` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `container`
--
ALTER TABLE `container`
  ADD CONSTRAINT `container_resource_id_foreign` FOREIGN KEY (`resource_id`) REFERENCES `resource` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `container_task_id_foreign` FOREIGN KEY (`task_id`) REFERENCES `task` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `history`
--
ALTER TABLE `history`
  ADD CONSTRAINT `history_task_id_foreign` FOREIGN KEY (`task_id`) REFERENCES `task` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `message`
--
ALTER TABLE `message`
  ADD CONSTRAINT `message_archive_id_foreign` FOREIGN KEY (`archive_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `message_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `message_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `notification`
--
ALTER TABLE `notification`
  ADD CONSTRAINT `notification_message_id_foreign` FOREIGN KEY (`message_id`) REFERENCES `message` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `notification_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `phase`
--
ALTER TABLE `phase`
  ADD CONSTRAINT `phase_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `progress`
--
ALTER TABLE `progress`
  ADD CONSTRAINT `progress_task_id_foreign` FOREIGN KEY (`task_id`) REFERENCES `task` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `progress_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `project`
--
ALTER TABLE `project`
  ADD CONSTRAINT `project_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `project_manager_id_foreign` FOREIGN KEY (`manager_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `project_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `task`
--
ALTER TABLE `task`
  ADD CONSTRAINT `fk_task_task1` FOREIGN KEY (`task_id`) REFERENCES `task` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_task_task2` FOREIGN KEY (`father_id`) REFERENCES `task` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `task_activity_id_foreign` FOREIGN KEY (`activity_id`) REFERENCES `activity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `task_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_company_id_foreign` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
