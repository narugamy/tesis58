<?php

	use Illuminate\Support\Facades\Schema;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;

	class CreateUserTable extends Migration {

		public function up() {
			Schema::create('user', function (Blueprint $table) {
				$table->bigIncrements('id');
				$table->unsignedBigInteger('company_id');
				$table->foreign('company_id')->references('id')->on('company')->onUpdate('cascade')->onDelete('cascade');
				$table->unsignedBigInteger('role_id');
				$table->foreign('role_id')->references('id')->on('role')->onUpdate('cascade')->onDelete('cascade');
				$table->string('name');
				$table->string('surname');
				$table->string('address');
				$table->string('email')->unique();
				$table->timestamp('email_verified_at')->nullable();
				$table->string('username')->unique();
				$table->string('password');
				$table->rememberToken();
				$table->timestamps();
				$table->softDeletes();
			});
		}

		public function down() {
			Schema::dropIfExists('user');
		}
	}
