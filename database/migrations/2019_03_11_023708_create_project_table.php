<?php

	use Illuminate\Support\Facades\Schema;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;

	class CreateProjectTable extends Migration {

		public function up() {
			Schema::create('project', function (Blueprint $table) {
				$table->bigIncrements('id');
				$table->unsignedBigInteger('client_id');
				$table->foreign('client_id')->references('id')->on('user')->onUpdate('cascade')->onDelete('cascade');
				$table->unsignedBigInteger('manager_id');
				$table->foreign('manager_id')->references('id')->on('user')->onUpdate('cascade')->onDelete('cascade');
				$table->unsignedBigInteger('type_id');
				$table->foreign('type_id')->references('id')->on('type')->onUpdate('cascade')->onDelete('cascade');
				$table->string('code',7)->unique();
				$table->string('name')->unique();
				$table->text('description');
				$table->date('start');
				$table->date('end');
				$table->decimal('price_base',12,2);
				$table->decimal('price_real',12,2)->nullable();
				$table->date('finalized')->nullable();
				$table->timestamps();
				$table->softDeletes();
			});
		}

		public function down() {
			Schema::dropIfExists('project');
		}
	}
