<?php

	use Illuminate\Support\Facades\Schema;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;

	class CreateProgressTable extends Migration {

		public function up() {
			Schema::create('progress', function (Blueprint $table) {
				$table->bigIncrements('id');
				$table->unsignedBigInteger('task_id');
				$table->foreign('task_id')->references('id')->on('task')->onUpdate('cascade')->onDelete('cascade');
				$table->unsignedBigInteger('user_id');
				$table->foreign('user_id')->references('id')->on('user')->onUpdate('cascade')->onDelete('cascade');
				$table->string('name');
				$table->text('description');
				$table->decimal('progress',5,2);
				$table->date('approved');
				$table->timestamps();
				$table->softDeletes();
			});
		}

		public function down() {
			Schema::dropIfExists('progress');
		}
	}
