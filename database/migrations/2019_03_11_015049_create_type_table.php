<?php

	use Illuminate\Support\Facades\Schema;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;

	class CreateTypeTable extends Migration {

		public function up() {
			Schema::create('type', function (Blueprint $table) {
				$table->bigIncrements('id');
				$table->string('name')->unique();
				$table->string('slug')->unique();
				$table->timestamps();
				$table->softDeletes();
			});
		}

		public function down() {
			Schema::dropIfExists('type');
		}
	}
