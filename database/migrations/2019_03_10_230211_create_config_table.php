<?php

	use Illuminate\Support\Facades\Schema;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;

	class CreateConfigTable extends Migration {

		public function up() {
			Schema::create('config', function (Blueprint $table) {
				$table->bigIncrements('id');
				$table->string('name')->unique();
				$table->string('key')->unique();
				$table->string('value', 191);
				$table->string('slug')->unique();
				$table->timestamps();
				$table->softDeletes();
			});
		}

		public function down() {
			Schema::dropIfExists('config');
		}
	}
