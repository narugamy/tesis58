<?php

	use Illuminate\Support\Facades\Schema;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;

	class CreateResourceTable extends Migration {

		public function up() {
			Schema::create('resource', function (Blueprint $table) {
				$table->bigIncrements('id');
				$table->string('name')->unique();
				$table->text('description');
				$table->string('slug')->unique();
				$table->timestamps();
				$table->softDeletes();
			});
		}

		public function down() {
			Schema::dropIfExists('resource');
		}
	}
