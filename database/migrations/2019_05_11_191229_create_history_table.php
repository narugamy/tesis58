<?php

	use Illuminate\Support\Facades\Schema;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;

	class CreateHistoryTable extends Migration {

		public function up() {
			Schema::create('history', function (Blueprint $table) {
				$table->bigIncrements('id');
				$table->unsignedBigInteger('task_id');
				$table->foreign('task_id')->references('id')->on('task')->onUpdate('cascade')->onDelete('cascade');
				$table->string('code', 191)->unique();
				$table->text('name');
				$table->text('description');
				$table->timestamps();
				$table->softDeletes();
			});
		}

		public function down() {
			Schema::dropIfExists('history');
		}
	}
