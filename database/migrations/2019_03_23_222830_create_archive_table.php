<?php

	use Illuminate\Support\Facades\Schema;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;

	class CreateArchiveTable extends Migration {

		public function up() {
			Schema::create('archive', function (Blueprint $table) {
				$table->bigIncrements('id');
				$table->text('name');
				$table->text('route');
				$table->text('encrypt');
				$table->timestamps();
				$table->softDeletes();
			});
		}

		public function down() {
			Schema::dropIfExists('archive');
		}

	}
