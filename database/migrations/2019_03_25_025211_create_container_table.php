<?php

	use Illuminate\Support\Facades\Schema;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;

	class CreateContainerTable extends Migration {

		public function up() {
			Schema::create('container', function (Blueprint $table) {
				$table->bigIncrements('id');
				$table->unsignedBigInteger('task_id');
				$table->foreign('task_id')->references('id')->on('task')->onUpdate('cascade')->onDelete('cascade');
				$table->unsignedBigInteger('resource_id');
				$table->foreign('resource_id')->references('id')->on('resource')->onUpdate('cascade')->onDelete('cascade');
				$table->integer('number');
				$table->decimal('price',12,2);
				$table->decimal('total',12,2);
				$table->timestamps();
				$table->softDeletes();
			});
		}

		public function down() {
			Schema::dropIfExists('container');
		}
	}
