<?php

	use Illuminate\Support\Facades\Schema;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;

	class CreateNotificationTable extends Migration {

		public function up() {
			Schema::create('notification', function (Blueprint $table) {
				$table->bigIncrements('id');
				$table->unsignedBigInteger('message_id');
				$table->foreign('message_id')->references('id')->on('message')->onUpdate('cascade')->onDelete('cascade');
				$table->unsignedBigInteger('user_id');
				$table->foreign('user_id')->references('id')->on('user')->onUpdate('cascade')->onDelete('cascade');
				$table->text('route');
				$table->timestamps();
				$table->softDeletes();
			});
		}

		public function down() {
			Schema::dropIfExists('notification');
		}
	}
