<?php

	use Illuminate\Support\Facades\Schema;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;

	class CreatePhaseTable extends Migration {

		public function up() {
			Schema::create('phase', function (Blueprint $table) {
				$table->bigIncrements('id');
				$table->unsignedBigInteger('project_id');
				$table->foreign('project_id')->references('id')->on('project')->onUpdate('cascade')->onDelete('cascade');
				$table->string('code',7)->unique();
				$table->string('name');
				$table->text('description');
				$table->date('start');
				$table->date('end');
				$table->timestamps();
				$table->softDeletes();
			});
		}

		public function down() {
			Schema::dropIfExists('phase');
		}
	}
