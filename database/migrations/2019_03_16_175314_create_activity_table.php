<?php

	use Illuminate\Support\Facades\Schema;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;

	class CreateActivityTable extends Migration {

		public function up() {
			Schema::create('activity', function (Blueprint $table) {
				$table->bigIncrements('id');
					$table->unsignedBigInteger('activity_id');
				$table->foreign('activity_id')->references('id')->on('activity')->onUpdate('cascade')->onDelete('cascade');
				$table->unsignedBigInteger('father_id');
				$table->foreign('father_id')->references('id')->on('father')->onUpdate('cascade')->onDelete('cascade');
				$table->unsignedBigInteger('phase_id');
				$table->foreign('phase_id')->references('id')->on('phase')->onUpdate('cascade')->onDelete('cascade');
				$table->unsignedBigInteger('user_id');
				$table->foreign('user_id')->references('id')->on('user')->onUpdate('cascade')->onDelete('cascade');
				$table->string('structure',191);
				$table->string('code',7)->unique();
				$table->string('name');
				$table->text('description');
				$table->date('start');
				$table->date('end');
				$table->date('start_new');
				$table->date('end_new');
				$table->date('approved');
				$table->timestamps();
				$table->softDeletes();
			});
		}

		public function down() {
			Schema::dropIfExists('activity');
		}
	}
