<?php

	use Illuminate\Support\Facades\Schema;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;

	class CreateMessageTable extends Migration {

		public function up() {
			Schema::create('message', function (Blueprint $table) {
				$table->bigIncrements('id');
				$table->unsignedBigInteger('archive_id')->nullable();
				$table->foreign('archive_id')->references('id')->on('user')->onUpdate('cascade')->onDelete('cascade');
				$table->unsignedBigInteger('employee_id');
				$table->foreign('employee_id')->references('id')->on('user')->onUpdate('cascade')->onDelete('cascade');
				$table->unsignedBigInteger('user_id');
				$table->foreign('user_id')->references('id')->on('user')->onUpdate('cascade')->onDelete('cascade');
				$table->unsignedBigInteger('table_id');
				$table->string('table_type',191);
				$table->string('name', 191);
				$table->text('description');
				$table->timestamps();
				$table->softDeletes();
			});
		}

		public function down() {
			Schema::dropIfExists('message');
		}
	}
