<?php

	namespace App\Http\Middleware;

	use Closure;
	use Exception;
	use Illuminate\Auth\AuthenticationException;
	use Illuminate\Auth\Middleware\Authenticate as Middleware;
	use Illuminate\Support\Arr;

	class Authenticate extends Middleware {

		protected $guards = [];

		public function handle($request, Closure $next, ...$guards) {
			$this->guards = $guards;
			return parent::handle($request, $next, ...$guards);
		}

		protected function redirectTo($request) {
			$guard = Arr::first($this->guards);
			if (!$request->expectsJson()) :
				return $this->get_route($guard);
			endif;
			return response()->json(['error' => ['message' => 'Usted no esta autenticado', 'route' => $this->get_route($guard)]], 401);
		}

		private function get_route($guard) {
			if ($guard === 'web' || empty($guard))
				return route('login');
			return route($guard . '.login');
		}

	}
