<?php

	namespace App\Http\Controllers\Admin\User;

	use App\Model\Company;
	use App\Model\Role;
	use App\Model\User;
	use Exception;
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use Illuminate\Support\Facades\DB;
	use Yajra\DataTables\Facades\DataTables;

	class UserController extends Controller {

		public function index() {
			$users = User::withTrashed()->get();
			return view('admin.user._index')->with(['title' => 'Panel de usuarios', 'class_header' => 'page-container-bg-solid', 'users' => $users]);
		}

		public function table(Request $request) {
			if ($request->ajax()):
				try {
					return DataTables::of(User::withTrashed()->with('role', 'company')->orderBy('name'))->addColumn('link', function ($row){
						$color = ($row->deleted_at) ? 'red' : 'blue';
						$buttons = "<div class='btn-group'><button class='btn $color btn-xs btn-outline dropdown-toggle' data-toggle='dropdown'>Acciones<i class='fa fa-angle-down'></i></button><ul class='dropdown-menu'>";
						if (!$row->deleted_at):
							$buttons .= "<li><a data-url='" . route('admin.user.update', $row->username) . "' class='btn-ajax'><i class='fa fa-pencil'></i> Mostrar</a></li>";
							$buttons .= "<li><a data-url='" . route('admin.user.delete', $row->username) . "' data-message='Desea deshabilitar la compañia: " . $row->name . "' class='btn-destroy'><i class='fa fa-low-vision'></i> Deshabilitar</a></li>";
							$buttons .= "<li><a data-url='" . route('admin.user.destroy', $row->username) . "' data-message='Desea eliminar la compañia: " . $row->name . "' class='btn-destroy'><i class='fa fa-trash'></i> Eliminar</a></li>";
						else:
							$buttons .= "<li><a data-url='" . route('admin.user.delete', $row->username) . "' data-message='Desea restaurar la compañia: " . $row->name . "' class='btn-destroy'><i class='fa fa-recycle'></i> Restaurar</a></li>";
						endif;
						$buttons .= "</ul></div>";
						return $buttons;
					})->addIndexColumn()->addColumn('full_name', function ($row) {
						return $row->full_name;
					})->rawColumns(['link'])->make(true);
				} catch (Exception $e) {
					return response()->json(["sEcho" => 1, "iTotalRecords" => 0, "iTotalDisplayRecords" => 0, "aaData" => []]);
				}
			else:
				return redirect()->route('admin.login');
			endif;
		}

		public function create(Request $request) {
			$roles = Role::orderBy('name')->pluck('name', 'id');
			$companies = Company::orderBy('name')->pluck('name', 'id');
			if ($request->ajax()):
				$data = view('admin.user._Create');
			else:
				$data = view('admin.user.Create')->with(['title' => 'Registro de usuario']);
			endif;
			return $data->with(['roles' => $roles, 'companies' => $companies]);
		}

		public function store(Request $request) {
			try {
				DB::beginTransaction();
				$user = new User($request->all());
				$user->save();
				DB::commit();
				$message = "Registro exitoso";
				$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('admin.user.index')], 'status' => 200, 'route' => route('admin.user.index'), 'message' => $message, 'type' => 'success'];
				$data = $this->optimize($array);
			} catch (Exception $e) {
				DB::rollBack();
				$message = "Ocurrio un error en el proceso";
				$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('admin.user.create'), 'message' => $message, 'type' => 'error'];
				$data = $this->optimize($array);
			}
			return $data;
		}

		public function show(Request $request, $username) {
			$user = User::where(['username' => $username])->with('role', 'company')->first();
			if ($user):
				$roles = Role::orderBy('name')->pluck('name', 'id');
				$companies = Company::orderBy('name')->pluck('name', 'id');
				if ($request->ajax()):
					$data = view('admin.user._Update');
				else:
					$data = view('admin.user.Update')->with(['title' => "Actualización del usuario: ".$user->full_name]);
				endif;
				$data->with(['user' => $user, 'roles' => $roles, 'companies' => $companies]);
			else:
				if ($request->ajax()):
					$data = "Usuario no existente";
				else:
					session()->flash('improper', "No intentes algo indebido, el usuario no existe");
					$data = redirect(route('admin.user.index'));
				endif;
			endif;
			return $data;
		}

		public function update(Request $request, $username) {
			$user = User::where(['username' => $username])->first();
			if ($user):
				try {
					DB::beginTransaction();
					$password = $user->password;
					$user->fill($request->all());
					$user->password = (!empty($user->password)) ? bcrypt($request->password) : $password;
					$user->save();
					DB::commit();
					$message = "Actualización exitosa";
					$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('admin.user.index')], 'status' => 200, 'route' => route('admin.user.index'), 'message' => $message, 'type' => 'success'];
					$data = $this->optimize($array);
				} catch (Exception $e) {
					DB::rollBack();
					$message = "Ocurrio un error en el proceso";
					$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('admin.user.update', $user->username), 'message' => $message, 'type' => 'error'];
					$data = $this->optimize($array);
				}
			else:
				$message = "No intentes algo indebido, el usuario no existe";
				$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('admin.user.index'), 'message' => $message, 'type' => 'improper'];
				$data = $this->optimize($array);
			endif;
			return $data;
		}

		public function delete(Request $request, $username) {
			if ($request->ajax()):
				$user = User::withTrashed()->where(['username' => $username])->first();
				if ($user):
					try {
						DB::beginTransaction();
						if ($user->deleted_at):
							$user->restore();
							$message = "Restauración exitosa";
						else:
							$user->delete();
							$message = "Deshabilitación exitosa";
						endif;
						DB::commit();
						session()->flash('success', $message);
						$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('admin.user.index')], 200);
					} catch (Exception $e) {
						DB::rollBack();
						$data = response()->json(['resp' => false, 'message' => "Ocurrio un error en el proceso"], 422);
					}
				else:
					$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, el usuario no existe"], 422);
				endif;
			else:
				session()->flash('improper', "No intentes algo indebido");
				$data = redirect(route('admin.user.index'));
			endif;
			return $data;
		}

		public function destroy(Request $request, $username) {
			if ($request->ajax()):
				$user = User::withTrashed()->where(['username' => $username])->first();
				if ($user):
					try {
						DB::beginTransaction();
						$user->forceDelete();
						DB::commit();
						$message = "Eliminación exitosa";
						session()->flash('success', $message);
						$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('admin.user.index')], 200);
					} catch (Exception $e) {
						DB::rollBack();
						$data = response()->json(['resp' => false, 'message' => "Ocurrio un error en el proceso"], 422);
					}
				else:
					$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, el usuario no existe"], 422);
				endif;
			else:
				session()->flash('improper', "No intentes algo indebido");
				$data = redirect(route('admin.user.index'));
			endif;
			return $data;
		}

	}
