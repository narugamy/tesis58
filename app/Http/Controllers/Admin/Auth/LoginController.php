<?php

	namespace App\Http\Controllers\Admin\Auth;

	use App\Http\Controllers\Controller;
	use App\Http\Requests\Auth\LoginRequest;
	use App\Model\User;
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\Auth;
	use Illuminate\Support\Facades\Hash;

	class LoginController extends Controller {

		public function showLoginForm() {
			return view('admin.auth.app');
		}

		public function login(LoginRequest $request) {
			$user = User::where(['username' => $request->username])->with('role')->first();
			if ($user):
				if (Hash::check($request->password, $user->password)):
					if ($user->is_role('admin')):
						Auth::guard('admin')->login($user);
						$message = 'Registro exitoso';
						$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('admin.index'), 'errors' => null], 'status' => 200, 'route' => route('admin.index'), 'message' => $message, 'type' => 'success'];
						return $this->optimize($array);
					else:
						$message = 'No tienes el rol correcto';
						$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message, 'errors' => ['username' => 'Su cuenta no tiene el privilegio']], 'status' => 422, 'route' => route('admin.login'), 'message' => $message, 'type' => 'improper'];
						return $this->optimize($array);
					endif;
				else:
					$message = 'Contraseña incorrecta';
					$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message, 'errors' => ['password' => 'Su contraseña es incorrecta']], 'status' => 422, 'route' => route('admin.login'), 'message' => $message, 'type' => 'improper'];
					return $this->optimize($array);
				endif;
			else:
				$message = 'El usuario no existe';
				$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message, 'errors' => ['username' => 'Su cuenta no existe']], 'status' => 422, 'route' => route('admin.login'), 'message' => $message, 'type' => 'improper'];
				return $this->optimize($array);
			endif;
		}

		public function destroy(Request $request) {
			//session()->forget('lock-expires-at');
			Auth::guard('admin')->logout();
			$message = 'Session cerrada exitosamente';
			$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'errors' => null], 'status' => 200, 'route' => route('admin.login'), 'message' => $message, 'type' => 'success'];
			return $this->optimize($array);
		}

		public function locked() {
			if (!session('lock-expires-at')) {
				return redirect('/');
			}
			if (session('lock-expires-at') > now()) {
				return redirect('/');
			}
			return view('admin.auth.lock');
		}

		public function unlock(Request $request) {
			$auth = Auth::guard('admin')->user();
			$check = Hash::check($request->password, $auth->password);
			if (!$check) {
				return redirect()->route('admin.login.locked')->withErrors(['Your password does not match your profile.']);
			}
			session(['lock-expires-at' => now()->addMinutes($auth->getLockoutTime())]);
			return redirect()->route('admin.index');
		}
	}
