<?php

	namespace App\Http\Controllers\Admin\Role;

	//use App\Http\Requests\Admin\Area\CreateRequest;
	use App\Model\Role;
	use Exception;
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use Illuminate\Support\Facades\DB;
	use Yajra\DataTables\DataTables;

	class RoleController extends Controller {

		public function index() {
			return view('admin.role._index')->with(['title' => 'Panel de roles', 'class_header' => 'page-container-bg-solid']);
		}

		public function table(Request $request) {
			if ($request->ajax()):
				try {
					return DataTables::of(Role::withTrashed()->orderBy('name'))->addColumn('link', function ($row){
						$color = ($row->deleted_at) ? 'red' : 'blue';
						$buttons = "<div class='btn-group'><button class='btn $color btn-xs btn-outline dropdown-toggle' data-toggle='dropdown'>Acciones<i class='fa fa-angle-down'></i></button><ul class='dropdown-menu'>";
						if (!$row->deleted_at):
							$buttons .= "<li><a data-url='" . route('admin.role.update', $row->slug) . "' class='btn-ajax'><i class='fa fa-pencil'></i> Mostrar</a></li>";
							$buttons .= "<li><a data-url='" . route('admin.role.delete', $row->slug) . "' data-message='Desea deshabilitar el rol: " . $row->name . "' class='btn-destroy'><i class='fa fa-low-vision'></i> Deshabilitar</a></li>";
							$buttons .= "<li><a data-url='" . route('admin.role.destroy', $row->slug) . "' data-message='Desea eliminar el rol: " . $row->name . "' class='btn-destroy'><i class='fa fa-trash'></i> Eliminar</a></li>";
						else:
							$buttons .= "<li><a data-url='" . route('admin.role.delete', $row->slug) . "' data-message='Desea restaurar el rol: " . $row->name . "' class='btn-destroy'><i class='fa fa-recycle'></i> Restaurar</a></li>";
						endif;
						$buttons .= "</ul></div>";
						return $buttons;
					})->addIndexColumn()->rawColumns(['link'])->make(true);
				} catch (Exception $e) {
					return response()->json(["sEcho" => 1, "iTotalRecords" => 0, "iTotalDisplayRecords" => 0, "aaData" => []]);
				}
			else:
				return redirect()->route('admin.login');
			endif;
		}

		public function create(Request $request) {
			if ($request->ajax()):
				$data = view('admin.role._Create');
			else:
				$data = view('admin.role.Create')->with(['title' => 'Registro de rol']);
			endif;
			return $data;
		}

		public function store(Request $request) {
			try {
				DB::beginTransaction();
				$role = new Role($request->all());
				$role->save();
				$message = "Registro exitoso";
				DB::commit();
				$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('admin.role.index')], 'status' => 200, 'route' => route('admin.role.index'), 'message' => $message, 'type' => 'success'];
				$data = $this->optimize($array);
			} catch (Exception $e) {
				DB::rollBack();
				$message = "Ocurrio un error en el proceso";
				$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $e], 'status' => 422, 'route' => route('admin.role.create'), 'message' => $message, 'type' => 'error'];
				$data = $this->optimize($array);
			}
			return $data;
		}

		public function show(Request $request, $slug) {
			$role = Role::withTrashed()->where(['slug' => $slug])->first();
			if ($role):
				if ($request->ajax()):
					$data = view('admin.role._Update');
				else:
					$data = view('admin.role.Update')->with(['title' => "Actualización del rol: $role->name"]);
				endif;
				$data->with(['role' => $role]);
			else:
				if ($request->ajax()):
					$data = "Area no existente";
				else:
					session()->flash('improper', "No intentes algo indebido, el role no existe");
					$data = redirect(route('admin.role.index'));
				endif;
			endif;
			return $data;
		}

		public function update(Request $request, $slug) {
			$role = Role::withTrashed()->where(['slug' => $slug])->first();
			if ($role):
				try {
					DB::beginTransaction();
					$role->fill($request->all())->save();
					$message = "Actualizacion exitosa";
					DB::commit();
					$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('admin.role.index')], 'status' => 200, 'route' => route('admin.role.index'), 'message' => $message, 'type' => 'success'];
					$data = $this->optimize($array);
				} catch (Exception $e) {
					DB::rollBack();
					$message = "Ocurrio un error en el proceso";
					$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('admin.role.update', $role->id), 'message' => $message, 'type' => 'error'];
					$data = $this->optimize($array);
				}
			else:
				$message = "No intentes algo indebido, el rol no existe";
				$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('admin.role.index'), 'message' => $message, 'type' => 'improper'];
				$data = $this->optimize($array);
			endif;
			return $data;
		}

		public function delete(Request $request, $slug) {
			if ($request->ajax()):
				$role = Role::withTrashed()->where(['slug' => $slug])->first();
				if ($role):
					try {
						DB::beginTransaction();
						if ($role->deleted_at):
							$role->restore();
							$message = "Restauracion exitosa";
						else:
							$role->delete();
							$message = "Deshabilitación exitosa";
						endif;
						DB::commit();
						session()->flash('success', $message);
						$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('admin.role.index')], 200);
					} catch (Exception $e) {
						DB::rollBack();
						$data = response()->json(['resp' => false, 'message' => "Ocurrio un error en el proceso"], 422);
					}
				else:
					$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, el rol no existe"], 422);
				endif;
			else:
				session()->flash('improper', "No intentes algo indebido");
				$data = redirect(route('admin.role.index'));
			endif;
			return $data;
		}

		public function destroy(Request $request, $slug) {
			if ($request->ajax()):
				$role = Role::withTrashed()->where(['slug' => $slug])->first();
				if ($role):
					try {
						DB::beginTransaction();
						$role->forceDelete();
						DB::commit();
						$message = "Eliminacion exitosa";
						session()->flash('success', $message);
						$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('admin.role.index')], 200);
					} catch (Exception $e) {
						DB::rollBack();
						$data = response()->json(['resp' => false, 'message' => "Ocurrio un error en el proceso"], 422);
					}
				else:
					$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, el rol no existe"], 422);
				endif;
			else:
				session()->flash('improper', "No intentes algo indebido");
				$data = redirect(route('admin.role.index'));
			endif;
			return $data;
		}

	}
