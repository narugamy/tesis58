<?php

	namespace App\Http\Controllers\Admin\Resource;

	use App\Model\Resource;
	use Exception;
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use Illuminate\Support\Facades\DB;
	use Yajra\DataTables\Facades\DataTables;

	class ResourceController extends Controller {

		public function index() {
			return view('admin.resource._index')->with(['title' => 'Panel de recursos', 'class_header' => 'page-container-bg-solid']);
		}

		public function table(Request $request) {
			if ($request->ajax()):
				try {
					return DataTables::of(Resource::withTrashed()->orderBy('name'))->addColumn('link', function ($row){
						$color = ($row->deleted_at) ? 'red' : 'blue';
						$buttons = "<div class='btn-group'><button class='btn $color btn-xs btn-outline dropdown-toggle' data-toggle='dropdown'>Acciones<i class='fa fa-angle-down'></i></button><ul class='dropdown-menu'>";
						if (!$row->deleted_at):
							$buttons .= "<li><a data-url='" . route('admin.resource.update', $row->slug) . "' class='btn-ajax'><i class='fa fa-pencil'></i> Mostrar</a></li>";
							$buttons .= "<li><a data-url='" . route('admin.resource.delete', $row->slug) . "' data-message='Desea deshabilitar el rol: " . $row->name . "' class='btn-destroy'><i class='fa fa-low-vision'></i> Deshabilitar</a></li>";
							$buttons .= "<li><a data-url='" . route('admin.resource.destroy', $row->slug) . "' data-message='Desea eliminar el rol: " . $row->name . "' class='btn-destroy'><i class='fa fa-trash'></i> Eliminar</a></li>";
						else:
							$buttons .= "<li><a data-url='" . route('admin.resource.delete', $row->slug) . "' data-message='Desea restaurar el rol: " . $row->name . "' class='btn-destroy'><i class='fa fa-recycle'></i> Restaurar</a></li>";
						endif;
						$buttons .= "</ul></div>";
						return $buttons;
					})->addIndexColumn()->rawColumns(['link'])->make(true);
				} catch (Exception $e) {
					return response()->json(["sEcho" => 1, "iTotalRecords" => 0, "iTotalDisplayRecords" => 0, "aaData" => []]);
				}
			else:
				return redirect()->route('admin.login');
			endif;
		}

		public function create(Request $request) {
			if ($request->ajax()):
				$data = view('admin.resource._Create');
			else:
				$data = view('admin.resource.Create')->with(['title' => 'Registro de recurso']);
			endif;
			return $data;
		}

		public function store(Request $request) {
			try {
				DB::beginTransaction();
				$resource = new Resource($request->all());
				$resource->save();
				$message = "Registro exitoso";
				DB::commit();
				$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('admin.resource.index')], 'status' => 200, 'route' => route('admin.resource.index'), 'message' => $message, 'type' => 'success'];
				$data = $this->optimize($array);
			} catch (Exception $e) {
				DB::rollBack();
				$message = "Ocurrio un error en el proceso";
				$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $e], 'status' => 422, 'route' => route('admin.resource.create'), 'message' => $message, 'type' => 'error'];
				$data = $this->optimize($array);
			}
			return $data;
		}

		public function show(Request $request, $slug) {
			$resource = Resource::withTrashed()->where(['slug' => $slug])->first();
			if ($resource):
				if ($request->ajax()):
					$data = view('admin.resource._Update');
				else:
					$data = view('admin.resource.Update')->with(['title' => "Actualización del recurso: $resource->name"]);
				endif;
				$data->with(['resource' => $resource]);
			else:
				if ($request->ajax()):
					$data = "Area no existente";
				else:
					session()->flash('improper', "No intentes algo indebido, el resource no existe");
					$data = redirect(route('admin.resource.index'));
				endif;
			endif;
			return $data;
		}

		public function update(Request $request, $slug) {
			$resource = Resource::withTrashed()->where(['slug' => $slug])->first();
			if ($resource):
				try {
					DB::beginTransaction();
					$resource->fill($request->all())->save();
					$message = "Actualizacion exitosa";
					DB::commit();
					$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('admin.resource.index')], 'status' => 200, 'route' => route('admin.resource.index'), 'message' => $message, 'type' => 'success'];
					$data = $this->optimize($array);
				} catch (Exception $e) {
					DB::rollBack();
					$message = "Ocurrio un error en el proceso";
					$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('admin.resource.update', $resource->id), 'message' => $message, 'type' => 'error'];
					$data = $this->optimize($array);
				}
			else:
				$message = "No intentes algo indebido, el recurso no existe";
				$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('admin.resource.index'), 'message' => $message, 'type' => 'improper'];
				$data = $this->optimize($array);
			endif;
			return $data;
		}

		public function delete(Request $request, $slug) {
			if ($request->ajax()):
				$resource = Resource::withTrashed()->where(['slug' => $slug])->first();
				if ($resource):
					try {
						DB::beginTransaction();
						if ($resource->deleted_at):
							$resource->restore();
							$message = "Restauracion exitosa";
						else:
							$resource->delete();
							$message = "Deshabilitación exitosa";
						endif;
						DB::commit();
						session()->flash('success', $message);
						$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('admin.resource.index')], 200);
					} catch (Exception $e) {
						DB::rollBack();
						$data = response()->json(['resp' => false, 'message' => "Ocurrio un error en el proceso"], 422);
					}
				else:
					$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, el recurso no existe"], 422);
				endif;
			else:
				session()->flash('improper', "No intentes algo indebido");
				$data = redirect(route('admin.resource.index'));
			endif;
			return $data;
		}

		public function destroy(Request $request, $slug) {
			if ($request->ajax()):
				$resource = Resource::withTrashed()->where(['slug' => $slug])->first();
				if ($resource):
					try {
						DB::beginTransaction();
						$resource->forceDelete();
						DB::commit();
						$message = "Eliminacion exitosa";
						session()->flash('success', $message);
						$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('admin.resource.index')], 200);
					} catch (Exception $e) {
						DB::rollBack();
						$data = response()->json(['resp' => false, 'message' => "Ocurrio un error en el proceso"], 422);
					}
				else:
					$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, el recurso no existe"], 422);
				endif;
			else:
				session()->flash('improper', "No intentes algo indebido");
				$data = redirect(route('admin.resource.index'));
			endif;
			return $data;
		}

	}
