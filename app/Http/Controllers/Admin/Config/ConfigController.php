<?php

	namespace App\Http\Controllers\Admin\Config;

	//use App\Http\Requests\Admin\Area\CreateRequest;
	use App\Model\Config;
	use Exception;
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use Illuminate\Support\Facades\DB;
	use Yajra\DataTables\DataTables;

	class ConfigController extends Controller {

		public function index() {
			return view('admin.config._index')->with(['title' => 'Panel de configuraciones', 'class_header' => 'page-container-bg-solid']);
		}

		public function table(Request $request) {
			if ($request->ajax()):
				try {
					return DataTables::of(Config::orderBy('id'))->addColumn('link', function ($row){
						$color = ($row->deleted_at) ? 'red' : 'blue';
						$buttons = "<div class='btn-group'><button class='btn $color btn-xs btn-outline dropdown-toggle' data-toggle='dropdown'>Acciones<i class='fa fa-angle-down'></i></button><ul class='dropdown-menu'>";
						if (!$row->deleted_at):
							$buttons .= "<li><a data-url='" . route('admin.config.update', $row->slug) . "' class='btn-ajax'><i class='fa fa-pencil'></i> Mostrar</a></li>";
							//$buttons .= "<li><a data-url='" . route('admin.config.delete', $row->slug) . "' data-message='Desea deshabilitar el rol: " . $row->name . "' class='btn-destroy'><i class='fa fa-low-vision'></i> Deshabilitar</a></li>";
							//$buttons .= "<li><a data-url='" . route('admin.config.destroy', $row->slug) . "' data-message='Desea eliminar el rol: " . $row->name . "' class='btn-destroy'><i class='fa fa-trash'></i> Eliminar</a></li>";
						else:
							//$buttons .= "<li><a data-url='" . route('admin.config.delete', $row->slug) . "' data-message='Desea restaurar el rol: " . $row->name . "' class='btn-destroy'><i class='fa fa-recycle'></i> Restaurar</a></li>";
						endif;
						$buttons .= "</ul></div>";
						return $buttons;
					})->addIndexColumn()->rawColumns(['link'])->make(true);
				} catch (Exception $e) {
					return response()->json(["sEcho" => 1, "iTotalRecords" => 0, "iTotalDisplayRecords" => 0, "aaData" => []]);
				}
			else:
				return redirect()->route('admin.login');
			endif;
		}

		public function create(Request $request) {
			if ($request->ajax()):
				$data = view('admin.config._Create');
			else:
				$data = view('admin.config.Create')->with(['title' => 'Registro de configuración']);
			endif;
			return $data;
		}

		public function store(Request $request) {
			try {
				DB::beginTransaction();
				$config = new Config($request->all());
				$config->save();
				$message = "Registro exitoso";
				DB::commit();
				$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('admin.config.index')], 'status' => 200, 'route' => route('admin.config.index'), 'message' => $message, 'type' => 'success'];
				$data = $this->optimize($array);
			} catch (Exception $e) {
				DB::rollBack();
				$message = "Ocurrio un error en el proceso";
				$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $e], 'status' => 422, 'route' => route('admin.config.create'), 'message' => $message, 'type' => 'error'];
				$data = $this->optimize($array);
			}
			return $data;
		}

		public function show(Request $request, $slug) {
			$config = Config::where(['slug' => $slug])->first();
			if ($config):
				if ($request->ajax()):
					$data = view('admin.config._Update');
				else:
					$data = view('admin.config.Update')->with(['title' => "Actualización de : $config->name"]);
				endif;
				$data->with(['config' => $config]);
			else:
				if ($request->ajax()):
					$data = "Configuración no existente";
				else:
					session()->flash('improper', "No intentes algo indebido, la configuración no existe");
					$data = redirect(route('admin.config.index'));
				endif;
			endif;
			return $data;
		}

		public function update(Request $request, $slug) {
			$config = Config::where(['slug' => $slug])->first();
			if ($config):
				try {
					DB::beginTransaction();
					$config->fill($request->all())->save();
					$message = "Actualizacion exitosa";
					DB::commit();
					$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('admin.config.index')], 'status' => 200, 'route' => route('admin.config.index'), 'message' => $message, 'type' => 'success'];
					$data = $this->optimize($array);
				} catch (Exception $e) {
					DB::rollBack();
					$message = "Ocurrio un error en el proceso";
					$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('admin.config.update', $config->id), 'message' => $message, 'type' => 'error'];
					$data = $this->optimize($array);
				}
			else:
				$message = "No intentes algo indebido, la configuración no existe";
				$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('admin.config.index'), 'message' => $message, 'type' => 'improper'];
				$data = $this->optimize($array);
			endif;
			return $data;
		}

		public function delete(Request $request, $slug) {
			if ($request->ajax()):
				$config = Config::where(['slug' => $slug])->first();
				if ($config):
					try {
						DB::beginTransaction();
						if ($config->deleted_at):
							$config->restore();
							$message = "Restauracion exitosa";
						else:
							$config->delete();
							$message = "Deshabilitación exitosa";
						endif;
						DB::commit();
						session()->flash('success', $message);
						$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('admin.config.index')], 200);
					} catch (Exception $e) {
						DB::rollBack();
						$data = response()->json(['resp' => false, 'message' => "Ocurrio un error en el proceso"], 422);
					}
				else:
					$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, la configuración no existe"], 422);
				endif;
			else:
				session()->flash('improper', "No intentes algo indebido");
				$data = redirect(route('admin.config.index'));
			endif;
			return $data;
		}

		public function destroy(Request $request, $slug) {
			if ($request->ajax()):
				$config = Config::where(['slug' => $slug])->first();
				if ($config):
					try {
						DB::beginTransaction();
						$config->forceDelete();
						DB::commit();
						$message = "Eliminacion exitosa";
						session()->flash('success', $message);
						$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('admin.config.index')], 200);
					} catch (Exception $e) {
						DB::rollBack();
						$data = response()->json(['resp' => false, 'message' => "Ocurrio un error en el proceso"], 422);
					}
				else:
					$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, la configuración no existe"], 422);
				endif;
			else:
				session()->flash('improper', "No intentes algo indebido");
				$data = redirect(route('admin.config.index'));
			endif;
			return $data;
		}

	}
