<?php

	namespace App\Http\Controllers\Developer\Activity;

	use App\Http\Controllers\Controller;
	use App\Model\Activity;
	use App\Model\Config;
	use App\Model\Phase;
	use App\Model\Project;
	use App\Model\Role;
	use App\Model\User;
	use Carbon\Carbon;
	use Exception;
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\DB;
	use Yajra\DataTables\DataTables;

	class ActivityController extends Controller {

		public function index() {
			return view('developer.activity._index')->with(['title' => "Panel de Actividades", 'class_header' => 'page-container-bg-solid']);
		}

		public function table(Request $request) {
			if ($request->ajax()):
				try {
					$config = ['end_date_first' => Config::where(['slug' => 'end_date_first'])->first(), 'end_date_last' => Config::where(['slug' => 'end_date_last'])->first()];
					$user = auth()->guard('developer')->user();
					return DataTables::of(Activity::signaling($request, $config)->range_start($request)->range_end($request)->where(['user_id' => $user->id])->orderByRaw("1*SUBSTRING_INDEX(activity.structure, '.', 1) desc, activity.id desc")->with('phase.project', 'activity'))->addColumn('link', function ($row) {
						$color = ($row->deleted_at) ? 'red' : 'blue';
						$buttons = "<div class='btn-group'><button class='btn $color btn-xs btn-outline dropdown-toggle' data-toggle='dropdown'>Acciones<i class='fa fa-angle-down'></i></button><ul class='dropdown-menu'>";
						$buttons .= "<li><a href='" . route('developer.task.index', [$row->code]) . "'><i class='fa fa-list-ol'></i> Tareas</a></li>";
						$buttons .= "<li><a data-url='" . route('developer.activity.update', [$row->code]) . "' class='btn-ajax'><i class='fa fa-pencil'></i> Mostrar</a></li>";
						$buttons .= "</ul></div>";
						return $buttons;
					})->addColumn('end', function ($row) use ($config) {
						if(!$row->approved):
							$dates = ['now' => Carbon::now('America/Lima'), 'end' => new Carbon($row->end, 'America/Lima')];
							$dates['now'] = new Carbon($dates['now']->toDateString(), 'America/Lima');
							return $this->signaling($config, $dates) . $row->end;
						else:
							return "<i class='fa fa-circle' style='color: #0070fe'></i> $row->approved";
						endif;
					})->addIndexColumn()->rawColumns(['link', 'end'])->make(true);
				} catch (Exception $e) {
					return response()->json(["sEcho" => 1, "iTotalRecords" => 0, "iTotalDisplayRecords" => 0, "aaData" => []]);
				}
			else:
				return redirect()->route('developer.login');
			endif;
		}

		public function show(Request $request, $code) {
			$activity = Activity::where(['code' => $code])->with('phase')->first();
			if ($activity):
				$role = Role::where(['slug' => 'developer'])->first();
				$developers = User::where(['role_id' => $role->id])->pluck('name', 'id');
				$activities = Activity::where(['phase_id' => $activity->phase->id])->orderByRaw("1*SUBSTRING_INDEX(structure, '.', 1) desc, id desc")->first()->pluck('full_name', 'id');
				if ($request->ajax()):
					$data = view('developer.activity._Update');
				else:
					$data = view('developer.activity.Update')->with(['title' => "Informacion de la actividad : $activity->name"]);
				endif;
				$data->with(['activity' => $activity, 'developers' => $developers, 'activities' => $activities]);
			else:
				if ($request->ajax()):
					$data = "Actividad no existente";
				else:
					session()->flash('improper', "No intentes algo indebido, la actividad no existe");
					$data = redirect(route('developer.activity.index'));
				endif;
			endif;
			return $data;
		}

	}
