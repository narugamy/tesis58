<?php

	namespace App\Http\Controllers\Developer\My_Tasks;

	use App\Http\Controllers\Controller;
	use App\Model\Config;
	use App\Model\Task;
	use Carbon\Carbon;
	use Exception;
	use Illuminate\Http\Request;
	use Yajra\DataTables\Facades\DataTables;

	class TaskController extends Controller {

		public function index() {
			return view('developer.my_tasks._index')->with(['title' => "Panel de mis tareas", 'class_header' => 'page-container-bg-solid']);
		}

		public function table(Request $request) {
			if ($request->ajax()):
				try {
					$config = ['end_date_first' => Config::where(['slug' => 'end_date_first'])->first(), 'end_date_last' => Config::where(['slug' => 'end_date_last'])->first()];
					$user = auth()->guard('developer')->user();
					return DataTables::of(Task::withTrashed()->signaling($request, $config)->range_start($request)->range_end($request)->where(['user_id' => $user->id])->orderByRaw("1*SUBSTRING_INDEX(task.structure, '.', 1) asc, task.id asc")->with('activity.phase.project', 'task', 'progress', 'activity.activity'))->addColumn('link', function ($row) {
						$color = ($row->deleted_at) ? 'red' : 'blue';
						$created = $this->authorization($row->start_new);
						$buttons = "<div class='btn-group'><button class='btn $color btn-xs btn-outline dropdown-toggle' data-toggle='dropdown'>Acciones<i class='fa fa-angle-down'></i></button><ul class='dropdown-menu'>";
						if (!$row->deleted_at):
							$buttons .= "<li><a href='" . route('developer.my_tasks.message.index', [$row->code]) . "'><i class='fa fa-comments'></i> Mensajes</a></li>";
							if (empty($row->activity->activity_id)):
								if (!$row->approved):
									if (empty($row->task)):
										$dates = (object)['start' => Carbon::parse($row->start_new), 'now' => Carbon::now()];
										if($dates->now >= $dates->start):
											$buttons .= "<li><a data-url='" . route('developer.my_tasks.progress.create', [$row->code]) . "' class='btn-ajax'><i class='fa fa-arrow-right'></i> Progreso</a></li>";
										endif;
									else:
										if ($row->task->approved):
											$buttons .= "<li><a data-url='" . route('developer.my_tasks.progress.create', [$row->code]) . "' class='btn-ajax'><i class='fa fa-arrow-right'></i> Progreso</a></li>";
										else:
											$buttons .= "<li><a data-url='" . route('developer.my_tasks.progress.create', [$row->code]) . "' class='btn-ajax'><i class='fa fa-arrow-right'></i> Progreso</a></li>";
										endif;
									endif;
								endif;
							else:
								if ($row->activity->activity->approved):
									$buttons .= "<li><a data-url='" . route('developer.my_tasks.progress.create', [$row->code]) . "' class='btn-ajax'><i class='fa fa-arrow-right'></i> Progreso</a></li>";
								endif;
							endif;
							$buttons .= "<li><a data-url='" . route('developer.my_tasks.update', [$row->code]) . "' class='btn-ajax'><i class='fa fa-pencil'></i> Mostrar</a></li>";
						else:
							$buttons .= "<li><a data-url='" . route('developer.my_tasks.delete', [$row->code]) . "' data-message='Desea restaurar la tarea: " . $row->name . "' class='btn-destroy'><i class='fa fa-recycle'></i> Restaurar</a></li>";
						endif;
						$buttons .= "</ul></div>";
						return $buttons;
					})->addColumn('end_new', function ($row) use ($config) {
						if(!$row->approved):
							$dates = ['now' => Carbon::now('America/Lima'), 'end' => new Carbon($row->end_new, 'America/Lima')];
							$dates['now'] = new Carbon($dates['now']->toDateString(), 'America/Lima');
							return $this->signaling($config, $dates) . $row->end_new;
						else:
							return "<i class='fa fa-circle' style='color: #0070fe'></i> $row->approved";
						endif;
					})->addColumn('dependence', function ($row) {
						if ($row->task_id):
							return $row->task->code;
						else:
							return $row->activity->code;
						endif;
					})->addIndexColumn()->rawColumns(['link', 'end_new', 'dependence'])->make(true);
				} catch (Exception $e) {
					return response()->json(["sEcho" => 1, "iTotalRecords" => 0, "iTotalDisplayRecords" => 0, "aaData" => []]);
				}
			else:
				return redirect()->route('developer.login');
			endif;
		}

		public function show(Request $request, $code) {
			$user = auth()->guard('developer')->user();
			$task = Task::withTrashed()->where(['code' => $code, 'user_id' => $user->id])->first();
			if ($task):
				if ($request->ajax()):
					$data = view('developer.my_tasks._Update');
				else:
					$data = view('developer.my_tasks.Update')->with(['title' => "Visualización de la tarea : $task->name"]);
				endif;
				$data->with(['task' => $task]);
			else:
				if ($request->ajax()):
					$data = "Tarea no existente";
				else:
					session()->flash('improper', "No intentes algo indebido, la tarea no existe");
					$data = redirect(route('developer.my_tasks.index'));
				endif;
			endif;
			return $data;
		}

	}
