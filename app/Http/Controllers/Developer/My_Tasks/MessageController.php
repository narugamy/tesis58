<?php

	namespace App\Http\Controllers\Developer\My_Tasks;

	use App\Model\Archive;
	use App\Model\Message;
	use App\Model\Notification;
	use App\Model\Task;
	use Exception;
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use Illuminate\Support\Facades\DB;
	use Illuminate\Support\Facades\File;
	use Illuminate\Support\Facades\Storage;

	class MessageController extends Controller {

		private function generate ($auth, $ids){
			$deletes = Notification::where(['user_id' => $auth->id])->whereIn('message_id', $ids)->pluck('id');
			Notification::whereIn('id', $deletes)->delete();
		}

		public function index($code) {
			$auth = auth()->guard('developer')->user();
			$task = Task::where(['code' => $code, 'user_id' => $auth->id])->first();
			if ($task):
				$messages = Message::where(['table_id' => $task->id, 'table_type' => 'task'])->with('user')->get();
				$this->generate($auth, $messages->pluck('id'));
				return view('developer.my_tasks.Message._Index')->with(['title' => "Mensajes de la tarea $task->code : $task->name", 'messages' => $messages, 'auth' => $auth, 'task' => $task]);
			else:
				return redirect()->route('developer.my_tasks.index');
			endif;
		}

		public function create(Request $request, $code) {
			$auth = auth()->guard('developer')->user();
			$task = Task::where(['code' => $code, 'user_id' => $auth->id])->first();
			if ($task):
				if ($request->ajax()):
					$data = view('developer.my_tasks.Message._Create')->with(['title' => "Mensajes de la tarea $task->code : $task->name", 'auth' => $auth, 'task' => $task]);
				else:
					$data = redirect()->route('developer.my_tasks.message.index');
				endif;
			else:
				if ($request->ajax()):
					$data = "<p>La actividad no existe</p>";
				else:
					$data = redirect()->route('developer.my_tasks.index');
				endif;
			endif;
			return $data;
		}

		public function store(Request $request, $code) {
			$auth = auth()->guard('developer')->user();
			$task = Task::where(['code' => $code, 'user_id' => $auth->id])->with('activity.phase.project.manager')->first();
			if ($task):
				try {
					DB::beginTransaction();
					$alert = new Message($request->all());
					$alert->fill(['user_id' => $auth->id, 'table_id' => $task->id, 'table_type' => 'task', 'employee_id' => $task->activity->phase->project->manager_id])->save();
					$notification = new Notification(['message_id' => $alert->id, 'user_id' => $alert->employee_id, 'route' => route('manager.task.message.index', $task->code)]);
					$notification->save();
					if ($request->hasFile('file')):
						$file = $request->file('file');
						$archive = new Archive();
						$archive->fill(['name' => $file->getClientOriginalName(),
							'encrypt' => time() . $file->getClientOriginalName()]);
						$archive->route = "archive/message/$archive->encrypt";
						Storage::disk('message')->put($archive->encrypt, File::get($file));
						$archive->save();
						$alert->archive_id = $archive->id;
						$alert->save();
					endif;
					DB::commit();
					$message = "Registro exitoso del mensaje";
					$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('developer.my_tasks.message.index', $task->code)], 'status' => 200, 'route' => route('developer.my_tasks.message.index', $task->code), 'message' => $message, 'type' => 'success'];
					$data = $this->optimize($array);
				} catch (Exception $e) {
					DB::rollBack();
					$message = "Ocurrio un error en el proceso";
					$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('developer.my_tasks.message.create', $task->code), 'message' => $message, 'type' => 'error'];
					$data = $this->optimize($array);
				}
			else:
				$message = "La tarea no existe";
				$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('developer.my_tasks.index'), 'message' => $message, 'type' => 'error'];
				$data = $this->optimize($array);
			endif;
			return $data;
		}

	}
