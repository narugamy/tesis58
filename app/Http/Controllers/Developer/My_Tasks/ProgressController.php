<?php

	namespace App\Http\Controllers\Developer\My_Tasks;

	use App\Model\Message;
	use App\Model\Notification;
	use App\Model\Progress;
	use App\Model\Project;
	use App\Model\Task;
	use Carbon\Carbon;
	use Exception;
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use Illuminate\Support\Facades\DB;

	class ProgressController extends Controller {

		public function create(Request $request, $code) {
			if ($request->ajax()):
				$auth = auth()->guard('developer')->user();
				$task = Task::where(['code' => $code, 'user_id' => $auth->id])->with('activity.phase.project')->first();
				if ($task):
					$data = view('developer.my_tasks.Progress._Create')->with(['task' => $task]);
				else:
					$data = "<p>La tarea no existe</p>";
				endif;
			else:
				$data = redirect()->route('developer.my_tasks.index');
			endif;
			return $data;
		}

		public function store(Request $request, $code) {
			$auth = auth()->guard('developer')->user();
			$task = Task::where(['code' => $code, 'user_id' => $auth->id])->with('activity.phase.project')->first();
			if ($task):
				try {
					DB::beginTransaction();
					$progress = new Progress($request->all());
					$progress->fill(['task_id' => $task->id, 'user_id' => $auth->id])->save();
					DB::commit();
					$message = "Registro exitoso del mensaje";
					$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('developer.my_tasks.index')], 'status' => 200, 'route' => route('developer.my_tasks.index'), 'message' => $message, 'type' => 'success'];
					$data = $this->optimize($array);
				} catch (Exception $e) {
					DB::rollBack();
					$message = "Ocurrio un error en el proceso";
					$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('developer.my_tasks.index'), 'message' => $message, 'type' => 'error'];
					$data = $this->optimize($array);
				}
			else:
				$message = "La tarea no existe";
				$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('developer.my_tasks.index'), 'message' => $message, 'type' => 'error'];
				$data = $this->optimize($array);
			endif;
			return $data;
		}

	}
