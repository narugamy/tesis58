<?php

	namespace App\Http\Controllers\Developer\Task;

	use App\Model\Activity;
	use App\Model\Config;
	use App\Model\Role;
	use App\Model\Task;
	use App\Model\User;
	use Carbon\Carbon;
	use Exception;
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use Illuminate\Support\Facades\DB;
	use Yajra\DataTables\Facades\DataTables;

	class TaskController extends Controller {

		public function index($code) {
			$user = auth()->guard('developer')->user();
			$activity = Activity::where(['user_id' => $user->id, 'code' => $code])->with('phase.project')->first();
			if ($activity):
				$create = $this->authorization($activity->phase->project->start);
				return view('developer.task._index')->with(['title' => "Panel de tareas de la actividad N° $activity->code del proyecto: ".$activity->phase->project->name, 'class_header' => 'page-container-bg-solid', 'activity' => $activity, 'created' => $create]);
			endif;
			return redirect()->route('developer.activity.index');
		}

		public function table(Request $request, $code) {
			if ($request->ajax()):
				$user = auth()->guard('developer')->user();
				$activity = Activity::where(['user_id' => $user->id, 'code' => $code])->first();
				if ($activity):
					try {
						$config = ['end_date_first' => Config::where(['slug' => 'end_date_first'])->first(), 'end_date_last' => Config::where(['slug' => 'end_date_last'])->first()];
						return DataTables::of(Task::withTrashed()->signaling($request, $config)->range_start($request)->range_end($request)->where(['activity_id' => $activity->id])->orderByRaw("1*SUBSTRING_INDEX(task.structure, '.', 1) desc, task.id desc")->with('activity.phase.project', 'task', 'progress'))->addColumn('link', function ($row) use ($activity) {
							$color = ($row->deleted_at) ? 'red' : 'blue';
							$buttons = "<div class='btn-group'><button class='btn $color btn-xs btn-outline dropdown-toggle' data-toggle='dropdown'>Acciones<i class='fa fa-angle-down'></i></button><ul class='dropdown-menu'>";
							if (!$row->deleted_at):
								$buttons .= "<li><a data-url='" . route('developer.task.assign', [$activity->code, $row->code]) . "' class='btn-ajax'><i class='fa fa-sign-in'></i> Asignar</a></li>";
								$buttons .= "<li><a data-url='" . route('developer.task.update', [$activity->code, $row->code]) . "' class='btn-ajax'><i class='fa fa-pencil'></i> Mostrar</a></li>";
								$buttons .= "<li><a data-url='" . route('developer.task.delete', [$activity->code, $row->code]) . "' data-message='Desea deshabilitar la tarea: " . $row->name . "' class='btn-destroy'><i class='fa fa-low-vision'></i> Deshabilitar</a></li>";
								$buttons .= "<li><a data-url='" . route('developer.task.destroy', [$activity->code, $row->code]) . "' data-message='Desea eliminar la tarea: " . $row->name . "' class='btn-destroy'><i class='fa fa-trash'></i> Eliminar</a></li>";
							else:
								$buttons .= "<li><a data-url='" . route('developer.task.delete', [$activity->code, $row->code]) . "' data-message='Desea restaurar la tarea: " . $row->name . "' class='btn-destroy'><i class='fa fa-recycle'></i> Restaurar</a></li>";
							endif;
							$buttons .= "</ul></div>";
							return $buttons;
						})->addColumn('end_new', function ($row) use ($config) {
							if(!$row->approved):
								$dates = ['now' => Carbon::now('America/Lima'), 'end' => new Carbon($row->end_new, 'America/Lima')];
								$dates['now'] = new Carbon($dates['now']->toDateString(), 'America/Lima');
								return $this->signaling($config, $dates) . $row->end_new;
							else:
								return "<i class='fa fa-circle' style='color: #0070fe'></i> $row->approved";
							endif;
						})->addIndexColumn()->rawColumns(['link', 'end_new'])->make(true);
					} catch (Exception $e) {
						return response()->json(["sEcho" => 1, "iTotalRecords" => 0, "iTotalDisplayRecords" => 0, "aaData" => []]);
					}
				else:
					return response()->json(["sEcho" => 1, "iTotalRecords" => 0, "iTotalDisplayRecords" => 0, "aaData" => []]);
				endif;
			else:
				return redirect()->route('developer.login');
			endif;
		}

		public function create(Request $request, $code) {
			$user = auth()->guard('developer')->user();
			$activity = Activity::where(['user_id' => $user->id, 'code' => $code])->first();
			if ($activity):
				$tasks = Task::where(['activity_id' => $activity->id])->get()->pluck('full_name', 'id');
				if ($request->ajax()):
					$data = view('developer.task._Create');
				else:
					$data = view('developer.task.Create')->with(['title' => 'Registro de tarea']);
				endif;
				$data->with(['activity' => $activity, 'tasks' => $tasks]);
			else:
				if ($request->ajax()):
					$data = "La actividad no existe";
				else:
					$data = redirect()->route('developer.task.index');
				endif;
			endif;
			return $data;
		}

		public function store(Request $request, $code) {
			$user = auth()->guard('developer')->user();
			$activity = Activity::where(['user_id' => $user->id, 'code' => $code])->first();
			if ($activity):
				try {
					DB::beginTransaction();
					$task = new Task($request->all());
					$task->fill(['activity_id' => $activity->id, 'user_id' => $user->id])->save();
					DB::commit();
					$message = "Registro exitoso del la fase N° $task->code";
					$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('developer.task.index', $activity->code)], 'status' => 200, 'route' => route('developer.task.index', $activity->code), 'message' => $message, 'type' => 'success'];
					$data = $this->optimize($array);
				} catch (Exception $e) {
					DB::rollBack();
					$message = "Ocurrio un error en el proceso";
					$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $e], 'status' => 422, 'route' => route('developer.task.create', $activity->code), 'message' => $message, 'type' => 'error'];
					$data = $this->optimize($array);
				}
			else:
				$data = redirect()->route('developer.project.index');
			endif;
			return $data;
		}

		public function assign(Request $request, $activity_code, $code) {
			if ($request->ajax()):
				$user = auth()->guard('developer')->user();
				$activity = Activity::where(['user_id' => $user->id, 'code' => $activity_code])->first();
				if ($activity):
					$task = Task::withTrashed()->where(['code' => $code, 'activity_id' => $activity->id])->first();
					if ($task):
						$role = Role::where(['slug' => 'developer'])->first();
						$developers = User::where(['role_id' => $role->id])->pluck('name', 'id');
						$data = view('developer.task._Assign')->with(['developers' => $developers, 'activity' => $activity, 'task' => $task]);
					else:
						$data = "Tarea no existente";
					endif;
				else:
					$data = "Actividad no existente";
				endif;
			else:
				session()->flash('improper', "No intentes algo indebido");
				$data = redirect(route('developer.activity.index'));
			endif;
			return $data;
		}

		public function assign_update(Request $request, $activity_code, $code) {
			$user = auth()->guard('developer')->user();
			$activity = Activity::where(['user_id' => $user->id, 'code' => $activity_code])->first();
			if ($activity):
				$task = Task::withTrashed()->where(['code' => $code, 'activity_id' => $activity->id])->first();
				if ($task):
					try {
						DB::beginTransaction();
						$task->fill($request->all())->save();
						$message = "Asignacion exitosa";
						DB::commit();
						$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('developer.task.index', $activity->code)], 'status' => 200, 'route' => route('developer.task.index', $activity->code), 'message' => $message, 'type' => 'success'];
						$data = $this->optimize($array);
					} catch (Exception $e) {
						DB::rollBack();
						$message = "Ocurrio un error en el proceso";
						$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('developer.task.update', $activity->code, $task->code), 'message' => $message, 'type' => 'error'];
						$data = $this->optimize($array);
					}
				else:
					$message = "No intentes algo indebido, la tarea no existe";
					$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('developer.activity.index'), 'message' => $message, 'type' => 'improper'];
					$data = $this->optimize($array);
				endif;
			else:
				$message = "No intentes algo indebido, la actividad no existe";
				$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('developer.activity.index'), 'message' => $message, 'type' => 'improper'];
				$data = $this->optimize($array);
			endif;
			return $data;
		}

		public function show(Request $request, $activity_code, $code) {
			$user = auth()->guard('developer')->user();
			$activity = Activity::where(['user_id' => $user->id, 'code' => $activity_code])->first();
			if ($activity):
				$task = Task::withTrashed()->where(['code' => $code, 'activity_id' => $activity->id])->first();
				$tasks = Task::where(['user_id' => $user->id, 'activity_id' => $activity->id])->get()->pluck('full_name', 'id');
				if ($task):
					if ($request->ajax()):
						$data = view('developer.task._Update');
					else:
						$data = view('developer.task.Update')->with(['title' => "Actualización de la tarea : $task->name"]);
					endif;
					$data->with(['activity' => $activity, 'task' => $task, 'tasks' => $tasks]);
				else:
					if ($request->ajax()):
						$data = "Tarea no existente";
					else:
						session()->flash('improper', "No intentes algo indebido, la tarea no existe");
						$data = redirect(route('developer.activity.index'));
					endif;
				endif;
			else:
				if ($request->ajax()):
					$data = "Area no existente";
				else:
					session()->flash('improper', "No intentes algo indebido, la actividad no existe");
					$data = redirect(route('developer.activity.index'));
				endif;
			endif;
			return $data;
		}

		public function update(Request $request, $activity_code, $code) {
			$user = auth()->guard('developer')->user();
			$activity = Activity::where(['user_id' => $user->id, 'code' => $activity_code])->first();
			if ($activity):
				$task = Task::withTrashed()->where(['code' => $code, 'activity_id' => $activity->id])->first();
				if ($task):
					try {
						DB::beginTransaction();
						$task->fill($request->all());
						$task->fill(['start_new' => $task->start, 'end_new' => $task->end])->save();
						$message = "Actualizacion exitosa";
						DB::commit();
						$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('developer.task.index', $activity->code)], 'status' => 200, 'route' => route('developer.task.index', $activity->code), 'message' => $message, 'type' => 'success'];
						$data = $this->optimize($array);
					} catch (Exception $e) {
						DB::rollBack();
						$message = "Ocurrio un error en el proceso";
						$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('developer.task.update', $activity->code, $task->code), 'message' => $message, 'type' => 'error'];
						$data = $this->optimize($array);
					}
				else:
					$message = "No intentes algo indebido, la tarea no existe";
					$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('developer.activity.index'), 'message' => $message, 'type' => 'improper'];
					$data = $this->optimize($array);
				endif;
			else:
				$message = "No intentes algo indebido, la actividad no existe";
				$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('developer.activity.index'), 'message' => $message, 'type' => 'improper'];
				$data = $this->optimize($array);
			endif;
			return $data;
		}

		public function delete(Request $request, $activity_code, $code) {
			if ($request->ajax()):
				$user = auth()->guard('developer')->user();
				$activity = Activity::where(['user_id' => $user->id, 'code' => $activity_code])->first();
				if ($activity):
					$task = Task::withTrashed()->where(['code' => $code, 'activity_id' => $activity->id])->first();
					if ($task):
						try {
							DB::beginTransaction();
							if ($task->deleted_at):
								$task->restore();
								$message = "Restauracion exitosa";
							else:
								$task->delete();
								$message = "Deshabilitación exitosa";
							endif;
							DB::commit();
							session()->flash('success', $message);
							$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('developer.task.index', $activity->code)], 200);
						} catch (Exception $e) {
							DB::rollBack();
							$data = response()->json(['resp' => false, 'message' => "Ocurrio un error en el proceso"], 422);
						}
					else:
						$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, la tarea no existe"], 422);
					endif;
				else:
					$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, la actividad no existe"], 422);
				endif;
			else:
				session()->flash('improper', "No intentes algo indebido");
				$data = redirect(route('developer.activity.index'));
			endif;
			return $data;
		}

		public function destroy(Request $request, $activity_code, $code) {
			if ($request->ajax()):
				$user = auth()->guard('developer')->user();
				$activity = Activity::where(['user_id' => $user->id, 'code' => $activity_code])->first();
				if ($activity):
					$task = Task::withTrashed()->where(['code' => $code, 'activity_id' => $activity->id])->first();
					if ($task):
						try {
							DB::beginTransaction();
							$task->forceDelete();
							DB::commit();
							$message = "Eliminacion exitosa";
							session()->flash('success', $message);
							$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('developer.task.index', $activity->code)], 200);
						} catch (Exception $e) {
							DB::rollBack();
							$data = response()->json(['resp' => false, 'message' => "Ocurrio un error en el proceso"], 422);
						}
					else:
						$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, la tarea no existe"], 422);
					endif;
				else:
					$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, la actividad no existe"], 422);
				endif;
			else:
				session()->flash('improper', "No intentes algo indebido");
				$data = redirect(route('developer.activity.index'));
			endif;
			return $data;
		}

	}
