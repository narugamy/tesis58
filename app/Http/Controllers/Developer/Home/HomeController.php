<?php

	namespace App\Http\Controllers\Developer\Home;

	use App\Model\Activity;
	use App\Http\Controllers\Controller;
	use App\Model\Notification;
	use Illuminate\Http\Request;

	class HomeController extends Controller {

		public function index() {
			$user = auth()->guard('developer')->user();
			$activities = Activity::where(['user_id' => $user->id])->with('tasks')->get();
			return view('developer.home._index')->with(['title' => 'Panel de desarrollador', 'class_header' => 'page-container-bg-solid', 'activities' => $activities, 'auth' => $user]);
		}

		public function notification(Request $request){
			if($request->ajax()):
			$user = auth()->guard('developer')->user();
			$notifications = Notification::where(['user_id' => $user->id])->with('message')->get();
			$view = view('developer.layout.notification')->with(['notifications' => $notifications])->render();
			return response()->json(['html' => $view, 'notification' => count($notifications)]);
			else:
				return redirect()->route('developer.login');
			endif;
		}

	}
