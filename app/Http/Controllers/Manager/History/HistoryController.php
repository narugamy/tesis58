<?php

	namespace App\Http\Controllers\Manager\History;

	use App\Model\History;
	use Carbon\Carbon;
	use Exception;
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use Yajra\DataTables\DataTables;

	class HistoryController extends Controller {

		public function index() {
			$histories = History::with('task.activity.phase.project')->get();
			return view('manager.history._index')->with(['histories' => $histories, 'title' => 'Lecciones Aprendidas']);
		}

		public function table(Request $request) {
			if ($request->ajax()):
				try {
					return DataTables::of(History::with('task.activity.phase.project')->orderBy('name'))->addColumn('link', function ($row) {
						$color = ($row->deleted_at) ? 'red' : 'blue';
						$buttons = "<div class='btn-group'><button class='btn $color btn-xs btn-outline dropdown-toggle' data-toggle='dropdown'>Acciones<i class='fa fa-angle-down'></i></button><ul class='dropdown-menu'>";
						$buttons .= "<li><a data-url='" . route('manager.history.update', $row->code) . "' class='btn-ajax'><i class='fa fa-pencil'></i> Mostrar</a></li>";
						$buttons .= "</ul></div>";
						return $buttons;
					})->addIndexColumn()->rawColumns(['link'])->make(true);
				} catch (Exception $e) {
					return response()->json(["sEcho" => 1, "iTotalRecords" => 0, "iTotalDisplayRecords" => 0, "aaData" => []]);
				}
			else:
				return redirect()->route('manager.login');
			endif;
		}

		public function show(Request $request, $code) {
			$history = History::where(['code' => $code])->first();
			if ($history):
				if ($request->ajax()):
					$data = view('manager.history._Update')->with(['history' => $history]);
				else:
					$data = redirect(route('manager.project.index'));
				endif;
			else:
				if ($request->ajax()):
					$data = "Historia no existente";
				else:
					session()->flash('improper', "No intentes algo indebido, la historia no existe");
					$data = redirect(route('manager.project.index'));
				endif;
			endif;
			return $data;
		}

	}
