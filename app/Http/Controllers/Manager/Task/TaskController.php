<?php

	namespace App\Http\Controllers\Manager\Task;

	use App\Model\Activity;
	use App\Model\Config;
	use App\Model\Progress;
	use App\Model\Task;
	use Carbon\Carbon;
	use Exception;
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use Illuminate\Support\Facades\DB;
	use Yajra\DataTables\Facades\DataTables;

	class TaskController extends Controller {

		public function index($code) {
			$activity = Activity::where(['code' => $code])->with('phase.project')->first();
			if ($activity):
				return view('manager.task._index')->with(['title' => "Panel de tareas de la actividad N° $activity->code", 'class_header' => 'page-container-bg-solid', 'activity' => $activity]);
			endif;
			return redirect()->route('manager.project.index');
		}

		public function table(Request $request, $code) {
			if ($request->ajax()):
				$activity = Activity::where(['code' => $code])->first();
				if ($activity):
					try {
						$config = ['end_date_first' => Config::where(['slug' => 'end_date_first'])->first(), 'end_date_last' => Config::where(['slug' => 'end_date_last'])->first()];
						return DataTables::of(Task::signaling($request, $config)->range_start($request)->range_end($request)->where(['activity_id' => $activity->id])->orderByRaw("1*SUBSTRING_INDEX(task.structure, '.', 1) desc, task.id desc")->with('activity.phase.project', 'task', 'progress'))->addColumn('link', function ($row) use ($activity) {
							$buttons = "<div class='btn-group'><button class='btn blue btn-xs btn-outline dropdown-toggle' data-toggle='dropdown'>Acciones<i class='fa fa-angle-down'></i></button><ul class='dropdown-menu'>";
							if($row->task_id):
								if($row->task->approved):
									$buttons .= "<li><a data-url='" . route('manager.task.history.create', [$row->code]) . "' class='btn-ajax'><i class='fa fa-flash'></i> Historia Aprendida</a></li>";
									$buttons .= "<li><a href='" . route('manager.task.progress.index', [$row->code]) . "'><i class='fa fa-flash'></i> Progresos</a></li>";
								endif;
							else:
								$buttons .= "<li><a data-url='" . route('manager.task.history.create', [$row->code]) . "' class='btn-ajax'><i class='fa fa-flash'></i> Lección Aprendida</a></li>";
								$buttons .= "<li><a href='" . route('manager.task.progress.index', [$row->code]) . "'><i class='fa fa-flash'></i> Progresos</a></li>";
							endif;
							$buttons .= "<li><a href='" . route('manager.task.resource.index', [$row->code]) . "'><i class='fa fa-shopping-cart'></i> Recursos</a></li>";
							$buttons .= "<li><a href='" . route('manager.task.message.index', [$row->code]) . "'><i class='fa fa-comments-o'></i> Mensajes</a></li>";
							$buttons .= "<li><a data-url='" . route('manager.task.update', [$activity->code, $row->code]) . "' class='btn-ajax'><i class='fa fa-pencil'></i> Mostrar</a></li>";
							$buttons .= "</ul></div>";
							return $buttons;
						})->addColumn('end_new', function ($row) use ($config) {
							if(!$row->approved):
								$dates = ['now' => Carbon::now('America/Lima'), 'end' => new Carbon($row->end_new, 'America/Lima')];
								$dates['now'] = new Carbon($dates['now']->toDateString(), 'America/Lima');
								return $this->signaling($config, $dates) . $row->end_new;
							else:
								return "<i class='fa fa-circle' style='color: #0070fe'></i> $row->approved";
							endif;
						})->addColumn('dependence', function ($row) {
							if($row->task_id):
								return $row->task->code;
							else:
								return $row->activity->code;
							endif;
						})->addColumn('progress_bar', function ($row) {
							if($row->progress):
								$progress = Progress::where(['task_id' => $row->id])->where('id', '>', $row->progress->id)->first();
								return ($progress)? 'Si':'No';
							else:
								$progress = Progress::where(['task_id' => $row->id])->first();
								return ($progress)? 'Si':'No';
							endif;
						})->addIndexColumn()->rawColumns(['link', 'end_new'])->make(true);
					} catch (Exception $e) {
						return response()->json(["sEcho" => 1, "iTotalRecords" => 0, "iTotalDisplayRecords" => 0, "aaData" => []]);
					}
				else:
					return response()->json(["sEcho" => 1, "iTotalRecords" => 0, "iTotalDisplayRecords" => 0, "aaData" => []]);
				endif;
			else:
				return redirect()->route('manager.login');
			endif;
		}

		public function show(Request $request, $activity_code, $code) {
			$activity = Activity::where(['code' => $activity_code])->first();
			if ($activity):
				$task = Task::withTrashed()->where(['code' => $code, 'activity_id' => $activity->id])->first();
				if ($task):
					if ($request->ajax()):
						$data = view('manager.task._Update');
					else:
						$data = view('manager.task.Update')->with(['title' => "Actualización de la tarea : $task->name"]);
					endif;
					$data->with(['activity' => $activity, 'task' => $task]);
				else:
					if ($request->ajax()):
						$data = "Tarea no existente";
					else:
						session()->flash('improper', "No intentes algo indebido, la tarea no existe");
						$data = redirect(route('manager.activity.index'));
					endif;
				endif;
			else:
				if ($request->ajax()):
					$data = "Area no existente";
				else:
					session()->flash('improper', "No intentes algo indebido, la actividad no existe");
					$data = redirect(route('manager.activity.index'));
				endif;
			endif;
			return $data;
		}

	}
