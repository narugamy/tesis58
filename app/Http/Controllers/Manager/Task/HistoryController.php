<?php

	namespace App\Http\Controllers\Manager\Task;

	use App\Model\History;
	use App\Model\Task;
	use Exception;
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use Illuminate\Support\Facades\DB;

	class HistoryController extends Controller {

		public function create($code){
			$task = Task::where(['code' => $code])->with('history')->first();
			if($task):
				return view('manager.task.History._Create')->with(['task' => $task]);
			else:
				return redirect()->back();
			endif;
		}

		public function store(Request $request, $code){
			$task = Task::where(['code' => $code])->with('history', 'activity')->first();
			if($task):
				try {
					DB::beginTransaction();
					if(empty($task->history)):
						$history = new History($request->all());
						$history->fill(['task_id' => $task->id])->save();
						$message = "Registro exitoso de la Leccion N° $history->code";
					else:
						$task->history->name = $request->name;
						$task->history->description = $request->description;
						$task->history->save();
						$message = "Actualizacion exitosa de la historia N° ".$task->history->code;
					endif;
					DB::commit();
					$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('manager.task.index', $task->activity->code)], 'status' => 200, 'route' => route('manager.task.index', $task->activity->code), 'message' => $message, 'type' => 'success'];
					$data = $this->optimize($array);
				} catch (Exception $e) {
					DB::rollBack();
					$message = "Ocurrio un error en el proceso";
					$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $e], 'status' => 422, 'route' => route('manager.task.history.create', $task->code), 'message' => $message, 'type' => 'error'];
					$data = $this->optimize($array);
				}
			else:
				$data = redirect()->back();
			endif;
			return $data;
		}

	}
