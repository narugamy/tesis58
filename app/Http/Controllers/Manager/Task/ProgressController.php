<?php

	namespace App\Http\Controllers\Manager\Task;

	use App\Model\Activity;
	use App\Model\Progress;
	use App\Model\Task;
	use Carbon\Carbon;
	use Exception;
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use Illuminate\Support\Facades\DB;
	use Yajra\DataTables\DataTables;

	class ProgressController extends Controller {

		public function index(Request $request, $code) {
			$task = Task::where(['code' => $code])->with('activity.phase.project', 'progresses')->first();
			if ($task):
				$data = view('manager.task.progress._index')->with(['task' => $task, 'title' => "Progreso de la tarea: $task->name"]);
			else:
				$message = "Ocurrio un error en el proceso";
				$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('developer.my_tasks.index'), 'message' => $message, 'type' => 'error'];
				$data = $this->optimize($array);
			endif;
			return $data;
		}

		public function table(Request $request, $code) {
			if ($request->ajax()):
				$task = Task::where(['code' => $code])->first();
				if ($task):
					$approved = Progress::where(['task_id' => $task->id])->whereNotNull('approved')->first();
					try {
						return DataTables::of(Progress::where(['task_id' => $task->id]))->addColumn('link', function ($row) use ($task, $approved) {
							if (!$row->approved):
								$color = ($row->deleted_at) ? 'red' : 'blue';
								$buttons = "<div class='btn-group'><button class='btn $color btn-xs btn-outline dropdown-toggle' data-toggle='dropdown'>Acciones<i class='fa fa-angle-down'></i></button><ul class='dropdown-menu'>";
								if ($approved):
									if ($approved->id < $row->id && (float)$approved->progress < 100):
										$buttons .= "<li><a data-url='" . route('manager.task.progress.index', [$task->code, $row->id]) . "' data-message='Desea aprobar el progreso: " . $row->progress . "' class='btn-destroy'><i class='fa fa-pencil'></i> Aprobar</a></li>";
									endif;
								else:
									$buttons .= "<li><a data-url='" . route('manager.task.progress.approved', [$task->code, $row->id]) . "' data-message='Desea aprobar el progreso: " . $row->progress . "' class='btn-destroy'><i class='fa fa-pencil'></i> Aprobar</a></li>";
								endif;
								$buttons .= "</ul></div>";
								if ($approved):
									if ($approved->id < $row->id && (float)$approved->progress < 100):
										$data = $buttons;
									else:
										$data = '';
									endif;
								else:
									$data = $buttons;
								endif;
							else:
								$data = 'Aprobado';
							endif;
							return $data;
						})->addIndexColumn()->rawColumns(['link'])->make(true);
					} catch (Exception $e) {
						return response()->json(["sEcho" => 1, "iTotalRecords" => 0, "iTotalDisplayRecords" => 0, "aaData" => []]);
					}
				else:
					return response()->json(["sEcho" => 1, "iTotalRecords" => 0, "iTotalDisplayRecords" => 0, "aaData" => []]);
				endif;
			else:
				return redirect()->route('manager.login');
			endif;
		}

		public function store(Request $request, $code, $id) {
			$task = Task::where(['code' => $code])->with('tasks', 'parent', 'activity.activities', 'activity')->first();
			if ($task):
				$progress = Progress::where(['task_id' => $task->id])->find($id);
				try {
					DB::beginTransaction();
					$date = date('Y-m-d');
					Progress::where(['task_id' => $task->id])->update(['approved' => null]);
					$progress->fill(['approved' => $date])->save();
					if ((float)$progress->progress == 100):
						$dates = (object)['start' => Carbon::parse($task->start_new), 'approved' => Carbon::parse($date), 'end' => Carbon::parse($task->end_new)];
						if ($dates->approved > $dates->end):
							$task->fill(['approved' => $dates->approved->toDateString()])->save();
							$days = $dates->approved->diffInWeekdays($dates->end);
							if (count($task->tasks) > 0):
								$task->tasks = $this->updater($task->tasks, $days);
							endif;
						elseif($dates->approved >= $dates->start && $dates->approved <= $dates->end):
							if($dates->approved < $dates->end):
								$days = $dates->end->diffInWeekdays($dates->approved,false);
								$task->fill(['approved' => $dates->approved->toDateString(), 'end_new' => $dates->end->addWeekdays($days)->toDateString()])->save();
								if (count($task->tasks) > 0):
									$task->tasks = $this->updater($task->tasks, $days);
								endif;
							endif;
						endif;
					endif;
					DB::commit();
					$message = "Aprobacion exitosa";
					session()->flash('success', $message);
					$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('manager.task.progress.index', $task->code)], 200);
				} catch (Exception $e) {
					DB::rollBack();
					$data = response()->json(['resp' => false, 'message' => "Ocurrio un error en el proceso"], 422);
				}
			else:
				$message = "La tarea no existe";
				$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('developer.my_tasks.index'), 'message' => $message, 'type' => 'error'];
				$data = $this->optimize($array);
			endif;
			return $data;
		}

	}
