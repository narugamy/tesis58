<?php

	namespace App\Http\Controllers\Manager\Task;

	use App\Model\Container;
	use App\Model\Project;
	use App\Model\Resource;
	use App\Model\Task;
	use Exception;
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use Illuminate\Support\Facades\DB;
	use Yajra\DataTables\Facades\DataTables;

	class ResourceController extends Controller {

		public function index($code) {
			$task = Task::where(['code' => $code])->with('activity.phase.project')->first();
			if ($task):
				$create = $this->authorization($task->start);
				return view('manager.task.resource._index')->with(['title' => "Panel de recursos de la tarea: $task->code", 'class_header' => 'page-container-bg-solid', 'task' => $task, 'created' => $create]);
			else:
				return redirect()->route('manager.project.index');
			endif;
		}

		public function table(Request $request, $code) {
			if ($request->ajax()):
				$task = Task::where(['code' => $code])->first();
				if ($task):
					try {
						return DataTables::of(Container::where(['task_id' => $task->id])->with('resource')->get())->addColumn('link', function ($row) use ($task) {
							$color = ($row->deleted_at) ? 'red' : 'blue';
							$buttons = "<div class='btn-group'><button class='btn $color btn-xs btn-outline dropdown-toggle' data-toggle='dropdown'>Acciones<i class='fa fa-angle-down'></i></button><ul class='dropdown-menu'>";
							if (!$row->deleted_at):
								$buttons .= "<li><a data-url='" . route('manager.task.resource.update', [$task->code, $row->id]) . "' class='btn-ajax'><i class='fa fa-pencil'></i> Mostrar</a></li>";
								$buttons .= "<li><a data-url='" . route('manager.task.resource.destroy', [$task->code, $row->id]) . "' data-message='Desea eliminar el recurso: " . $row->resource->name . "' class='btn-destroy'><i class='fa fa-trash'></i> Eliminar</a></li>";
							endif;
							$buttons .= "</ul></div>";
							return $buttons;
						})->addIndexColumn()->rawColumns(['link'])->make(true);
					} catch (Exception $e) {
						return response()->json(["sEcho" => 1, "iTotalRecords" => 0, "iTotalDisplayRecords" => 0, "aaData" => []]);
					}
				else:
					return response()->json(["sEcho" => 1, "iTotalRecords" => 0, "iTotalDisplayRecords" => 0, "aaData" => []]);
				endif;
			else:
				return redirect()->route('manager.login');
			endif;
		}

		public function create(Request $request, $code) {
			$task = Task::where(['code' => $code])->first();
			if ($task):
				$resources = Resource::pluck('name', 'id');
				if ($request->ajax()):
					$data = view('manager.task.resource._Create');
				else:
					$data = view('manager.task.resource.Create')->with(['title' => 'Registro de recurso']);
				endif;
				$data->with(['resources' => $resources, 'task' => $task]);
			else:
				if ($request->ajax()):
					$data = 'La tarea no existe';
				else:
					$data = redirect()->route('manager.project.index');
				endif;
			endif;
			return $data;
		}

		public function store(Request $request, $code) {
			$task = Task::where(['code' => $code])->first();
			if ($task):
				try {
					DB::beginTransaction();
					$container = new Container($request->all());
					$container->fill(['total' => ((int)$container->number * (float)$container->price), 'task_id' => $task->id])->save();
					$message = "Registro exitoso";
					DB::commit();
					$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('manager.task.resource.index', $task->code)], 'status' => 200, 'route' => route('manager.task.resource.index', $task->code), 'message' => $message, 'type' => 'success'];
					$data = $this->optimize($array);
				} catch (Exception $e) {
					DB::rollBack();
					$message = "Ocurrio un error en el proceso";
					$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('manager.task.resource.index', $task->code), 'message' => $message, 'type' => 'error'];
					$data = $this->optimize($array);
				}
			else:
				$message = "No existe la tarea";
				$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('manager.project.index'), 'message' => $message, 'type' => 'error'];
				$data = $this->optimize($array);
			endif;
			return $data;
		}

		public function show(Request $request, $code, $container_id) {
			$task = Task::where(['code' => $code])->first();
			if ($task):
				$container = Container::where(['task_id' => $task->id, 'id' => $container_id])->first();
				$resources = Resource::pluck('name', 'id');
				if ($request->ajax()):
					$data = view('manager.task.resource._Update');
				else:
					$data = view('manager.task.resource.Update')->with(['title' => "Actualización del contenedor de la tarea: $task->name"]);
				endif;
				$data->with(['task' => $task, 'resources' => $resources, 'container' => $container]);
			else:
				if ($request->ajax()):
					$data = "Proyecto no existente";
				else:
					session()->flash('improper', "No intentes algo indebido, la tarea no existe");
					$data = redirect(route('manager.project.index'));
				endif;
			endif;
			return $data;
		}

		public function update(Request $request, $code, $container_id) {
			$task = Task::withTrashed()->where(['code' => $code])->first();
			if ($task):
				$container = Container::find($container_id);
				if ($container):
					try {
						DB::beginTransaction();
						$container->fill($request->all());
						$container->total = ((int)$container->number * (float)$container->price);
						$container->save();
						$message = "Actualizacion exitosa";
						DB::commit();
						$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('manager.task.resource.index', $task->code)], 'status' => 200, 'route' => route('manager.task.resource.index', $task->code), 'message' => $message, 'type' => 'success'];
						$data = $this->optimize($array);
					} catch (Exception $e) {
						DB::rollBack();
						$message = "Ocurrio un error en el proceso";
						$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('manager.task.resource.update', [$task->id, $container->id]), 'message' => $message, 'type' => 'error'];
						$data = $this->optimize($array);
					}
				else:
					$message = "No intentes algo indebido, el contenedor no existe";
					$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('manager.task.resource.index', $task->code), 'message' => $message, 'type' => 'improper'];
					$data = $this->optimize($array);
				endif;
			else:
				$message = "No intentes algo indebido, la tarea no existe";
				$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('manager.project.index'), 'message' => $message, 'type' => 'improper'];
				$data = $this->optimize($array);
			endif;
			return $data;
		}

		public function destroy(Request $request, $code, $container_id) {
			if ($request->ajax()):
				$task = Task::withTrashed()->where(['code' => $code])->first();
				if ($task):
					$container = Container::where(['id' => $container_id, 'task_id' => $task->id])->first();
					if ($container):
						try {
							DB::beginTransaction();
							$container->forceDelete();
							DB::commit();
							$message = "Eliminacion exitosa";
							session()->flash('success', $message);
							$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('manager.task.resource.index', $task->code)], 200);
						} catch (Exception $e) {
							DB::rollBack();
							$data = response()->json(['resp' => false, 'message' => $e], 422);
						}
					else:
						$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, el contanedor no existe"], 422);endif;
				else:
					$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, la tarea no existe"], 422);
				endif;
			else:
				session()->flash('improper', "No intentes algo indebido");
				$data = redirect(route('manager.project.index'));
			endif;
			return $data;
		}

	}
