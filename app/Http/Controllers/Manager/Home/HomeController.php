<?php

	namespace App\Http\Controllers\Manager\Home;

	use App\Model\Notification;
	use App\Model\Project;
	use App\Model\Task;
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use Illuminate\Support\Facades\Auth;

	class HomeController extends Controller {

		public function index() {
			$auth = auth()->guard('manager')->user();
			$projects = Project::where(['manager_id' => $auth->id])->with('phases.activities.tasks')->get();
			return view('manager.home._index')->with(['title' => 'Panel de Jefe de Proyectos', 'class_header' => 'page-container-bg-solid', 'projects' => $projects]);
		}

		public function notification(Request $request){
			if($request->ajax()):
				$user = auth()->guard('manager')->user();
				$notifications = Notification::where(['user_id' => $user->id])->with('message')->get();
				$view = view('developer.layout.notification')->with(['notifications' => $notifications])->render();
				return response()->json(['html' => $view, 'notification' => count($notifications)]);
			else:
				return redirect()->route('developer.login');
			endif;
		}

	}
