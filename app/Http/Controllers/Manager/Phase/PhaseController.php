<?php

	namespace App\Http\Controllers\Manager\Phase;

	use App\Http\Controllers\Controller;
	use App\Model\Config;
	use App\Model\Phase;
	use App\Model\Project;
	use Carbon\Carbon;
	use Exception;
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\DB;
	use Yajra\DataTables\DataTables;

	class PhaseController extends Controller {

		public function index($code) {
			$project = Project::whereCode($code)->first();
			if ($project):
				$create = $this->authorization($project->start);
				return view('manager.phase._index')->with(['title' => "Panel de Fases del proyecto N° $project->code", 'class_header' => 'page-container-bg-solid', 'project' => $project, 'created' => $create]);
			endif;
			return redirect()->route('manager.project.index');
		}

		public function table(Request $request, $code) {
			$project = Project::whereCode($code)->first();
			if ($request->ajax()):
				if ($project):
					try {
						$config = ['end_date_first' => Config::where(['slug' => 'end_date_first'])->first(), 'end_date_last' => Config::where(['slug' => 'end_date_last'])->first()];
						return DataTables::of(Phase::with('project')->withTrashed()->signaling($request, $config)->range_start($request)->range_end($request)->where(['project_id' => $project->id])->orderBy('name'))->addColumn('link', function ($row) use ($project) {
							$color = ($row->deleted_at) ? 'red' : 'blue';
							$buttons = "<div class='btn-group'><button class='btn $color btn-xs btn-outline dropdown-toggle' data-toggle='dropdown'>Acciones<i class='fa fa-angle-down'></i></button><ul class='dropdown-menu'>";
							if (!$row->deleted_at):
								$buttons .= "<li><a href='" . route('manager.activity.index', $row->code) . "'><i class='fa fa-list-ol'></i> Actividades</a></li>";
								$buttons .= "<li><a data-url='" . route('manager.phase.update', [$project->code, $row->code]) . "' class='btn-ajax'><i class='fa fa-pencil'></i> Mostrar</a></li>";
								$buttons .= "<li><a data-url='" . route('manager.phase.delete', [$project->code, $row->code]) . "' data-message='Desea deshabilitar el proyecto: " . $row->name . "' class='btn-destroy'><i class='fa fa-low-vision'></i> Deshabilitar</a></li>";
								$buttons .= "<li><a data-url='" . route('manager.phase.destroy', [$project->code, $row->code]) . "' data-message='Desea eliminar el proyecto: " . $row->name . "' class='btn-destroy'><i class='fa fa-trash'></i> Eliminar</a></li>";
							else:
								$buttons .= "<li><a data-url='" . route('manager.phase.delete', [$project->code, $row->code]) . "' data-message='Desea restaurar el proyecto: " . $row->name . "' class='btn-destroy'><i class='fa fa-recycle'></i> Restaurar</a></li>";
							endif;
							$buttons .= "</ul></div>";
							return $buttons;
						})->addColumn('end', function ($row) use ($config){
							if(!$row->project->finalized):
								$dates = ['now' => Carbon::now('America/Lima'), 'end' => new Carbon($row->end, 'America/Lima')];
								$dates['now'] = new Carbon($dates['now']->toDateString(), 'America/Lima');
								return $this->signaling($config, $dates).$row->end;
							else:
								return "<i class='fa fa-circle' style='color: #0070fe'></i> ".$row->project->finalized;
							endif;
						})->addIndexColumn()->rawColumns(['link', 'end'])->make(true);
					} catch (Exception $e) {
						return response()->json(["sEcho" => 1, "iTotalRecords" => 0, "iTotalDisplayRecords" => 0, "aaData" => []]);
					}
				else:
					return response()->json(["sEcho" => 1, "iTotalRecords" => 0, "iTotalDisplayRecords" => 0, "aaData" => []]);
				endif;
			else:
				if ($project):
					return redirect()->route('manager.project.index');
				endif;
				return redirect()->route('manager.login');
			endif;
		}

		public function create(Request $request, $code) {
			$project = Project::whereCode($code)->first();
			if ($project):
				if ($request->ajax()):
					$data = view('manager.phase._Create');
				else:
					$data = view('manager.phase.Create')->with(['title' => 'Registro de empresa']);
				endif;
				$data->with(['project' => $project]);
			else:
				$data = redirect()->route('manager.project.index');
			endif;
			return $data;
		}

		public function store(Request $request, $code) {
			$project = Project::whereCode($code)->first();
			if ($project):
				try {
					DB::beginTransaction();
					$phase = new Phase($request->all());
					$phase->fill(['project_id' => $project->id])->save();
					DB::commit();
					$message = "Registro exitoso del la fase N° $phase->code";
					$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('manager.phase.index', $project->code)], 'status' => 200, 'route' => route('manager.phase.index', $project->code), 'message' => $message, 'type' => 'success'];
					$data = $this->optimize($array);
				} catch (Exception $e) {
					DB::rollBack();
					$message = "Ocurrio un error en el proceso";
					$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $e], 'status' => 422, 'route' => route('manager.phase.create'), 'message' => $message, 'type' => 'error'];
					$data = $this->optimize($array);
				}
			else:
				$data = redirect()->route('manager.project.index');
			endif;
			return $data;
		}

		public function show(Request $request, $project_code, $code) {
			$project = Project::withTrashed()->whereCode($project_code)->first();
			if ($project):
				$phase = Phase::withTrashed()->where(['project_id' => $project->id, 'code' => $code])->first();
				if ($phase):
					if ($request->ajax()):
						$data = view('manager.phase._Update');
					else:
						$data = view('manager.phase.Update')->with(['title' => "Actualización de la fase : $phase->name"]);
					endif;
					$data->with(['project' => $project, 'phase' => $phase]);
				else:
					if ($request->ajax()):
						$data = "Area no existente";
					else:
						session()->flash('improper', "No intentes algo indebido, la fase no existe");
						$data = redirect(route('manager.phase.index', $project->code));
					endif;
				endif;
			else:
				if ($request->ajax()):
					$data = "Area no existente";
				else:
					session()->flash('improper', "No intentes algo indebido, el proyecto no existe");
					$data = redirect(route('manager.project.index'));
				endif;
			endif;
			return $data;
		}

		public function update(Request $request, $project_code, $code) {
			$project = Project::withTrashed()->whereCode($project_code)->first();
			if ($project):
				$phase = Phase::withTrashed()->where(['project_id' => $project->id, 'code' => $code])->first();
				if ($phase):
					try {
						DB::beginTransaction();
						$phase->fill($request->all())->save();
						$message = "Actualizacion exitosa";
						DB::commit();
						$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('manager.phase.index', $project->code, $phase->code)], 'status' => 200, 'route' => route('manager.phase.index', $project->code, $phase->code), 'message' => $message, 'type' => 'success'];
						$data = $this->optimize($array);
					} catch (Exception $e) {
						DB::rollBack();
						$message = "Ocurrio un error en el proceso";
						$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('manager.phase.update', $project->code, $phase->code), 'message' => $message, 'type' => 'error'];
						$data = $this->optimize($array);
					}
				else:
					$message = "No intentes algo indebido, la fase no existe";
					$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('manager.phase.index', $project->code), 'message' => $message, 'type' => 'improper'];
					$data = $this->optimize($array);
				endif;
			else:
				$message = "No intentes algo indebido, el proyecto no existe";
				$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('manager.project.index'), 'message' => $message, 'type' => 'improper'];
				$data = $this->optimize($array);
			endif;
			return $data;
		}

		public function delete(Request $request, $project_code, $code) {
			if ($request->ajax()):
				$project = Project::withTrashed()->whereCode($project_code)->first();
				if ($project):
					$phase = Phase::withTrashed()->where(['project_id' => $project->id, 'code' => $code])->first();
					if ($phase):
						try {
							DB::beginTransaction();
							if ($phase->deleted_at):
								$phase->restore();
								$message = "Restauracion exitosa";
							else:
								$phase->delete();
								$message = "Deshabilitación exitosa";
							endif;
							DB::commit();
							session()->flash('success', $message);
							$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('manager.phase.index', $project->code)], 200);
						} catch (Exception $e) {
							DB::rollBack();
							$data = response()->json(['resp' => false, 'message' => "Ocurrio un error en el proceso"], 422);
						}
					else:
						$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, la fase no existe"], 422);
					endif;
				else:
					$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, el proyecto no existe"], 422);
				endif;
			else:
				session()->flash('improper', "No intentes algo indebido");
				$data = redirect(route('manager.phase.index'));
			endif;
			return $data;
		}

		public function destroy(Request $request, $project_code, $code) {
			if ($request->ajax()):
				$project = Project::withTrashed()->whereCode($project_code)->first();
				if ($project):
					$phase = Phase::withTrashed()->where(['project_id' => $project->id, 'code' => $code])->first();
					if ($phase):
						try {
							DB::beginTransaction();
							$phase->forceDelete();
							DB::commit();
							$message = "Eliminacion exitosa";
							session()->flash('success', $message);
							$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('manager.phase.index', $project->code)], 200);
						} catch (Exception $e) {
							DB::rollBack();
							$data = response()->json(['resp' => false, 'message' => "Ocurrio un error en el proceso"], 422);
						}
					else:
						$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, la fase no existe"], 422);
					endif;
				else:
					$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, el proyecto no existe"], 422);
				endif;
			else:
				session()->flash('improper', "No intentes algo indebido");
				$data = redirect(route('manager.project.index'));
			endif;
			return $data;
		}

	}
