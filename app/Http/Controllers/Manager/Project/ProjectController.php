<?php

	namespace App\Http\Controllers\Manager\Project;

	use App\Model\Config;
	use App\Model\Project;
	use App\Model\Resource;
	use App\Model\Role;
	use App\Model\Task;
	use App\Model\Type;
	use App\Model\User;
	use Carbon\Carbon;
	use Exception;
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use Illuminate\Support\Facades\DB;
	use Yajra\DataTables\DataTables;

	class ProjectController extends Controller {

		public function index() {
			return view('manager.project._index')->with(['title' => 'Panel de proyectos', 'class_header' => 'page-container-bg-solid']);
		}

		public function gant($code) {
			$auth = auth()->guard('manager')->user();
			$project = Project::where(['code' => $code, 'manager_id' => $auth->id])->with('phases.activities.activity', 'phases.activities.tasks.progress', 'phases.activities.tasks.task', 'phases.activities.tasks.father')->first();
			if ($project):
				return view('manager.project._gant')->with(['project' => $this->days($project), 'title' => 'Diagrama de Gant']);
			else:
				return redirect()->route('manager.project.index');
			endif;
		}

		public function costs($code) {
			$auth = auth()->guard('manager')->user();
			$project = Project::where(['code' => $code, 'manager_id' => $auth->id])->first();
			if ($project):
				$tasks = Task::Join('activity', 'task.activity_id', '=', 'activity.id')->Join('phase', 'activity.phase_id', '=', 'phase.id')->Join('project', 'phase.project_id', '=', 'project.id')->where(['project.code' => $code])->select('task.*')->with('progress', 'containers')->get();
				$temp = $this->get_days($tasks, $project);
				$tasks = $temp['tasks'];
				foreach ($tasks as $task):
					$task->pv = $task->diff_new * $temp['price_day'];
					$task->rest = round(1 - ($task->progressive / 100), 2);
					$task->ev = $task->pv - ((float)$task->pv * (float)$task->rest);
				endforeach;
				return view('manager.project.schedule')->with(['title' => 'Indice del desempeño del cronograma', 'tasks' => $tasks, 'days_new' => $temp['days_new'], 'price_day' => $temp['price_day']]);
			else:
				return redirect()->route('manager.project.index');
			endif;
		}

		private function get_cost($project){
			$tasks = Task::Join('activity', 'task.activity_id', '=', 'activity.id')->join('phase', 'activity.phase_id', '=', 'phase.id')->join('project', 'phase.project_id', '=', 'project.id')->where(['project.code' => $project->code])->select('task.*')->with('progress', 'father', 'containers', 'activity')->get();
			$temp = $this->get_days($tasks, $project);
			$tasks = $temp['tasks'];
			$count = ['ev' => 0, 'ac' => 0];
			foreach ($tasks as $task):
				$dates = (object)['start' => Carbon::parse($task->start), 'end' => Carbon::parse($task->end), 'start_new' => Carbon::parse($task->start_new), 'end_new' => Carbon::parse($task->end_new), 'now' => Carbon::now('America/Lima'),];
				if ($task->approved):
					$task->counter = true;
					$task->pv = $task->diff * $temp['price_day'];
					$task->ev = ($task->progressive * $task->pv) / 100;
					$task->ac = ($task->diff_new * $temp['price_day']);
					$task->indice = ($task->ev === 0) ? 0 : ($task->ev / $task->ac);
				else:
					if ($task->father !== null):
						if ($task->father->counter):
							if ($dates->now < $dates->start_new):
								$task->counter = false;
								$task->pv = $task->diff_new * $temp['price_day'];
								$task->ve = null;
								$task->ac = null;
								$task->indice = null;
							else:
								$task->counter = true;
								if ($dates->now >= $dates->start_new && $dates->now <= $dates->end_new):
									$last = $dates->now->diffInWeekdays($dates->start_new);
									$task->pv = $task->diff * $temp['price_day'];
									$task->ev = $task->progressive * (($task->pv) / 100);
									$task->ac = ($last * $temp['price_day']) + $task->resource;
									$task->indice = ($task->ev / $task->ac);
								else:
									$task->diff_new = $dates->now->diffInWeekdays($dates->start_new);
									$task->pv = $task->diff * $temp['price_day'];
									$task->ev = $task->progressive * (($task->pv) / 100);
									$task->ac = ($task->diff_new * $temp['price_day']);
									$task->indice = ($task->ev === 0) ? 0 : ($task->ev / $task->pv);
								endif;
							endif;
						else:
							$task->counter = false;
						endif;
					else:
						if ($dates->now < $dates->start_new):
							$task->counter = false;
							$task->pv = $task->diff * $temp['price_day'];
							$task->ve = null;
							$task->ac = null;
							$task->indice = null;
						else:
							$task->counter = true;
							if ($dates->now >= $dates->start_new && $dates->now <= $dates->end_new):
								$last = $dates->now->diffInWeekdays($dates->start_new) ;
								$task->pv = $task->diff * $temp['price_day'];
								$task->ev = $task->progressive * (($task->pv) / 100);
								$task->ac = ($last * $temp['price_day']) + $task->resource;
								$task->indice = ($task->ev / $task->ac);
							else:
								$task->diff_new = $dates->now->diffInWeekdays($dates->start_new);
								$task->pv = $task->diff * $temp['price_day'];
								$task->ev = $task->progressive * (($task->pv) / 100);
								$task->ac = ($task->diff_new * $temp['price_day']);
								$task->indice = ($task->ev === 0) ? 0 : ($task->ev / $task->pv);
							endif;
						endif;
					endif;
				endif;
				if ($task->counter):
					$count['ev'] += $task->ev;
					$count['ac'] += $task->ac;
				endif;
			endforeach;
			$mean = ($count['ac'] === 0)? 0:round($count['ev'] / $count['ac'], 2);
			return ['mean' => $mean, 'tasks' => $tasks];
		}

		private function get_schedule($project){
			$tasks = Task::join('activity', 'task.activity_id', '=', 'activity.id')->join('phase', 'activity.phase_id', '=', 'phase.id')->join('project', 'phase.project_id', '=', 'project.id')->where(['project.code' => $project->code])->select('task.*')->with('progress', 'father', 'containers', 'activity')->get();
			$temp = $this->get_days($tasks, $project);
			$tasks = $temp['tasks'];
			$count = ['pv' => 0, 'ev' => 0];
			foreach ($tasks as $task):
				$dates = (object)['start' => Carbon::parse($task->start), 'end' => Carbon::parse($task->end), 'start_new' => Carbon::parse($task->start_new), 'end_new' => Carbon::parse($task->end_new), 'now' => Carbon::now('America/Lima'),];
				if ($task->approved):
					$task->counter = true;
					$task->pv = $task->diff;
					$task->ev = $task->diff_new;
					$task->indice = ($task->ev === 0) ? 0 : ($task->ev / $task->pv);
				else:
					if ($task->father !== null):
						if ($task->father->counter):
							$task->counter = true;
							if ($dates->now < $dates->start_new):
								$task->pv = $task->diff_new * $temp['price_day'];
								$task->ve = null;
								$task->indice = null;
							else:
								if ($dates->now >= $dates->start_new && $dates->now <= $dates->end_new):
									$task->pv = $task->diff_new;
									$task->ev = $task->diff_new;
									$task->indice = ($task->ev / $task->pv);
								else:
									$task->diff_new = $dates->now->diffInWeekdays($dates->start_new);
									$task->pv = $task->diff;
									$task->ev = $task->diff_new;
									$task->indice = ($task->ev === 0) ? 0 : ($task->ev / $task->pv);
								endif;
							endif;
						else:
							$task->counter = false;
						endif;
					else:
						$task->counter = true;
						if ($dates->now < $dates->start_new):
							$task->pv = $task->diff_new * $temp['price_day'];
							$task->ve = null;
							$task->indice = null;
						else:
							if ($dates->now >= $dates->start_new && $dates->now <= $dates->end_new):
								$last = $dates->now->diffInWeekdays($dates->start_new);
								$task->pv = $last;
								$task->ev = $last;
								$task->indice = ($task->ev / $task->pv);
							else:
								$task->diff_new = $dates->now->diffInWeekdays($dates->start_new);
								$task->pv = $task->diff;
								$task->ev = $task->diff_new;
								$task->indice = ($task->ev === 0) ? 0 : ($task->ev / $task->pv);
							endif;
						endif;
					endif;
				endif;
				if ($task->counter):
					$count['pv'] += $task->pv;
					$count['ev'] += $task->ev;
				endif;
			endforeach;
			$mean = ($count['pv'] === 0)? 0:round($count['ev'] / $count['pv'], 2);
			return ['mean' => $mean, 'tasks' => $tasks];
		}

		public function schedule($code) {
			$auth = auth()->guard('manager')->user();
			$project = Project::where(['code' => $code, 'manager_id' => $auth->id])->first();
			if ($project):
				$tasks = Task::Join('activity', 'task.activity_id', '=', 'activity.id')->join('phase', 'activity.phase_id', '=', 'phase.id')->join('project', 'phase.project_id', '=', 'project.id')->where(['project.code' => $code])->select('task.*')->with('progress', 'father', 'containers', 'activity')->get();
				$temp = $this->get_days($tasks, $project);
				$tasks = $temp['tasks'];
				$count = ['pv' => 0, 'ev' => 0];
				foreach ($tasks as $task):
					$dates = (object)['start' => Carbon::parse($task->start), 'end' => Carbon::parse($task->end), 'start_new' => Carbon::parse($task->start_new), 'end_new' => Carbon::parse($task->end_new), 'now' => Carbon::now('America/Lima'),];
					if ($task->approved):
						$task->counter = true;
						$task->pv = $task->diff;
						$task->ev = $task->diff_new;
						$task->indice = ($task->ev === 0) ? 0 : ($task->ev / $task->pv);
					else:
						if ($task->father !== null):
							if ($task->father->counter):
								$task->counter = true;
								if ($dates->now < $dates->start_new):
									$task->pv = $task->diff_new * $temp['price_day'];
									$task->ve = null;
									$task->indice = null;
								else:
									if ($dates->now >= $dates->start_new && $dates->now <= $dates->end_new):
										$task->pv = $task->diff_new;
										$task->ev = $task->diff_new;
										$task->indice = ($task->ev / $task->pv);
									else:
										$task->diff_new = $dates->now->diffInWeekdays($dates->start_new);
										$task->pv = $task->diff;
										$task->ev = $task->diff_new;
										$task->indice = ($task->ev === 0) ? 0 : ($task->ev / $task->pv);
									endif;
								endif;
							else:
								$task->counter = false;
							endif;
						else:
							$task->counter = true;
							if ($dates->now < $dates->start_new):
								$task->pv = $task->diff_new * $temp['price_day'];
								$task->ve = null;
								$task->indice = null;
							else:
								if ($dates->now >= $dates->start_new && $dates->now <= $dates->end_new):
									$last = $dates->now->diffInWeekdays($dates->start_new);
									$task->pv = $last;
									$task->ev = $last;
									$task->indice = ($task->ev / $task->pv);
								else:
									$task->diff_new = $dates->now->diffInWeekdays($dates->start_new);
									$task->pv = $task->diff;
									$task->ev = $task->diff_new;
									$task->indice = ($task->ev === 0) ? 0 : ($task->ev / $task->pv);
								endif;
							endif;
						endif;
					endif;
					if ($task->counter):
						$count['pv'] += $task->pv;
						$count['ev'] += $task->ev;
					endif;
				endforeach;
				$mean = ($count['pv'] === 0)? 0:round($count['ev'] / $count['pv'], 2);
				return view('manager.project.schedule')->with(['title' => 'Indice del desempeño del cronograma', 'tasks' => $tasks, 'days' => $temp['days'], 'price_day' => $temp['price_day'], 'mean' => $mean]);
			else:
				return redirect()->route('manager.project.index');
			endif;
		}

		public function cost($code) {
			$auth = auth()->guard('manager')->user();
			$project = Project::where(['code' => $code, 'manager_id' => $auth->id])->first();
			if ($project):
				$tasks = Task::Join('activity', 'task.activity_id', '=', 'activity.id')->join('phase', 'activity.phase_id', '=', 'phase.id')->join('project', 'phase.project_id', '=', 'project.id')->where(['project.code' => $code])->select('task.*')->with('progress', 'father', 'containers', 'activity')->get();
				$temp = $this->get_days($tasks, $project);
				$tasks = $temp['tasks'];
				$count = ['ev' => 0, 'ac' => 0];
				foreach ($tasks as $task):
					$dates = (object)['start' => Carbon::parse($task->start), 'end' => Carbon::parse($task->end), 'start_new' => Carbon::parse($task->start_new), 'end_new' => Carbon::parse($task->end_new), 'now' => Carbon::now('America/Lima'),];
					if ($task->approved):
						$task->counter = true;
						$task->pv = $task->diff * $temp['price_day'];
						$task->ev = ($task->progressive * $task->pv) / 100;
						$task->ac = ($task->diff_new * $temp['price_day']);
						$task->indice = ($task->ev === 0) ? 0 : ($task->ev / $task->ac);
					else:
						if ($task->father !== null):
							if ($task->father->counter):
								if ($dates->now < $dates->start_new):
									$task->counter = false;
									$task->pv = $task->diff_new * $temp['price_day'];
									$task->ve = null;
									$task->ac = null;
									$task->indice = null;
								else:
									$task->counter = true;
									if ($dates->now >= $dates->start_new && $dates->now <= $dates->end_new):
										$last = $dates->now->diffInWeekdays($dates->start_new);
										$task->pv = $task->diff * $temp['price_day'];
										$task->ev = $task->progressive * (($task->pv) / 100);
										$task->ac = ($last * $temp['price_day']) + $task->resource;
										$task->indice = ($task->ev / $task->ac);
									else:
										$task->diff_new = $dates->now->diffInWeekdays($dates->start_new);
										$task->pv = $task->diff * $temp['price_day'];
										$task->ev = $task->progressive * (($task->pv) / 100);
										$task->ac = ($task->diff_new * $temp['price_day']);
										$task->indice = ($task->ev === 0) ? 0 : ($task->ev / $task->pv);
									endif;
								endif;
							else:
								$task->counter = false;
							endif;
						else:
							if ($dates->now < $dates->start_new):
								$task->counter = false;
								$task->pv = $task->diff * $temp['price_day'];
								$task->ve = null;
								$task->ac = null;
								$task->indice = null;
							else:
								$task->counter = true;
								if ($dates->now >= $dates->start_new && $dates->now <= $dates->end_new):
									$last = $dates->now->diffInWeekdays($dates->start_new) ;
									$task->pv = $task->diff * $temp['price_day'];
									$task->ev = $task->progressive * (($task->pv) / 100);
									$task->ac = ($last * $temp['price_day']) + $task->resource;
									$task->indice = ($task->ev / $task->ac);
								else:
									$task->diff_new = $dates->now->diffInWeekdays($dates->start_new);
									$task->pv = $task->diff * $temp['price_day'];
									$task->ev = $task->progressive * (($task->pv) / 100);
									$task->ac = ($task->diff_new * $temp['price_day']);
									$task->indice = ($task->ev === 0) ? 0 : ($task->ev / $task->pv);
								endif;
							endif;
						endif;
					endif;
					if ($task->counter):
						$count['ev'] += $task->ev;
						$count['ac'] += $task->ac;
					endif;
				endforeach;
				$mean = ($count['ac'] === 0)? 0:round($count['ev'] / $count['ac'], 2);
				return view('manager.project.cost')->with(['title' => 'Indice del desempeño del costo', 'tasks' => $tasks, 'days' => $temp['days'], 'price_day' => $temp['price_day'], 'mean' => $mean]);
			else:
				return redirect()->route('manager.project.index');
			endif;
		}

		public function resources($code) {
			$auth = auth()->guard('manager')->user();
			$project = Project::where(['code' => $code, 'manager_id' => $auth->id])->with('phases.activities.tasks.containers')->first();
			if ($project):
				$colors = ['color_1' => $this->rand_color(), 'color_2' => $this->rand_color()];
				foreach ($project->phases as $phase):
					foreach ($phase->activities as $activity):
						foreach ($activity->tasks as $task):
							$task->money = 0;
							$task->count = 0;
							$task->color = $colors;
							foreach ($task->containers as $container):
								$task->money += (double)$container->total;
								$task->count += (double)$container->number;
							endforeach;
						endforeach;
					endforeach;
				endforeach;
				return view('manager.project._Resources')->with(['project' => $project, 'title' => 'Graficas de recursos']);
			else:
				return redirect()->route('manager.project.index');
			endif;
		}

		private function rand_color() {
			return '#' . str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);
		}

		public function table(Request $request) {
			if ($request->ajax()):
				try {
					$config = ['end_date_first' => Config::where(['slug' => 'end_date_first'])->first(), 'end_date_last' => Config::where(['slug' => 'end_date_last'])->first()];
					return DataTables::of(Project::withTrashed()->signaling($request, $config)->range_start($request)->range_end($request)->orderBy('name'))->addColumn('link', function ($row) {
						$color = ($row->deleted_at) ? 'red' : 'blue';
						$buttons = "<div class='btn-group'><button class='btn $color btn-xs btn-outline dropdown-toggle' data-toggle='dropdown'>Acciones<i class='fa fa-angle-down'></i></button><ul class='dropdown-menu'>";
						if (!$row->deleted_at):
							$buttons .= "<li><a href='" . route('manager.phase.index', $row->code) . "'><i class='fa fa-list-ol'></i> Fases</a></li>";
							$buttons .= "<li><a data-url='" . route('manager.project.update', $row->code) . "' class='btn-ajax'><i class='fa fa-pencil'></i> Mostrar</a></li>";
							if(!$row->finalized):
								$buttons .= "<li><a data-url='" . route('manager.project.finish', $row->code) . "' data-message='Desea finalizar el proyecto: " . $row->name . "' class='btn-destroy'><i class='fa fa-flag'></i> Finalizar</a></li>";
							endif;
							$buttons .= "<li><a data-url='" . route('manager.project.delete', $row->code) . "' data-message='Desea deshabilitar el proyecto: " . $row->name . "' class='btn-destroy'><i class='fa fa-low-vision'></i> Deshabilitar</a></li>";
							$buttons .= "<li><a data-url='" . route('manager.project.destroy', $row->code) . "' data-message='Desea eliminar el proyecto: " . $row->name . "' class='btn-destroy'><i class='fa fa-trash'></i> Eliminar</a></li>";
						else:
							$buttons .= "<li><a data-url='" . route('manager.project.delete', $row->code) . "' data-message='Desea restaurar el proyectoe: " . $row->name . "' class='btn-destroy'><i class='fa fa-recycle'></i> Restaurar</a></li>";
						endif;
						$buttons .= "</ul></div>";
						$buttons .= "<div class='btn-group'><button class='btn $color btn-xs btn-outline dropdown-toggle' data-toggle='dropdown' style='margin-left: 2em;'>Principales<i class='fa fa-angle-down'></i></button><ul class='dropdown-menu'>";
						if (!$row->deleted_at):
							$buttons .= "<li><a href='" . route('manager.project.schedule.index', $row->code) . "'><i class='fa fa-bar-chart-o'></i> Indice de desempeño del Cronograma</a></li>";
							$buttons .= "<li><a href='" . route('manager.project.cost.index', $row->code) . "'><i class='fa fa-bar-chart-o'></i> Indice de desempeño del Costo</a></li>";
							$buttons .= "<li><a href='" . route('manager.project.resource.index', $row->code) . "'><i class='fa fa-bar-chart-o'></i> Grafica de Recursos</a></li>";
							$buttons .= "<li><a href='" . route('manager.project.gant.index', $row->code) . "'><i class='fa fa-bar-chart-o'></i> Gantt</a></li>";
						endif;
						$buttons .= "</ul></div>";
						return $buttons;
					})->addColumn('cpi', function ($row) use ($config) {
						if($row->finalized):
							return $this->get_schedule($row)['mean'];
						else:
							return "";
						endif;
					})
					->addColumn('spi', function ($row) use ($config) {
						if($row->finalized):
							return $this->get_cost($row)['mean'];
						else:
							return "";
						endif;
					})->addColumn('end', function ($row) use ($config) {
							if(!$row->finalized):
								$dates = ['now' => Carbon::now('America/Lima'), 'end' => new Carbon($row->end_new, 'America/Lima')];
								$dates['now'] = new Carbon($dates['now']->toDateString(), 'America/Lima');
								return $this->signaling($config, $dates) . $row->end;
							else:
								return "<i class='fa fa-circle' style='color: #0070fe'></i> $row->finalized";
							endif;
					})
					->addIndexColumn()->rawColumns(['link', 'end'])->make(true);
				} catch (Exception $e) {
					return response()->json(["sEcho" => 1, "iTotalRecords" => 0, "iTotalDisplayRecords" => 0, "aaData" => []]);
				}
			else:
				return redirect()->route('manager.login');
			endif;
		}

		public function create(Request $request) {
			$role = Role::where(['slug' => 'client'])->first();
			$clients = User::where(['role_id' => $role->id])->pluck('name', 'id');
			$types = Type::pluck('name', 'id');
			if ($request->ajax()):
				$data = view('manager.project._Create');
			else:
				$data = view('manager.project.Create')->with(['title' => 'Registro de empresa']);
			endif;
			return $data->with(['clients' => $clients, 'types' => $types]);
		}

		public function store(Request $request) {
			try {
				DB::beginTransaction();
				$project = new Project($request->all());
				$project->fill(['manager_id' => auth()->guard('manager')->user()->id, 'price_real' => $request->price_base, 'start_new' => $request->start, 'end_new' => $request->end])->save();
				$message = "Registro exitoso";
				DB::commit();
				$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('manager.project.index')], 'status' => 200, 'route' => route('manager.project.index'), 'message' => $message, 'type' => 'success'];
				$data = $this->optimize($array);
			} catch (Exception $e) {
				DB::rollBack();
				$message = "Ocurrio un error en el proceso";
				$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('manager.project.create'), 'message' => $message, 'type' => 'error'];
				$data = $this->optimize($array);
			}
			return $data;
		}

		public function show(Request $request, $code) {
			$project = Project::withTrashed()->where(['code' => $code])->first();
			if ($project):
				$role = Role::where(['slug' => 'client'])->first();
				$clients = User::where(['role_id' => $role->id])->pluck('name', 'id');
				$types = Type::pluck('name', 'id');
				if ($request->ajax()):
					$data = view('manager.project._Update');
				else:
					$data = view('manager.project.Update')->with(['title' => "Actualización del project: $project->name"]);
				endif;
				$data->with(['project' => $project, 'clients' => $clients, 'types' => $types]);
			else:
				if ($request->ajax()):
					$data = "Proyecto no existente";
				else:
					session()->flash('improper', "No intentes algo indebido, el projecto no existe");
					$data = redirect(route('manager.project.index'));
				endif;
			endif;
			return $data;
		}

		public function update(Request $request, $code) {
			$project = Project::withTrashed()->where(['code' => $code])->first();
			if ($project):
				try {
					DB::beginTransaction();
					$project->fill($request->all())->save();
					$message = "Actualizacion exitosa";
					DB::commit();
					$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('manager.project.index')], 'status' => 200, 'route' => route('manager.project.index'), 'message' => $message, 'type' => 'success'];
					$data = $this->optimize($array);
				} catch (Exception $e) {
					DB::rollBack();
					$message = "Ocurrio un error en el proceso";
					$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('manager.project.update', $project->id), 'message' => $message, 'type' => 'error'];
					$data = $this->optimize($array);
				}
			else:
				$message = "No intentes algo indebido, la compañia no existe";
				$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('manager.project.index'), 'message' => $message, 'type' => 'improper'];
				$data = $this->optimize($array);
			endif;
			return $data;
		}

		public function delete(Request $request, $code) {
			if ($request->ajax()):
				$project = Project::withTrashed()->where(['code' => $code])->first();
				if ($project):
					try {
						DB::beginTransaction();
						if ($project->deleted_at):
							$project->restore();
							$message = "Restauracion exitosa";
						else:
							$project->delete();
							$message = "Deshabilitación exitosa";
						endif;
						DB::commit();
						session()->flash('success', $message);
						$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('manager.project.index')], 200);
					} catch (Exception $e) {
						DB::rollBack();
						$data = response()->json(['resp' => false, 'message' => "Ocurrio un error en el proceso"], 422);
					}
				else:
					$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, el proyecto no existe"], 422);
				endif;
			else:
				session()->flash('improper', "No intentes algo indebido");
				$data = redirect(route('manager.project.index'));
			endif;
			return $data;
		}

		public function destroy(Request $request, $code) {
			if ($request->ajax()):
				$project = Project::withTrashed()->where(['code' => $code])->first();
				if ($project):
					try {
						DB::beginTransaction();
						$project->forceDelete();
						DB::commit();
						$message = "Eliminacion exitosa";
						session()->flash('success', $message);
						$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('manager.project.index')], 200);
					} catch (Exception $e) {
						DB::rollBack();
						$data = response()->json(['resp' => false, 'message' => "Ocurrio un error en el proceso"], 422);
					}
				else:
					$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, el proyecto no existe"], 422);
				endif;
			else:
				session()->flash('improper', "No intentes algo indebido");
				$data = redirect(route('manager.project.index'));
			endif;
			return $data;
		}

		public function finish($code) {
			$project = Project::withTrashed()->where(['code' => $code])->first();
			if ($project):
				try {
					DB::beginTransaction();
					$dates = Task::Join('activity', 'task.activity_id', '=', 'activity.id')->join('phase', 'activity.phase_id', '=', 'phase.id')->join('project', 'phase.project_id', '=', 'project.id')->where(['project.code' => $code])->select(DB::raw('max(task.end_new) as end, min(task.start_new) as start'))->first();
					$tasks = Task::Join('activity', 'task.activity_id', '=', 'activity.id')->Join('phase', 'activity.phase_id', '=', 'phase.id')->Join('project', 'phase.project_id', '=', 'project.id')->where(['project.code' => $code])->select('task.*')->with('progress', 'containers')->get();
					$temp = $this->get_days($tasks, $project);
					$count = ['ac' => 0];
					foreach ($tasks as $task):
						$dates = (object)['start' => Carbon::parse($task->start), 'end' => Carbon::parse($task->end), 'start_new' => Carbon::parse($task->start_new), 'end_new' => Carbon::parse($task->end_new), 'now' => Carbon::now('America/Lima'),];
						if ($task->approved):
							$task->counter = true;
							$task->ac = ($task->diff_new * $temp['price_day']);
						else:
							if ($task->father !== null):
								if ($task->father->counter):
									if ($dates->now < $dates->start_new):
										$task->counter = false;
										$task->ac = 0;
									else:
										$task->counter = true;
										if ($dates->now >= $dates->start_new && $dates->now <= $dates->end_new):
											$last = $dates->now->diffInWeekdays($dates->start_new);
											$task->ac = ($last * $temp['price_day']) + $task->resource;
										else:
											$task->diff_new = $dates->now->diffInWeekdays($dates->start_new);
											$task->ac = ($task->diff_new * $temp['price_day']);
										endif;
									endif;
								else:
									$task->counter = false;
								endif;
							else:
								if ($dates->now < $dates->start_new):
									$task->counter = false;
									$task->ac = 0;
								else:
									$task->counter = true;
									if ($dates->now >= $dates->start_new && $dates->now <= $dates->end_new):
										$last = $dates->now->diffInWeekdays($dates->start_new) ;
										$task->ac = ($last * $temp['price_day']) + $task->resource;
									else:
										$task->diff_new = $dates->now->diffInWeekdays($dates->start_new);
										$task->ac = ($task->diff_new * $temp['price_day']);
									endif;
								endif;
							endif;
						endif;
						if ($task->counter):
							$count['ac'] += $task->ac;
						endif;
					endforeach;
					$project->fill(['start_new' => $dates->start, 'end_new' => $dates->end, 'price_real' => $count['ac'], 'finalized' => date('Y-m-d')])->save();
					DB::commit();
					$message = "Actualizacion exitosa";
					session()->flash('success', $message);
					$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('manager.project.index')], 200);
				} catch (Exception $e) {
					DB::rollBack();
					$data = response()->json(['resp' => false, 'message' => "Ocurrio un error en el proceso"], 422);
				}
			else:
				$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, el proyecto no existe"], 422);
			endif;
			return $data;
		}

	}
