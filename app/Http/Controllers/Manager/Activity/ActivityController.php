<?php

	namespace App\Http\Controllers\Manager\Activity;

	use App\Http\Controllers\Controller;
	use App\Model\Activity;
	use App\Model\Config;
	use App\Model\Phase;
	use App\Model\Project;
	use App\Model\Role;
	use App\Model\Task;
	use App\Model\User;
	use Carbon\Carbon;
	use Exception;
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\DB;
	use Yajra\DataTables\DataTables;

	class ActivityController extends Controller {

		public function index($code) {
			$phase = Phase::whereCode($code)->with('project')->first();
			if ($phase):
				$create = $this->authorization($phase->project->start);
				return view('manager.activity._index')->with(['title' => "Panel de Actividades de la fase N° $phase->code", 'class_header' => 'page-container-bg-solid', 'phase' => $phase, 'created' => $create]);
			endif;
			return redirect()->route('manager.activity.index');
		}

		public function table(Request $request, $code) {
			$phase = Phase::whereCode($code)->first();
			if ($request->ajax()):
				if ($phase):
					try {
						$config = ['end_date_first' => Config::where(['slug' => 'end_date_first'])->first(), 'end_date_last' => Config::where(['slug' => 'end_date_last'])->first()];
						return DataTables::of(Activity::withTrashed()->signaling($request, $config)->range_start($request)->range_end($request)->where(['phase_id' => $phase->id])->orderByRaw("1*SUBSTRING_INDEX(activity.structure, '.', 1) desc, activity.id desc")->with('phase', 'activity', 'activities'))->addColumn('link', function ($row) use ($phase) {
							$color = ($row->deleted_at) ? 'red' : 'blue';
							$buttons = "<div class='btn-group'><button class='btn $color btn-xs btn-outline dropdown-toggle' data-toggle='dropdown'>Acciones<i class='fa fa-angle-down'></i></button><ul class='dropdown-menu'>";
							if (!$row->deleted_at):
								if ($row->activity_id):
									if ($row->activity->approved):
										if (count($row->activities) > 0 && empty($row->approved)):
											$buttons .= "<li><a data-url='" . route('manager.activity.activate', [$phase->code, $row->code]) . "' data-message='Desea aprobar la actividad: " . $row->name . "' class='btn-destroy'><i class='fa fa-hand-o-up'></i> Aprobar</a></li>";
										endif;
									endif;
								else:
									if (count($row->activities) > 0 && empty($row->approved)):
										$buttons .= "<li><a data-url='" . route('manager.activity.activate', [$phase->code, $row->code]) . "' data-message='Desea aprobar la actividad: " . $row->name . "' class='btn-destroy'><i class='fa fa-hand-o-up'></i> Aprobar</a></li>";
									endif;
								endif;
								$buttons .= "<li><a href='" . route('manager.activity.message.index', [$row->code]) . "'><i class='fa fa-comments-o'></i> Mensajes Cliente</a></li>";
								$buttons .= "<li><a href='" . route('manager.task.index', [$row->code]) . "'><i class='fa fa-list-ol'></i> Tareas</a></li>";
								$buttons .= "<li><a data-url='" . route('manager.activity.update', [$phase->code, $row->code]) . "' class='btn-ajax'><i class='fa fa-pencil'></i> Mostrar</a></li>";
								$buttons .= "<li><a data-url='" . route('manager.activity.delete', [$phase->code, $row->code]) . "' data-message='Desea deshabilitar el proyecto: " . $row->name . "' class='btn-destroy'><i class='fa fa-low-vision'></i> Deshabilitar</a></li>";
								$buttons .= "<li><a data-url='" . route('manager.activity.destroy', [$phase->code, $row->code]) . "' data-message='Desea eliminar el proyecto: " . $row->name . "' class='btn-destroy'><i class='fa fa-trash'></i> Eliminar</a></li>";
							else:
								$buttons .= "<li><a data-url='" . route('manager.activity.delete', [$phase->code, $row->code]) . "' data-message='Desea restaurar el proyecto: " . $row->name . "' class='btn-destroy'><i class='fa fa-recycle'></i> Restaurar</a></li>";
							endif;
							$buttons .= "</ul></div>";
							return $buttons;
						})->addColumn('end_new', function ($row) use ($config) {
							if(!$row->approved):
								$dates = ['now' => Carbon::now('America/Lima'), 'end' => new Carbon($row->end_new, 'America/Lima')];
								$dates['now'] = new Carbon($dates['now']->toDateString(), 'America/Lima');
								return $this->signaling($config, $dates) . $row->end_new;
							else:
								return "<i class='fa fa-circle' style='color: #0070fe'></i> $row->approved";
							endif;
						})->addIndexColumn()->rawColumns(['link', 'end_new'])->make(true);
					} catch (Exception $e) {
						return response()->json(["sEcho" => 1, "iTotalRecords" => 0, "iTotalDisplayRecords" => 0, "aaData" => []]);
					}
				else:
					return response()->json(["sEcho" => 1, "iTotalRecords" => 0, "iTotalDisplayRecords" => 0, "aaData" => []]);
				endif;
			else:
				if ($phase):
					return redirect()->route('manager.activity.index');
				endif;
				return redirect()->route('manager.login');
			endif;
		}

		public function create(Request $request, $code) {
			$phase = Phase::whereCode($code)->first();
			if ($phase):
				$role = Role::where(['slug' => 'developer'])->first();
				$developers = User::where(['role_id' => $role->id])->pluck('name', 'id');
				$activities = Activity::where(['phase_id' => $phase->id])->orderByRaw("1*SUBSTRING_INDEX(structure, '.', 1) desc, id desc")->get()->pluck('full_name', 'id');
				if ($request->ajax()):
					$data = view('manager.activity._Create');
				else:
					$data = view('manager.activity.Create')->with(['title' => 'Registro de actividad']);
				endif;
				$data->with(['phase' => $phase, 'developers' => $developers, 'activities' => $activities]);
			else:
				$data = redirect()->route('manager.activity.index');
			endif;
			return $data;
		}

		public function store(Request $request, $code) {
			$phase = Phase::whereCode($code)->first();
			if ($phase):
				try {
					DB::beginTransaction();
					$activity = new Activity($request->all());
					$activity->fill(['phase_id' => $phase->id, 'start_new' => $request->start, 'end_new' => $request->end])->save();
					DB::commit();
					$message = "Registro exitoso del la fase N° $activity->code";
					$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('manager.activity.index', $phase->code)], 'status' => 200, 'route' => route('manager.activity.index', $phase->code), 'message' => $message, 'type' => 'success'];
					$data = $this->optimize($array);
				} catch (Exception $e) {
					DB::rollBack();
					$message = "Ocurrio un error en el proceso";
					$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $e], 'status' => 422, 'route' => route('manager.activity.create', $phase->code), 'message' => $message, 'type' => 'error'];
					$data = $this->optimize($array);
				}
			else:
				$data = redirect()->route('manager.project.index');
			endif;
			return $data;
		}

		public function show(Request $request, $phase_code, $code) {
			$phase = Phase::withTrashed()->whereCode($phase_code)->first();
			if ($phase):
				$activity = Activity::where(['code' => $code, 'phase_id' => $phase->id])->first();
				if ($activity):
					$role = Role::where(['slug' => 'developer'])->first();
					$developers = User::where(['role_id' => $role->id])->pluck('name', 'id');
					$activities = Activity::where(['phase_id' => $phase->id])->orderByRaw("1*SUBSTRING_INDEX(structure, '.', 1) desc, id desc")->get()->pluck('full_name', 'id');
					if ($request->ajax()):
						$data = view('manager.activity._Update');
					else:
						$data = view('manager.activity.Update')->with(['title' => "Actualización de la actividad : $activity->name"]);
					endif;
					$data->with(['activity' => $activity, 'phase' => $phase, 'developers' => $developers, 'activities' => $activities]);
				else:
					if ($request->ajax()):
						$data = "Actividad no existente";
					else:
						session()->flash('improper', "No intentes algo indebido, la actividad no existe");
						$data = redirect(route('manager.activity.index', $phase->code));
					endif;
				endif;
			else:
				if ($request->ajax()):
					$data = "Area no existente";
				else:
					session()->flash('improper', "No intentes algo indebido, el proyecto no existe");
					$data = redirect(route('manager.activity.index'));
				endif;
			endif;
			return $data;
		}

		public function update(Request $request, $phase_code, $code) {
			$phase = Phase::withTrashed()->whereCode($phase_code)->first();
			if ($phase):
				$activity = Activity::withTrashed()->where(['code' => $code, 'phase_id' => $phase->id])->first();
				if ($activity):
					try {
						DB::beginTransaction();
						$activity->fill($request->all());
						$activity->fill(['start_new' => $activity->start, 'end_new' => $activity->end])->save();
						$message = "Actualizacion exitosa";
						DB::commit();
						$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('manager.activity.index', $phase->code)], 'status' => 200, 'route' => route('manager.activity.index', $phase->code), 'message' => $message, 'type' => 'success'];
						$data = $this->optimize($array);
					} catch (Exception $e) {
						DB::rollBack();
						$message = "Ocurrio un error en el proceso";
						$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('manager.activity.update', $phase->code, $activity->code), 'message' => $message, 'type' => 'error'];
						$data = $this->optimize($array);
					}
				else:
					$message = "No intentes algo indebido, la actividad no existe";
					$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('manager.activity.index', $phase->code), 'message' => $message, 'type' => 'improper'];
					$data = $this->optimize($array);
				endif;
			else:
				$message = "No intentes algo indebido, el proyecto no existe";
				$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('manager.project.index'), 'message' => $message, 'type' => 'improper'];
				$data = $this->optimize($array);
			endif;
			return $data;
		}

		public function delete(Request $request, $phase_code, $code) {
			if ($request->ajax()):
				$phase = Phase::withTrashed()->whereCode($phase_code)->first();
				if ($phase):
					$activity = Activity::withTrashed()->where(['code' => $code, 'phase_id' => $phase->id])->first();
					if ($activity):
						try {
							DB::beginTransaction();
							if ($activity->deleted_at):
								$activity->restore();
								$message = "Restauracion exitosa";
							else:
								$activity->delete();
								$message = "Deshabilitación exitosa";
							endif;
							DB::commit();
							session()->flash('success', $message);
							$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('manager.activity.index', $phase->code)], 200);
						} catch (Exception $e) {
							DB::rollBack();
							$data = response()->json(['resp' => false, 'message' => "Ocurrio un error en el proceso"], 422);
						}
					else:
						$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, la actividad no existe"], 422);
					endif;
				else:
					$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, la etapa no existe"], 422);
				endif;
			else:
				session()->flash('improper', "No intentes algo indebido");
				$data = redirect(route('manager.project.index'));
			endif;
			return $data;
		}

		public function destroy(Request $request, $phase_code, $code) {
			if ($request->ajax()):
				$phase = Phase::withTrashed()->whereCode($phase_code)->first();
				if ($phase):
					$activity = Activity::withTrashed()->where(['code' => $code, 'phase_id' => $phase->id])->first();
					if ($activity):
						try {
							DB::beginTransaction();
							$activity->forceDelete();
							DB::commit();
							$message = "Eliminacion exitosa";
							session()->flash('success', $message);
							$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('manager.activity.index', $phase->code)], 200);
						} catch (Exception $e) {
							DB::rollBack();
							$data = response()->json(['resp' => false, 'message' => "Ocurrio un error en el proceso"], 422);
						}
					else:
						$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, la actividad no existe"], 422);
					endif;
				else:
					$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, la fase no existe"], 422);
				endif;
			else:
				session()->flash('improper', "No intentes algo indebido");
				$data = redirect(route('manager.project.index'));
			endif;
			return $data;
		}

		public function activate(Request $request, $phase_code, $code) {
			if ($request->ajax()):
				$phase = Phase::withTrashed()->whereCode($phase_code)->first();
				if ($phase):
					$activity = Activity::withTrashed()->where(['code' => $code, 'phase_id' => $phase->id])->with('activities')->first();
					if ($activity):
						try {
							DB::beginTransaction();
							$date = date('Y-m-d');
							$dates = (object)['end' => Carbon::parse($activity->end_new), 'approved' => Carbon::parse($activity->approved)];
							if ($dates->approved > $dates->end):
								$activity->fill(['approved' => $date])->save();
								$days = $dates->approved->diffInWeekdays($dates->end);
								if (count($activity->activities) > 0):
									$this->updater_activity($activity->activities, $days);
								endif;
							elseif($dates->approved >= $dates->start && $dates->approved <= $dates->end):
								if($dates->approved < $dates->end):
									$days = $dates->approved->diffInWeekdays($dates->end, false);
									$activity->fill(['approved' => $dates->approved->toDateString(), 'end_new' => $dates->end->addWeekdays($days)->toDateString()])->save();
									if (count($activity->activities) > 0):
										$this->updater_activity($activity->activities, $days);
									endif;
								endif;
							endif;
							$temp_task = Task::where(['activity_id' => $activity->id])->select(DB::raw('max(end_new) as end_new, min(start_new) as start_new'))->first();
							if($temp_task):
								$activity->fill(['start_new' => $temp_task->start_new, 'end_new' => $temp_task->end_new])->save();
							endif;
							DB::commit();
							$message = "Aprobacion exitosa";
							session()->flash('success', $message);
							$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('manager.activity.index', $phase->code)], 200);
						} catch (Exception $e) {
							DB::rollBack();
							$data = response()->json(['resp' => false, 'message' => $e], 422);
						}
					else:
						$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, la actividad no existe"], 422);
					endif;
				else:
					$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, la fase no existe"], 422);
				endif;
			else:
				session()->flash('improper', "No intentes algo indebido");
				$data = redirect(route('manager.project.index'));
			endif;
			return $data;
		}

	}
