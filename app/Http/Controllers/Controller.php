<?php

	namespace App\Http\Controllers;

	use App\Model\Task;
	use Carbon\Carbon;
	use Illuminate\Foundation\Bus\DispatchesJobs;
	use Illuminate\Routing\Controller as BaseController;
	use Illuminate\Foundation\Validation\ValidatesRequests;
	use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
	use Illuminate\Support\Facades\DB;

	class Controller extends BaseController {

		use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

		protected function optimize($array) {
			session()->flash($array->type, $array->message);
			if ($array->request->ajax()):
				$data = response()->json($array->array, $array->status);
			else:
				$data = redirect($array->route);
			endif;
			return $data;
		}

		protected function get_days($tasks, $project){
			$days = ['days' => 0, 'days_new' => 0];
			foreach ($tasks as $task):
				if(count($task->containers) > 0):
					$task->resource = 0;
					foreach ($task->containers as $container):
						$task->resource+=(float)$container->total;
					endforeach;
				else:
					$task->resource = 0;
				endif;
				$dates = (object)[
					'start' => Carbon::parse($task->start),
					'end' => Carbon::parse($task->end),
					'start_new' => Carbon::parse($task->start_new),
					];
				$dates->end_new = ($task->approved)? Carbon::parse($task->approved):Carbon::parse($task->end_new);
				$task->progressive = ($task->progress)?(float)$task->progress->progress: 0;
				$task->diff = $dates->end->diffInWeekdays($dates->start) + 1;
				$task->diff_new = $dates->end_new->diffInWeekdays($dates->start_new) + 1;
				$days['days']+=$task->diff;
				$days['days_new'] += $task->diff_new;
				$task->days_percentage = $task->diff_new;
			endforeach;
			return ['days' => $days['days'], 'days_new' => $days['days_new'], 'tasks' => $tasks, 'price_day' => ($days['days'] === 0)? 0:round((float)$project->price_base/ $days['days'],2)];
		}

		protected function days($project) {
			foreach ($project->phases as $phase):
				foreach ($phase->activities as $activity):
					foreach ($activity->tasks as $task):
						$start = Carbon::parse($task->start_new);
						if($task->approved):
							$approved = Carbon::parse($task->approved);
							$end_new = Carbon::parse($task->end_new);
							$end = ($approved > $end_new)? $approved: $end_new;
						else:
							$end = Carbon::parse($task->end_new);
						endif;
						$task->days = $start->diffInWeekdays($end) + 1;
					endforeach;
				endforeach;
			endforeach;
			return $project;
		}

		public function authorization($start, $timezone = 'America/Lima'){
			$start = new Carbon($start, $timezone);
			$now = Carbon::now($timezone);
			return $now >= $start;
		}

		public function difference($start, $timezone = 'America/Lima'){
			$start = new Carbon($start, $timezone);
			$now = Carbon::now($timezone);
			return $now->diffInWeekdays($start);
		}

		public function signaling($config, $dates){
			$diff = $dates['now']->DiffInDays($dates['end'], false);
			if($diff >= (int)$config['end_date_first']->value):
				$color = 'green';
			elseif($diff < (int)$config['end_date_first']->value && $diff > (int)$config['end_date_last']->value):
				$color = 'yellow';
			else:
				$color = 'red';
			endif;
			return "<i class='fa fa-circle' style='color: $color;'></i> ";
		}

		public function updater($tasks, $number = 0) {
			foreach ($tasks as $task):
				$dates = (object)['start' => Carbon::parse($task->start_new)->addWeekdays($number), 'end' => Carbon::parse($task->end_new)->addWeekdays($number)];
				$task->fill(['start_new' => $dates->start->toDateString(), 'end_new' => $dates->end->toDateString()])->save();
				if (count($task->tasks) > 0):
					$this->updater($task->tasks, $number);
				endif;
			endforeach;
			return $tasks;
		}

		public function updater_activity($activities, $number = 0) {
			foreach ($activities as $activity):
				$dates = (object)['start' => Carbon::parse($activity->start_new)->addWeekdays($number), 'end' => Carbon::parse($activity->end_new)->addWeekdays($number)];
				$activity->fill(['start_new' => $dates->start->toDateString(), 'end_new' => $dates->end->toDateString()])->save();
				if (count($activity->activities) > 0):
					$this->updater_activity($activity->activities, $number);
				endif;
				$tasks = Task::where(['activity_id' => $activity->id])->whereNull('task_id')->with('tasks')->get();
				foreach ($tasks as $task):
					$dates_task = (object)['start' => Carbon::parse($task->start_new)->addWeekdays($number), 'end' => Carbon::parse($task->end_new)->addWeekdays($number)];
					$task->fill(['start_new' => $dates_task->start->toDateString(), 'end_new' => $dates_task->end->toDateString()])->save();
					if (count($task->tasks) > 0):
						$task->tasks = $this->updater($task->tasks, $number);
					endif;
				endforeach;
				$temp_task = Task::where(['activity_id' => $activity->id])->select(DB::raw('max(end_new) as end_new, min(start_new) as start_new'))->first();
				if($temp_task):
					$activity->fill(['start_new' => $temp_task->start_new, 'end_new' => $temp_task->end_new])->save();
				endif;
			endforeach;
			return $activities;
		}

	}
