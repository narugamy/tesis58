<?php

	namespace App\Http\Controllers\Client\Home;

	use App\Model\Notification;
	use App\Model\Project;
	use App\Model\Type;
	use App\Http\Controllers\Controller;
	use Illuminate\Http\Request;

	class HomeController extends Controller {

		public function index() {
			$type = Type::where(['slug' => 'project'])->first();
			$projects = Project::where(['type_id' => $type->id])->with('phases.activities')->get();
			return view('client.home._index')->with(['title' => 'Panel de cliente', 'class_header' => 'page-container-bg-solid', 'projects' => $projects]);
		}

		public function notification(Request $request){
			if($request->ajax()):
				$user = auth()->guard('client')->user();
				$notifications = Notification::where(['user_id' => $user->id])->with('message')->get();
				$view = view('client.layout.notification')->with(['notifications' => $notifications])->render();
				return response()->json(['html' => $view, 'notification' => count($notifications)]);
			else:
				return redirect()->route('client.login');
			endif;
		}

	}
