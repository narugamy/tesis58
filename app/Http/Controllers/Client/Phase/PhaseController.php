<?php

	namespace App\Http\Controllers\Client\Phase;

	use App\Http\Controllers\Controller;
	use App\Model\Config;
	use App\Model\Phase;
	use App\Model\Project;
	use Carbon\Carbon;
	use Exception;
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\DB;
	use Yajra\DataTables\DataTables;

	class PhaseController extends Controller {

		public function index($code) {
			$auth = auth()->guard('client')->user();
			$project = Project::where(['code' => $code, 'client_id' => $auth->id])->first();
			if ($project):
				$create = $this->authorization($project->start);
				return view('client.phase._index')->with(['title' => "Panel de Fases del proyecto N° $project->code", 'class_header' => 'page-container-bg-solid', 'project' => $project, 'created' => $create]);
			endif;
			return redirect()->route('client.project.index');
		}

		public function table(Request $request, $code) {
			$auth = auth()->guard('client')->user();
			$project = Project::where(['code' => $code, 'client_id' => $auth->id])->first();
			if ($request->ajax()):
				if ($project):
					try {
						$config = ['end_date_first' => Config::where(['slug' => 'end_date_first'])->first(), 'end_date_last' => Config::where(['slug' => 'end_date_last'])->first()];
						return DataTables::of(Phase::withTrashed()->signaling($request, $config)->range_start($request)->range_end($request)->where(['project_id' => $project->id])->orderBy('name')->with('project'))->addColumn('link', function ($row) use ($project) {
							$color = ($row->deleted_at) ? 'red' : 'blue';
							$buttons = "<div class='btn-group'><button class='btn $color btn-xs btn-outline dropdown-toggle' data-toggle='dropdown'>Acciones<i class='fa fa-angle-down'></i></button><ul class='dropdown-menu'>";
							if (!$row->deleted_at):
								$buttons .= "<li><a href='" . route('client.activity.index', [$row->code]) . "'><i class='fa fa-list'></i> Actividades</a></li>";
								$buttons .= "<li><a data-url='" . route('client.phase.update', [$project->code, $row->code]) . "' class='btn-ajax'><i class='fa fa-pencil'></i> Mostrar</a></li>";
							endif;
							$buttons .= "</ul></div>";
							return $buttons;
						})->addColumn('end', function ($row) use ($config){
							if(!$row->project->finalized):
								$dates = ['now' => Carbon::now('America/Lima'), 'end' => new Carbon($row->end, 'America/Lima')];
								$dates['now'] = new Carbon($dates['now']->toDateString(), 'America/Lima');
								return $this->signaling($config, $dates).$row->end;
							else:
								return "<i class='fa fa-circle' style='color: #0070fe'></i> ".$row->project->finalized;
							endif;
						})->addIndexColumn()->rawColumns(['link', 'end'])->make(true);
					} catch (Exception $e) {
						return response()->json(["sEcho" => 1, "iTotalRecords" => 0, "iTotalDisplayRecords" => 0, "aaData" => []]);
					}
				else:
					return response()->json(["sEcho" => 1, "iTotalRecords" => 0, "iTotalDisplayRecords" => 0, "aaData" => []]);
				endif;
			else:
				if ($project):
					return redirect()->route('client.project.index');
				endif;
				return redirect()->route('client.login');
			endif;
		}

		public function show(Request $request, $project_code, $code) {
			$project = Project::withTrashed()->whereCode($project_code)->first();
			if ($project):
				$phase = Phase::withTrashed()->where(['project_id' => $project->id, 'code' => $code])->first();
				if ($phase):
					if ($request->ajax()):
						$data = view('client.phase._Update');
					else:
						$data = view('client.phase.Update')->with(['title' => "Actualización de la fase : $phase->name"]);
					endif;
					$data->with(['project' => $project, 'phase' => $phase]);
				else:
					if ($request->ajax()):
						$data = "Area no existente";
					else:
						session()->flash('improper', "No intentes algo indebido, la fase no existe");
						$data = redirect(route('client.phase.index', $project->code));
					endif;
				endif;
			else:
				if ($request->ajax()):
					$data = "Area no existente";
				else:
					session()->flash('improper', "No intentes algo indebido, el proyecto no existe");
					$data = redirect(route('client.project.index'));
				endif;
			endif;
			return $data;
		}

	}
