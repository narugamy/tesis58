<?php

	namespace App\Http\Controllers\Client\Project;

	use App\Model\Config;
	use App\Model\Project;
	use App\Model\Resource;
	use App\Model\Role;
	use App\Model\Type;
	use App\Model\User;
	use Carbon\Carbon;
	use Exception;
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use Illuminate\Support\Facades\DB;
	use Yajra\DataTables\DataTables;

	class ProjectController extends Controller {

		public function index() {
				return view('client.project._index')->with(['title' => 'Panel de proyectos', 'class_header' => 'page-container-bg-solid']);
		}

		public function gant($code) {
			$auth = auth()->guard('client')->user();
			$project = Project::where(['code' => $code, 'client_id' => $auth->id])->with('phases.activities.activity', 'phases.activities.tasks.progress', 'phases.activities.tasks.task')->first();
			if ($project):
				return view('client.project._gant')->with(['project' => $this->days($project), 'title' => 'Diagrama de Gant']);
			else:
				return redirect()->route('client.project.index');
			endif;
		}

		public function table(Request $request) {
			if ($request->ajax()):
				try {
					$auth = auth()->guard('client')->user();
					$config = ['end_date_first' => Config::where(['slug' => 'end_date_first'])->first(), 'end_date_last' => Config::where(['slug' => 'end_date_last'])->first()];
					return DataTables::of(Project::withTrashed()->where(['client_id' => $auth->id])->signaling($request, $config)->range_start($request)->range_end($request)->orderBy('name'))->addColumn('link', function ($row) {
						$color = ($row->deleted_at) ? 'red' : 'blue';
						$buttons = "<div class='btn-group'><button class='btn $color btn-xs btn-outline dropdown-toggle' data-toggle='dropdown'>Acciones<i class='fa fa-angle-down'></i></button><ul class='dropdown-menu'>";
						if (!$row->deleted_at):
							$buttons .= "<li><a href='" . route('client.project.gant.index', $row->code) . "'><i class='fa fa-bar-chart-o'></i> Gantt</a></li>";
							$buttons .= "<li><a href='" . route('client.phase.index', $row->code) . "'><i class='fa fa-list-ol'></i> Fases</a></li>";
							$buttons .= "<li><a data-url='" . route('client.project.update', $row->code) . "' class='btn-ajax'><i class='fa fa-pencil'></i> Mostrar</a></li>";
						endif;
						$buttons .= "</ul></div>";
						return $buttons;
					})->addColumn('end', function ($row) use ($config) {
						if(!$row->finalized):
							$dates = ['now' => Carbon::now('America/Lima'), 'end' => new Carbon($row->end_new, 'America/Lima')];
							$dates['now'] = new Carbon($dates['now']->toDateString(), 'America/Lima');
							return $this->signaling($config, $dates) . $row->end;
						else:
							return "<i class='fa fa-circle' style='color: #0070fe'></i> $row->finalized";
						endif;
					})->addIndexColumn()->rawColumns(['link', 'end'])->make(true);
				} catch (Exception $e) {
					return response()->json(["sEcho" => 1, "iTotalRecords" => 0, "iTotalDisplayRecords" => 0, "aaData" => []]);
				}
			else:
				return redirect()->route('client.login');
			endif;
		}

		public function show(Request $request, $code) {
			$project = Project::withTrashed()->with('resources')->where(['code' => $code])->first();
			if ($project):
				$role = Role::where(['slug' => 'client'])->first();
				$clients = User::where(['role_id' => $role->id])->pluck('name', 'id');
				$types = Type::pluck('name', 'id');
				$resources = Resource::pluck('name', 'id');
				if ($request->ajax()):
					$data = view('client.project._Update');
				else:
					$data = view('client.project.Update')->with(['title' => "Actualización del project: $project->name"]);
				endif;
				$data->with(['project' => $project, 'clients' => $clients, 'types' => $types, 'resources' => $resources]);
			else:
				if ($request->ajax()):
					$data = "Proyecto no existente";
				else:
					session()->flash('improper', "No intentes algo indebido, el projecto no existe");
					$data = redirect(route('client.project.index'));
				endif;
			endif;
			return $data;
		}

	}
