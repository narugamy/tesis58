<?php

	namespace App\Http\Controllers\Client\Activity;

	use App\Model\Activity;
	use App\Model\Config;
	use App\Model\Phase;
	use Carbon\Carbon;
	use Exception;
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use Yajra\DataTables\DataTables;

	class ActivityController extends Controller {

		public function index($code) {
			$phase = Phase::where(['code' => $code])->with('project')->first();
			if ($phase):
				$create = $this->authorization($phase->start);
				return view('client.activity._index')->with(['title' => "Panel de Actividades de la fase N° $phase->code", 'class_header' => 'page-container-bg-solid', 'phase' => $phase, 'created' => $create]);
			endif;
			return redirect()->route('client.project.index');
		}

		public function table(Request $request, $code) {
			$phase = Phase::where(['code' => $code])->with('project')->first();
			if ($request->ajax()):
				if ($phase):
					try {
						$config = ['end_date_first' => Config::where(['slug' => 'end_date_first'])->first(), 'end_date_last' => Config::where(['slug' => 'end_date_last'])->first()];
						return DataTables::of(Activity::withTrashed()->signaling($request, $config)->range_start($request)->range_end($request)->where(['phase_id' => $phase->id])->orderBy('name'))->addColumn('link', function ($row) use ($phase) {
							$color = ($row->deleted_at) ? 'red' : 'blue';
							$buttons = "<div class='btn-group'><button class='btn $color btn-xs btn-outline dropdown-toggle' data-toggle='dropdown'>Acciones<i class='fa fa-angle-down'></i></button><ul class='dropdown-menu'>";
							if (!$row->deleted_at):
								$buttons .= "<li><a href='" . route('client.activity.message.index', [$row->code]) . "'><i class='fa fa-comments-o'></i> Mensajes</a></li>";
								$buttons .= "<li><a data-url='" . route('client.activity.update', [$phase->code, $row->code]) . "' class='btn-ajax'><i class='fa fa-pencil'></i> Mostrar</a></li>";
							endif;
							$buttons .= "</ul></div>";
							return $buttons;
						})->addColumn('end', function ($row) use ($config){
							if(!$row->approved):
								$dates = ['now' => Carbon::now('America/Lima'), 'end' => new Carbon($row->end_new, 'America/Lima')];
								$dates['now'] = new Carbon($dates['now']->toDateString(), 'America/Lima');
								return $this->signaling($config, $dates) . $row->end_new;
							else:
								return "<i class='fa fa-circle' style='color: #0070fe'></i> $row->approved";
							endif;
						})->addIndexColumn()->rawColumns(['link', 'end'])->make(true);
					} catch (Exception $e) {
						return response()->json(["sEcho" => 1, "iTotalRecords" => 0, "iTotalDisplayRecords" => 0, "aaData" => []]);
					}
				else:
					return response()->json(["sEcho" => 1, "iTotalRecords" => 0, "iTotalDisplayRecords" => 0, "aaData" => []]);
				endif;
			else:
				if ($phase):
					return redirect()->route('client.project.index');
				endif;
				return redirect()->route('client.login');
			endif;
		}

		public function show(Request $request, $phase_code, $code) {
			$phase = Phase::where(['code' => $phase_code])->with('project')->first();
			if ($phase):
				$activity = Activity::withTrashed()->where(['phase_id' => $phase->id, 'code' => $code])->first();
				if ($activity):
					if ($request->ajax()):
						$data = view('client.activity._Update');
					else:
						$data = view('client.activity.Update')->with(['title' => "Actualización de la actividad : $phase->name"]);
					endif;
					$data->with(['activity' => $activity, 'phase' => $phase]);
				else:
					if ($request->ajax()):
						$data = "Area no existente";
					else:
						session()->flash('improper', "No intentes algo indebido, la actividad no existe");
						$data = redirect(route('client.phase.index', $phase->code));
					endif;
				endif;
			else:
				if ($request->ajax()):
					$data = "Area no existente";
				else:
					session()->flash('improper', "No intentes algo indebido, la actividad no existe");
					$data = redirect(route('client.project.index'));
				endif;
			endif;
			return $data;
		}

	}
