<?php

	namespace App\Http\Requests\Auth;

	use Illuminate\Foundation\Http\FormRequest;

	class LoginRequest extends FormRequest {

		public function authorize() {
			return true;
		}

		public function rules() {
			return [
				'username' => 'required|min:4|max:21|exists:user',
				'password' => 'required|min:4|max:255'
			];
		}

		public function messages(){
			return [
				'username.required'=>'requerido',
				'username.exists'=>'el usuario no existe',
				'username.min'=>'min. :min caracteres',
				'username.max'=>'max. :max caracteres',
				'password.required'=>'requerido',
				'password.min'=>'min. :min caracteres',
				'password.max'=>'max. :max caracteres'
			];
		}

	}
