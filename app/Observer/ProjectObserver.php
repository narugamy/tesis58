<?php

	namespace App\Observer;

	use App\Model\Project;

	class ProjectObserver {

		public function creating(Project $project) {
			$code = str_pad($this->stringer(Project::withTrashed()->max('code'))+1, 6, "0", STR_PAD_LEFT);
			$project->code = "P".$code;
		}

		private function stringer($code){
			return (int)substr($code, 1);
		}

	}