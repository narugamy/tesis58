<?php

	namespace App\Observer;

	use App\Model\User;

	class UserObserver {

		public function creating(User $user) {
			$user->password = bcrypt($user->password);
		}

	}