<?php

	namespace App\Observer;

	use App\Model\Company;
	use Illuminate\Support\Str;

	class CompanyObserver {

		public function creating(Company $company) {
			$company->slug = Str::slug(strtolower($company->name));
		}

	}