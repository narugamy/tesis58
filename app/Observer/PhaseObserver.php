<?php

	namespace App\Observer;

	use App\Model\Phase;

	class PhaseObserver {

		public function creating(Phase $phase) {
			$code = str_pad($this->stringer(Phase::withTrashed()->max('code'))+1, 6, "0", STR_PAD_LEFT);
			$phase->code = "F".$code;
		}

		private function stringer($code){
			return (int)substr($code, 1);
		}

	}