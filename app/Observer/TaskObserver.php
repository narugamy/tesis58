<?php

	namespace App\Observer;

	use App\Model\Task;

	class TaskObserver {

		public function creating(Task $task) {
			if(!empty($task->task_id)):
				$father = Task::find($task->task_id);
				$temp = Task::where(['task_id' => $task->task_id])->orderByRaw("1*SUBSTRING_INDEX(structure, '.', 1) desc")->first();
				if ($temp):
					$array = explode('.', $temp->structure);
					$count = count($array);
					$number = (int)$array[$count] + 1;
				else:
					$number = 1;
				endif;
				$task->fill(['structure' => $father->structure . "." . $number, 'father_id' => $father->father_id??$father->id]);
			else:
				$temp = Task::whereNull('task_id')->orderByRaw("1*SUBSTRING_INDEX(structure, '.', 1) desc")->first();
				$task->fill(['structure' => ((int)$temp->structure+1)]);
			endif;
			$task->fill(['start_new' => $task->start, 'end_new' => $task->end]);
			$code = str_pad($this->stringer(Task::withTrashed()->max('code'))+1, 6, "0", STR_PAD_LEFT);
			$task->code = "T".$code;
		}

		private function stringer($code){
			return (int)substr($code, 1);
		}

	}