<?php

	namespace App\Observer;

	use App\Model\Activity;

	class ActivityObserver {

		public function creating(Activity $activity) {
			if(!empty($activity->activity_id)):
				$father = Activity::find($activity->activity_id);
				$temp = Activity::where(['activity_id' => $activity->activity_id])->orderByRaw("1*SUBSTRING_INDEX(structure, '.', 1) desc")->first();
				if ($temp):
					$array = explode('.', $temp->structure);
					$count = count($array);
					$number = (int)$array[$count] + 1;
				else:
					$number = 1;
				endif;
				$activity->fill(['structure' => $father->structure . "." . $number, 'father_id' => $father->father_id??$father->id]);
			else:
				$temp = Activity::whereNull('activity_id')->orderByRaw("1*SUBSTRING_INDEX(structure, '.', 1) desc")->first();
				$activity->fill(['structure' => ((int)$temp->structure+1)]);
			endif;
			$activity->fill(['start_new' => $activity->start, 'end_new' => $activity->end]);
			$code = str_pad($this->stringer(Activity::withTrashed()->max('code'))+1, 6, "0", STR_PAD_LEFT);
			$activity->code = "A".$code;
		}

		private function stringer($code){
			return (int)substr($code, 1);
		}

	}