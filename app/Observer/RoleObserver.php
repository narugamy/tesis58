<?php

	namespace App\Observer;


	use App\Model\Role;
	use Illuminate\Support\Str;

	class RoleObserver {

		public function creating(Role $role) {
			$role->slug = Str::slug(strtolower($role->name));
		}

	}