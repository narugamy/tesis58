<?php

	namespace App\Observer;

	use App\Model\Resource;
	use Illuminate\Support\Str;

	class ResourceObserver {

		public function creating(Resource $resource) {
			$resource->slug = Str::slug(strtolower($resource->name));
		}

	}