<?php

	namespace App\Observer;

	use App\Model\History;

	class HistoryObserver {

		public function creating(History $history) {
			$code = str_pad($this->stringer(History::withTrashed()->max('code'))+1, 6, "0", STR_PAD_LEFT);
			$history->code = "H".$code;
		}

		private function stringer($code){
			return (int)substr($code, 1);
		}

	}