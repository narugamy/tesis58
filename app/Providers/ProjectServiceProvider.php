<?php

	namespace App\Providers;

	use App\Model\Project;
	use App\Observer\ProjectObserver;
	use Illuminate\Support\ServiceProvider;

	class ProjectServiceProvider extends ServiceProvider {

		public function register() {
			//
		}

		public function boot() {
			Project::observe(ProjectObserver::class);
		}
	}
