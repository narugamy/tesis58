<?php

	namespace App\Providers;

	use App\Model\Phase;
	use App\Observer\PhaseObserver;
	use Illuminate\Support\ServiceProvider;

	class PhaseServiceProvider extends ServiceProvider {

		public function register() {
			//
		}

		public function boot() {
			Phase::observe(PhaseObserver::class);
		}
	}
