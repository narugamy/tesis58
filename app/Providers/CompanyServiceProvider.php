<?php

	namespace App\Providers;

	use App\Model\Company;
	use App\Observer\CompanyObserver;
	use Illuminate\Support\ServiceProvider;

	class CompanyServiceProvider extends ServiceProvider {

		public function register() {
			//
		}

		public function boot() {
			Company::observe(CompanyObserver::class);
		}
	}
