<?php

	namespace App\Providers;

	use App\Model\Activity;
	use App\Observer\ActivityObserver;
	use Illuminate\Support\ServiceProvider;

	class ActivityServiceProvider extends ServiceProvider {

		public function register() {
			//
		}

		public function boot() {
			Activity::observe(ActivityObserver::class);
		}
	}
