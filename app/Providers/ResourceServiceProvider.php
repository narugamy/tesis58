<?php

	namespace App\Providers;

	use App\Model\Resource;
	use App\Observer\ResourceObserver;
	use Illuminate\Support\ServiceProvider;

	class ResourceServiceProvider extends ServiceProvider {

		public function register() {
			//
		}

		public function boot() {
			Resource::observe(ResourceObserver::class);
		}
	}
