<?php

	namespace App\Providers;

	use App\Model\History;
	use App\Observer\HistoryObserver;
	use Illuminate\Support\ServiceProvider;

	class HistoryServiceProvider extends ServiceProvider {

		public function register() {
			//
		}

		public function boot() {
			History::observe(HistoryObserver::class);
		}
	}
