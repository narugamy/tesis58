<?php

	namespace App\Providers;

	use App\Model\User;
	use App\Observer\UserObserver;
	use Illuminate\Support\ServiceProvider;

	class UserServiceProvider extends ServiceProvider {

		public function register() {
			//
		}

		public function boot() {
			User::observe(UserObserver::class);
		}
	}
