<?php

	namespace App\Providers;

	use App\Model\Task;
	use App\Observer\TaskObserver;
	use Illuminate\Support\ServiceProvider;

	class TaskServiceProvider extends ServiceProvider {

		public function register() {
			//
		}

		public function boot() {
			Task::observe(TaskObserver::class);
		}
	}
