<?php

	namespace App\Providers;

	use App\Model\Role;
	use App\Observer\RoleObserver;
	use Illuminate\Support\ServiceProvider;

	class RoleServiceProvider extends ServiceProvider {

		public function register() {
			//
		}

		public function boot() {
			Role::observe(RoleObserver::class);
		}
	}
