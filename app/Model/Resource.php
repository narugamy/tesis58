<?php

	namespace App\Model;

	use Illuminate\Database\Eloquent\Model;
	use Illuminate\Database\Eloquent\SoftDeletes;

	class Resource extends Model {

		use SoftDeletes;

		protected $table = 'resource';

		protected $fillable = ['name', 'description', 'slug',];

		public function projects() {
			return $this->belongsToMany(Project::class);
		}

	}
