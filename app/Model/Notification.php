<?php

	namespace App\Model;

	use Illuminate\Database\Eloquent\Model;
	use Illuminate\Database\Eloquent\SoftDeletes;

	class Notification extends Model {

		use SoftDeletes;

		protected $table = 'notification';

		protected $fillable = ['message_id', 'user_id', 'route'];

		public function message() {
			return $this->belongsTo(Message::class);
		}

		public function user() {
			return $this->belongsTo(User::class);
		}

	}
