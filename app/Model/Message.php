<?php

	namespace App\Model;

	use Illuminate\Database\Eloquent\Model;
	use Illuminate\Database\Eloquent\SoftDeletes;

	class Message extends Model {

		use SoftDeletes;

		protected $table = 'message';

		protected $fillable = ['table_id', 'table_type', 'archive_id', 'user_id', 'employee_id', 'name', 'description'];

		public function user() {
			return $this->belongsTo(User::class);
		}

		public function archive() {
			return $this->belongsTo(Archive::class);
		}

		public function activities() {
			return $this->hasMany(Activity::class, 'id', 'table_id');
		}

		public function tasks() {
			return $this->hasMany(Task::class, 'id', 'table_id');
		}

		public function employee() {
			return $this->belongsTo(User::class, 'employee_id');
		}

	}
