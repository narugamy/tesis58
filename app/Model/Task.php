<?php

	namespace App\Model;

	use Illuminate\Database\Eloquent\Model;
	use Illuminate\Database\Eloquent\SoftDeletes;

	class Task extends Model {

		use SoftDeletes;

		protected $table = 'task';

		protected $fillable = ['activity_id', 'father_id', 'task_id', 'user_id', 'structure', 'code', 'name', 'description', 'start', 'end', 'start_new', 'end_new', 'approved',];

		public function parent() {
			return $this->belongsTo(Task::class, 'task_id', 'id')->with('parent');
		}

		public function father() {
			return $this->belongsTo(Task::class, 'father_id');
		}

		public function tasks() {
			return $this->hasMany(Task::class, 'task_id')->with('tasks');
		}

		public function activity() {
			return $this->belongsTo(Activity::class);
		}

		public function task() {
			return $this->belongsTo(Task::class);
		}

		public function user() {
			return $this->belongsTo(User::class);
		}

		public function progress() {
			return $this->hasOne(Progress::class)->whereNotNull('approved');
		}

		public function history() {
			return $this->hasOne(History::class);
		}

		public function containers() {
			return $this->hasMany(Container::class);
		}

		public function progresses() {
			return $this->hasMany(Progress::class);
		}

		public function getFullNameAttribute() {
			return "{$this->code} {$this->name}";
		}

		public function scopeRange_start($query, $request) {
			$between = (empty($request->start_from) || empty($request->start_to)) ? null : [$request->start_from, $request->start_to];
			return !empty($between) ? $query->WhereBetween('start', $between): $query;
		}

		public function scopeSignaling($query, $request, $config) {
			$between = (empty($request->signaling)) ? null : $request->signaling;
			if ($between):
				switch ($between):
					case 'red':
						return $query->whereRaw("datediff(end,NOW()) < 0")->whereNull('approved');
						break;
					case 'yellow':
						return $query->whereRaw("(datediff(end,NOW()) >= 0 and datediff(end,NOW()) < " . $config['end_date_first']->value . ")")->whereNull('approved');
						break;
					default:
						return $query->whereRaw("datediff(end,NOW()) >=" . $config['end_date_first']->value)->whereNull('approved');
						break;
				endswitch;
			else:
				return $query;
			endif;
		}

		public function scopeRange_end($query, $request) {
			$between = (empty($request->end_from) || empty($request->end_to)) ? null : [$request->end_from, $request->end_to];
			return !empty($between) ? $query->orWhereBetween('end', $between) : $query;
		}

	}
