<?php

	namespace App\Model;

	use Illuminate\Database\Eloquent\Model;
	use Illuminate\Database\Eloquent\SoftDeletes;

	class Activity extends Model {

		use SoftDeletes;

		protected $table = 'activity';

		protected $fillable = ['activity_id', 'father_id', 'phase_id', 'user_id', 'structure', 'code', 'name', 'description', 'start', 'end', 'start_new', 'end_new', 'approved'];

		public function parent() {
			return $this->belongsTo(Activity::class, 'activity_id', 'id')->with('parent');
		}

		public function activities() {
			return $this->hasMany(Activity::class, 'activity_id')->with('activities');
		}

		public function phase() {
			return $this->belongsTo(Phase::class);
		}

		public function activity() {
			return $this->belongsTo(Activity::class);
		}

		public function tasks() {
			return $this->hasMany(Task::class);
		}

		public function getFullNameAttribute() {
			return "{$this->code} {$this->name}";
		}

		public function scopeSignaling($query, $request, $config) {
			$between = (empty($request->signaling)) ? null : $request->signaling;
			if($between):
				switch ($between):
					case 'red':
						return $query->whereRaw("datediff(end_new,NOW()) < 0")->whereNull('approved');
						break;
					case 'yellow':
						return $query->whereRaw("(datediff(end_new,NOW()) >= 0 and datediff(end_new,NOW()) < ".$config['end_date_first']->value.")")->whereNull('approved');
						break;
					default:
						return $query->whereRaw("datediff(end_new,NOW()) >=".$config['end_date_first']->value)->whereNull('approved');
						break;
				endswitch;
			else:
				return $query;
			endif;
		}

		public function scopeRange_start($query, $request) {
			$between = (empty($request->start_from) || empty($request->start_to)) ? null : [$request->start_from, $request->start_to];
			return !empty($between) ? $query->orWhereBetween('start_new', $between) : $query;
		}

		public function scopeRange_end($query, $request) {
			$between = (empty($request->end_from) || empty($request->end_to)) ? null : [$request->end_from, $request->end_to];
			return !empty($between) ? $query->orWhereBetween('end_new', $between) : $query;
		}

	}
