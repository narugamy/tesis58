<?php

	namespace App\Model;

	use Illuminate\Database\Eloquent\Model;
	use Illuminate\Database\Eloquent\SoftDeletes;

	class Config extends Model {

		use SoftDeletes;

		protected $table = 'config';

		protected $fillable = ['name', 'key', 'value', 'slug'];

	}
