<?php

	namespace App\Model;

	use Illuminate\Database\Eloquent\Model;
	use Illuminate\Database\Eloquent\SoftDeletes;

	class History extends Model {

		use SoftDeletes;

		protected $table = 'history';

		protected $fillable = ['task_id', 'code', 'name', 'description'];

		public function task() {
			return $this->belongsTo(Task::class);
		}

	}
