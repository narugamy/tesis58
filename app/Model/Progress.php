<?php

	namespace App\Model;

	use Illuminate\Database\Eloquent\Model;
	use Illuminate\Database\Eloquent\SoftDeletes;

	class Progress extends Model {

		use SoftDeletes;

		protected $table = 'progress';

		protected $fillable = ['task_id', 'user_id', 'name', 'description', 'progress', 'approved'];

		public function task() {
			return $this->belongsTo(Task::class);
		}

		public function user() {
			return $this->belongsTo(User::class);
		}

	}
