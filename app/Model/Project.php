<?php

	namespace App\Model;

	use Illuminate\Database\Eloquent\Model;
	use Illuminate\Database\Eloquent\SoftDeletes;

	class Project extends Model {

		use SoftDeletes;

		protected $table = 'project';

		protected $fillable = ['client_id', 'manager_id', 'type_id', 'code', 'name', 'description', 'start', 'end', 'start_new', 'end_new', 'price_base','price_real', 'finalized',];

		public function client() {
			return $this->belongsTo(User::class, 'client_id');
		}

		public function manager() {
			return $this->belongsTo(User::class, 'manager_id');
		}

		public function type() {
			return $this->belongsTo(Type::class);
		}

		public function phases() {
			return $this->hasMany(Phase::class);
		}

		public function scopeSignaling($query, $request, $config) {
			$between = (empty($request->signaling)) ? null : $request->signaling;
			if($between):
				switch ($between):
					case 'red':
						return $query->whereRaw("datediff(end,NOW()) < 0")->whereNull('approved');
						break;
					case 'yellow':
						return $query->whereRaw("(datediff(end,NOW()) >= 0 and datediff(end,NOW()) < ".$config['end_date_first']->value.")")->whereNull('approved');
						break;
					default:
						return $query->whereRaw("datediff(end,NOW()) >=".$config['end_date_first']->value)->whereNull('approved');
						break;
				endswitch;
			else:
				return $query;
			endif;
		}

		public function scopeRange_start($query, $request) {
			$between = (empty($request->start_from) || empty($request->start_to)) ? null : [$request->start_from, $request->start_to];
			return !empty($between) ? $query->orWhereBetween('start', $between) : $query;
		}

		public function scopeRange_end($query, $request) {
			$between = (empty($request->end_from) || empty($request->end_to)) ? null : [$request->end_from, $request->end_to];
			return !empty($between) ? $query->orWhereBetween('end', $between) : $query;
		}

	}
