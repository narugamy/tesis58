<?php

	namespace App\Model;

	use Illuminate\Database\Eloquent\Model;
	use Illuminate\Database\Eloquent\SoftDeletes;

	class Company extends Model {

		use SoftDeletes;

		protected $table = 'company';

		protected $fillable = ['name', 'slug',];

		public function users() {
			return $this->hasMany(User::class);
		}

	}
