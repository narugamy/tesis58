<?php

	namespace App\Model;

	use Illuminate\Database\Eloquent\Model;
	use Illuminate\Database\Eloquent\SoftDeletes;

	class Role extends Model {

		use SoftDeletes;

		protected $table = 'role';

		protected $fillable = ['name', 'slug',];

		public function users() {
			return $this->hasMany(User::class);
		}

	}
