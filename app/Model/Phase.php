<?php

	namespace App\Model;

	use Illuminate\Database\Eloquent\Model;
	use Illuminate\Database\Eloquent\SoftDeletes;

	class Phase extends Model {

		use SoftDeletes;

		protected $table = 'phase';

		protected $fillable = ['project_id', 'code', 'name', 'description', 'start', 'end',];

		public function project() {
			return $this->belongsTo(Project::class);
		}

		public function activities() {
			return $this->hasMany(Activity::class);
		}

		public function scopeSignaling($query, $request, $config) {
			$between = (empty($request->signaling)) ? null : $request->signaling;
			if($between):
				switch ($between):
					case 'red':
						return $query->whereRaw("datediff(end,NOW()) < 0");
						break;
					case 'yellow':
						return $query->whereRaw("(datediff(end,NOW()) >= 0 and datediff(end,NOW()) < ".$config['end_date_first']->value.")");
						break;
					default:
						return $query->whereRaw("datediff(end,NOW()) >=".$config['end_date_first']->value);
						break;
				endswitch;
			else:
				return $query;
			endif;
		}

		public function scopeRange_start($query, $request) {
			$between = (empty($request->start_from) || empty($request->start_to)) ? null : [$request->start_from, $request->start_to];
			return !empty($between) ? $query->orWhereBetween('start', $between) : $query;
		}

		public function scopeRange_end($query, $request) {
			$between = (empty($request->end_from) || empty($request->end_to)) ? null : [$request->end_from, $request->end_to];
			return !empty($between) ? $query->orWhereBetween('end', $between) : $query;
		}

	}
