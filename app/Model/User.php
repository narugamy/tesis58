<?php

	namespace App\Model;

	use Illuminate\Database\Eloquent\SoftDeletes;
	use Illuminate\Notifications\Notifiable;
	use Illuminate\Contracts\Auth\MustVerifyEmail;
	use Illuminate\Foundation\Auth\User as Authenticatable;

	class User extends Authenticatable {

		use Notifiable, SoftDeletes;

		protected $table = 'user';

		protected $fillable = ['company_id', 'role_id', 'name', 'surname', 'address', 'email', 'username', 'password',];

		protected $hidden = ['password', 'remember_token',];

		protected $casts = ['email_verified_at' => 'datetime',];

		public function company() {
			return $this->belongsTo(Company::class);
		}

		public function role() {
			return $this->belongsTo(Role::class);
		}

		public function is_role($role) {
			return $this->role->slug === $role;
		}

		public function getFullNameAttribute() {
			return "{$this->surname} {$this->name}";
		}

	}
