<?php

	namespace App\Model;

	use Illuminate\Database\Eloquent\Model;
	use Illuminate\Database\Eloquent\SoftDeletes;

	class Container extends Model {

		use SoftDeletes;

		protected $table = 'container';

		protected $fillable = ['task_id', 'resource_id', 'number', 'price', 'total',];

		public function task() {
			return $this->belongsTo(Task::class);
		}

		public function resource() {
			return $this->belongsTo(Resource::class);
		}

	}
