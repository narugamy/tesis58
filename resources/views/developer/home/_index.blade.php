@extends('developer.layout.layout')
@section('container')
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<span>Home</span>
				</li>
			</ul>
		</div>
		<!-- END PAGE BAR -->
		<!-- BEGIN PAGE TITLE-->
		<h1 class="page-title"> {{ $title }}</h1>
		<!-- END PAGE TITLE-->
		<!-- END PAGE HEADER-->
		<div class="row">
			<div class="col-xs-12">
				<div id='calendar'></div>
			</div>
		</div>
	</div>
@endsection
@section('styles')
	<link rel="stylesheet" href="{{ asset('plugins/fullcalendar/core/main.min.css') }}">
	<link rel="stylesheet" href="{{ asset('plugins/fullcalendar/daygrid/main.min.css') }}">
	<link rel="stylesheet" href="{{ asset('plugins/fullcalendar/timegrid/main.min.css') }}">
	<link rel="stylesheet" href="{{ asset('plugins/fullcalendar/list/main.min.css') }}">
@endsection
@section('scripts')
	<script src="{{ asset('plugins/fullcalendar/core/main.min.js') }}"></script>
	<script src="{{ asset('plugins/fullcalendar/core/locales-all.min.js') }}"></script>
	<script src="{{ asset('plugins/fullcalendar/interaction/main.js') }}"></script>
	<script src="{{ asset('plugins/fullcalendar/daygrid/main.min.js') }}"></script>
	<script src="{{ asset('plugins/fullcalendar/timegrid/main.min.js') }}"></script>
	<script src="{{ asset('plugins/fullcalendar/list/main.min.js') }}"></script>
	<script>
		$(document).ready(function () {
			let element = document.getElementById('calendar');
			let calendar = new FullCalendar.Calendar(element, {
				header: {
					left: 'prev,next today',
					center: 'title',
					right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'
				},
				defaultView: 'dayGridMonth',
				plugins: ['interaction', 'dayGrid', 'timeGrid', 'list' ],
				locale: 'es',
				businessHours: true,
				eventLimit: true,
				events: [ @foreach($activities as $activity) { title: "Actividad {{$activity->code}} : {{$activity->name}}", start: "{{$activity->end_new}}", end: "{{$activity->end_new}}",}, @foreach($activity->tasks as $task) { title: "Tarea {{$task->code}} : {{$task->name}}", start: "{{$task->end_new}}", end: "{{$task->end_new}}", @if((int)$task->user_id === (int)$auth->id) color: 'blue', @endif }, @endforeach @endforeach ]
			});
			calendar.render();
		});
	</script>
@endsection