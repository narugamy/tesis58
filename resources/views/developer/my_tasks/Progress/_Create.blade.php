<div class="portlet light bordered wizzard portlet-form">
	<div class="portlet-body form">
		{{ Form::open(['route' => ['developer.my_tasks.progress.create', $task->code], 'class' => 'form-horizontal form-file']) }}
		@include('developer.my_tasks.Progress._From_Create')
		{{ Form::close() }}
	</div>
</div>