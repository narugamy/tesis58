@extends('developer.layout.layout')
@section('container')
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<a href="{{ route('developer.index') }}">Home</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<a href="{{ route('developer.activity.index') }}">Actividad</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<a href="{{ route('developer.task.index', $activity->code ) }}">Tarea</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<span>Actualizar</span>
				</li>
			</ul>
		</div>
		<!-- END PAGE BAR -->
		<!-- BEGIN PAGE TITLE-->
		<h1 class="page-title"> {{ $title }}
		</h1>
		<div class="form-wizzard">
			@if(session('error'))
				<div class="alert alert-danger fade in">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					{{ session('error') }}
				</div>
			@endif
			@if(count($errors) > 0)
				<div class="alert alert-danger fade in">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<ul>
						@foreach($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif
			<div class="portlet light bordered wizzard portlet-form">
				<div class="portlet-body form">
					{{ Form::open(['route' => ['developer.task.update', $activity->code, $task->code], 'class' => 'form-horizontal form-submit', 'method' => 'put']) }}
					@include('developer.task._From_Update')
					{{ Form::close() }}
				</div>
			</div>
		</div>
	</div>
@endsection