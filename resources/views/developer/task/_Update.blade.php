<div class="portlet light bordered wizzard portlet-form">
	<div class="portlet-body form">
		{{ Form::open(['route' => ['developer.task.update', $activity->code, $task->code], 'class' => 'form-horizontal form-modal', 'method' => 'put']) }}
		@include('developer.task._From_Update')
		{{ Form::close() }}
	</div>
</div>