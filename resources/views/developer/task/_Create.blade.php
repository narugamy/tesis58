<div class="portlet light bordered wizzard portlet-form">
	<div class="portlet-body form">
		{{ Form::open(['route' => ['developer.task.create', $activity->code], 'class' => 'form-horizontal form-modal']) }}
		@include('developer.task._From_Create')
		{{ Form::close() }}
	</div>
</div>