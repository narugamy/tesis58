<div class="portlet light bordered wizzard portlet-form">
	<div class="portlet-body form">
		{{ Form::open(['route' => ['admin.company.update', $company->slug], 'class' => 'form-horizontal form-modal', 'method' => 'put']) }}
		@include('admin.company._From_Update')
		{{ Form::close() }}
	</div>
</div>