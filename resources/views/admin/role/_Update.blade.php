<div class="portlet light bordered wizzard portlet-form">
	<div class="portlet-body form">
		{{ Form::open(['route' => ['admin.role.update', $role->slug], 'class' => 'form-horizontal form-modal', 'method' => 'put']) }}
		@include('admin.role._From_Update')
		{{ Form::close() }}
	</div>
</div>