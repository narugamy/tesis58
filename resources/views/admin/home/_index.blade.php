@extends('admin.layout.layout')
@section('container')
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<span>Home</span>
				</li>
			</ul>
		</div>
		<!-- END PAGE BAR -->
		<!-- BEGIN PAGE TITLE-->
		<h1 class="page-title"> {{ $title }}</h1>
		<!-- END PAGE TITLE-->
		<!-- END PAGE HEADER-->
		<div class="row">
		</div>
	</div>
@endsection