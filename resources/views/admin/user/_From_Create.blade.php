<div class="form-wizard">
	<div class="form-body">
		<ul class="nav nav-pills nav-justified steps">
			<li>
				<a href="#tab1" data-toggle="tab" class="step">
					<span class="number"> 1 </span>
					<span class="desc">
						<i class="fa fa-check"></i> Principal
					</span>
				</a>
			</li>
			<li>
				<a href="#tab2" data-toggle="tab" class="step">
					<span class="number"> 2 </span>
					<span class="desc">
						<i class="fa fa-check"></i> Cuenta
					</span>
				</a>
			</li>
		</ul>
		<div id="bar" class="progress progress-striped" role="progressbar">
			<div class="progress-bar progress-bar-success active"></div>
		</div>
		<div class="tab-content portlet-body">
			<div class="tab-pane active" id="tab1">
				<div class="form-body">
					<div class="form-group form-md-line-input form-md-floating-label">
						<input type="text" class="form-control text-texter" name="name" id="name" data-rule-required="true" data-rule-minlength="3" data-rule-maxlength="191" minlength="3" maxlength="191" required>
						<label for="name">Nombres</label>
					</div>
					<div class="form-group form-md-line-input form-md-floating-label">
						<input type="text" class="form-control text-texter" name="surname" id="surname" data-rule-required="true" data-rule-minlength="3" data-rule-maxlength="191" minlength="3" maxlength="191" required>
						<label for="surname">Apellidos</label>
					</div>
					<div class="form-group form-md-line-input form-md-floating-label">
						<input type="email" class="form-control" name="email" id="email" data-rule-required="true" data-rule-minlength="5" data-rule-maxlength="191" data-rule-email="true" minlength="5" maxlength="191" required>
						<label for="email">Correo</label>
					</div>
					<div class="form-group form-md-line-input form-md-floating-label">
						<textarea class="form-control" name="address" id="address" data-rule-required="true" data-rule-minlength="4" minlength="4" required></textarea>
						<label for="address">Dirección</label>
					</div>
					<div class="form-group form-md-line-input form-md-floating-label">
						{{ Form::select('role_id', $roles,  null, ['id' => 'role_id', 'required' => '', 'placeholder' => 'Seleccionar una', 'data-rule-required' => 'true', 'class' => 'form-control edited']) }}
						<label for="role_id">Rol</label>
					</div>
				</div>
				<div class="dlearfix"></div>
			</div>
			<div class="tab-pane active" id="tab2">
				<div class="form-body">
					<div class="form-group form-md-line-input form-md-floating-label">
						<input type="text" class="form-control" name="username" id="username" data-rule-required="true" data-rule-minlength="4" data-rule-maxlength="191" minlength="4" maxlength="191" required>
						<label for="username">Usuario</label>
					</div>
					<div class="form-group form-md-line-input form-md-floating-label">
						<input type="password" class="form-control" name="password" id="password" data-rule-required="true" data-rule-minlength="4" data-rule-maxlength="20" data-rule-password="true" minlength="4" maxlength="20" required>
						<label for="password">Contraseña</label>
					</div>
					<div class="form-group form-md-line-input form-md-floating-label">
						{{ Form::select('company_id', $companies,  null, ['id' => 'company_id', 'placeholder' => 'Seleccionar una', 'class' => 'form-control edited']) }}
						<label for="company_id">Compañia</label>
					</div>
				</div>
				<div class="form-group form-md-line-input form-md-floating-label text-center">
					<button class="btn green button-submit"> Crear
						<i class="fa fa-check"></i>
					</button>
				</div>
				<div class="clearfix"></div>
			</div>
			<ul class="pager wizard">
				<li class="previous">
					<a class="btn default button-previous"><i class="fa fa-angle-left"></i>Anterior</a>
				</li>
				<li class="next">
					<a class="btn btn-outline green button-next">Siguiente <i class="fa fa-angle-right"></i></a>
				</li>
			</ul>
		</div>
	</div>
</div>