<div class="portlet light bordered wizzard portlet-form">
	<div class="portlet-body form">
		{{ Form::open(['route' => ['admin.config.update', $config->slug], 'class' => 'form-horizontal form-modal', 'method' => 'put']) }}
		@include('admin.config._From_Update')
		{{ Form::close() }}
	</div>
</div>