@extends('manager.layout.layout')
@section('container')
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<a href="{{ route('manager.index') }}">Home</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<a href="{{ route('manager.project.index') }}">Proyectos</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<span>Recursos</span>
				</li>
			</ul>
			@if(!$created)
				<div class="page-toolbar">
					<div class="btn-group pull-right">
						<a data-url="{{ route('manager.resource.create', $project->code) }}" class="btn green btn-sm btn-outline btn-ajax"> Crear</a>
					</div>
				</div>
			@endif
		</div>
		<h1 class="page-title"> {{ $title }}
		</h1>
		@if(session('success'))
			<div class="alert alert-success fade in">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				{{ session('success') }}
			</div>
		@elseif(session('improper'))
			<div class="alert alert-danger fade in">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				{{ session('improper') }}
			</div>
		@endif
		<div class="portlet green box">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-globe"></i>Lista
				</div>
				<div class="tools"></div>
			</div>
			<div class="portlet-body">
				<table class="table table-striped table-bordered table-hover dt-responsive data-table" width="100%" id="sample_1">
					<thead>
					<tr>
						<th class="all order-now">N°</th>
						<th class="all">Nombre</th>
						<th>Cantidad</th>
						<th>Precio Unitario</th>
						<th class="all acciones">Opciones</th>
					</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="ajax" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-body">
				</div>
			</div>
		</div>
	</div>
@endsection
@section('scripts')
	<script src="{{ asset('js/datatables.min.js') }}" type="text/javascript"></script>
	<script type="text/javascript">
		let datatable = $(document).find('.data-table').DataTable({
			"serverSide": true,
			"processing": true,
			"ajax": "{{ route('manager.resource.table', $project->code) }}",
			"columns": [
				{data: 'DT_RowIndex', orderable: false, searchable: false},
				{data: 'resource.name'},
				{data: 'number'},
				{data: 'price'},
				{data: 'link'},
			],
			"aLengthMenu": [[5, 10, 15, -1], [5, 10, 15, 'All']], "language": {
				"buttons": {
					copyTitle: 'Copiado en porta papeles', copySuccess: {
						_: '%d lineas copiadas', 1: 'una linea copiada'
					}
				},
				"aria": {
					"sortAscending": ": Actilet para ordenar la columna de manera ascendente",
					"sortDescending": ": Actilet para ordenar la columna de manera descendente"
				},
				"infoFiltered": "(filtrado  de un total de _MAX_ registros)",
				"lengthMenu": `<span class="seperator"></span>Mostrar _MENU_ registros`,
				"sProcessing": "Procesando...",
				"info": `<span class="seperator"></span>Mostrando registros del _START_ al _END_`,
				"infoEmpty": "Mostrando registros del 0 al 0",
				"emptyTable": "Ningún dato disponible en esta tabla",
				"search": '<i class="fa fa-search"></i>',
				"paginate": {
					"previous": '<i class="fa fa-angle-left"></i>', "next": '<i class="fa fa-angle-right"></i>'
				},
				"zeroRecords": "No se encontraron resultados"
			}, "dom": "Blfrtip", buttons: [{
				extend: 'print',
				className: 'dt-button btn dark',
				text: `<i class="fa fa-print"></i>`,
				exportOptions: {columns: ':not(.acciones)'}
			}, {
				extend: 'copy',
				className: 'dt-button btn red',
				text: `<i class="fa fa-clipboard"></i>`,
				exportOptions: {columns: ':not(.acciones)'}
			}, {
				extend: 'pdf',
				className: 'dt-button btn blue',
				text: `<i class="fa fa-file-pdf-o"></i>`,
				defaultStyle: {alignment: "center"},
				exportOptions: {columns: ':not(.acciones)'},
				orientation: 'landscape',
				customize: function (doc) {
					let colCount = [];
					t.find("tbody tr:first-child td:not(.row-acction)").each(function () {
						if ($(this).attr('colspan')) {
							for (let i = 0; i < $(this).attr('colspan'); i++) {
								colCount.push('*');
							}
						} else {
							colCount.push('*');
						}
					});
					doc.styles.tableBodyOdd = {alignment: "center"};
					doc.styles.tableBodyEven = {alignment: "center"};
					doc.content[1].table.widths = '*';
				}
			}, {
				extend: 'excel',
				className: 'dt-button btn yellow',
				text: `<i class="far fa-file-excel-o"></i>`,
				exportOptions: {columns: ":not(.acciones)"},
			}, {
				extend: 'colvis', className: 'dt-button btn purple', text: `<i class="fa fa-list-ul"></i>`
			}], responsive: true
		});
	</script>
@endsection