<div class="portlet light bordered wizzard portlet-form">
	<div class="portlet-body form">
		{{ Form::open(['route' => ['manager.resource.create', $project->code], 'class' => 'form-horizontal form-modal']) }}
		@include('manager.resource._From_Create')
		{{ Form::close() }}
	</div>
</div>