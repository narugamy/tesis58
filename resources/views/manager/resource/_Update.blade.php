<div class="portlet light bordered wizzard portlet-form">
	<div class="portlet-body form">
		{{ Form::open(['route' => ['manager.resource.update', $project->code, $container->id], 'class' => 'form-horizontal form-modal', 'method' => 'put']) }}
		@include('manager.resource._From_Update')
		{{ Form::close() }}
	</div>
</div>