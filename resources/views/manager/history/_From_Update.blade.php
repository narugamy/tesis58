<div class="form-wizard">
	<div class="form-body">
		<ul class="nav nav-pills nav-justified steps">
			<li>
				<a href="#tab1" data-toggle="tab" class="step">
					<span class="number"> 1 </span>
					<span class="desc">
						<i class="fa fa-check"></i> Principal
					</span>
				</a>
			</li>
		</ul>
		<div id="bar" class="progress progress-striped" role="progressbar">
			<div class="progress-bar progress-bar-success active"></div>
		</div>
		<div class="tab-content portlet-body">
			<div class="tab-pane active" id="tab1">
				<div class="form-body">
					<div class="form-group form-md-line-input form-md-floating-label">
						<input type="text" class="form-control" id="code" data-rule-required="true" data-rule-minlength="3" data-rule-maxlength="191" minlength="3" maxlength="191" value="{{ $history->code }}" disabled>
						<label for="code">Codigo</label>
					</div>
					<div class="form-group form-md-line-input form-md-floating-label">
						<input type="text" class="form-control" id="name" data-rule-required="true" data-rule-minlength="3" data-rule-maxlength="191" minlength="3" maxlength="191" value="{{ $history->name}}" disabled>
						<label for="name">Nombre</label>
					</div>
					<div class="form-group form-md-line-input form-md-floating-label">
						<textarea type="text" id="description" class="form-control" data-rule-required="true" disabled>{{ $history->description }}</textarea>
						<label for="description">Descripción</label>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function () {
		$(document).find("#resources").select2({
			theme: "bootstrap",
			width: "100%"
		});
	});
</script>