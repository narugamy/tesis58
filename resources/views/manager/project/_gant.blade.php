@extends('manager.layout.layout')
@section('styles')
	<link rel="stylesheet" href="{{ asset('plugins/codebase/dhtmlxgantt.css?v=6.1.4') }}">
	<link rel="stylesheet" href="{{ asset('plugins/codebase/common/controls_styles.css?v=6.1.4') }}">
	<link rel="stylesheet" href="{{ asset('plugins/codebase/skins/dhtmlxgantt_broadway.css') }}">
@endsection
@section('container')
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<a href="{{ route('manager.index') }}">Home</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<a href="{{ route('manager.project.index') }}">Proyectos</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<span>Gant</span>
				</li>
			</ul>
		</div>
		<h1 class="page-title"> {{ $title }}
		</h1>
		<div class="gantt_control">
			<button onclick="updateCriticalPath(this)">Mostrar ruta critica</button>
		</div>
		<div id="gantt_here" style='width:100%; height:600px;'></div>
	</div>
@endsection
@section('scripts')
	<script src="{{ asset('plugins/codebase/dhtmlxgantt.js?v=6.1.4') }}" type="text/javascript"></script>
	<script src="{{ asset('plugins/codebase/locale/locale_es.js?v=6.1.') }}" charset="utf-8" type="text/javascript"></script>
	<script src="{{ asset('plugins/codebase/ext/dhtmlxgantt_critical_path.js?v=6.1.4') }}" type="text/javascript"></script>
	<script type="text/javascript">
		function updateCriticalPath(toggle) {
			toggle.enabled = !toggle.enabled;
			if (toggle.enabled) {
				toggle.innerHTML = "Ocultar ruta critica";
				gantt.config.highlight_critical_path = true;
			} else {
				toggle.innerHTML = "Mostrar ruta critica";
				gantt.config.highlight_critical_path = false;
			}
			gantt.render();
		}
		let demo_tasks = {
			data: [@foreach($project->phases as $phase)@foreach($phase->activities as $activity){"id": "{{ $activity->code }}", "text": "{{$activity->code}}: {{ $activity->name }}", open: true, type: gantt.config.types.project},@foreach($activity->tasks as $task){"id": "{{$task->code}}", type: gantt.config.types.task, "text": "{{$task->code}}: {{$task->name}}", "start_date": "{{\Carbon\Carbon::parse($task->start_new)->format('d-m-Y')}}", "duration": "{{ $task->days }}", "progress": {{ empty($task->progress)? 0: ((float)$task->progress->progress/100) }}, "parent": "{{empty($task->task_id)? $activity->code:$task->task->code}}", open: true},@endforeach @endforeach @endforeach],
			links: [@foreach($project->phases as $phase)@foreach($phase->activities as $activity)@if($activity->activity_id){id: "{{$activity->code}}", source: "{{$activity->activity->code}}", target: "{{$activity->code}}", type: "0"},@endif @foreach($activity->tasks as $task)@if($task->task_id){id: "{{$task->code}}", source: "{{$task->task->code}}", target: "{{$task->code}}", type: "0"},@else{id: "{{$task->code}}", source: "{{$activity->code}}", target: "{{$task->code}}", type: "1"},@endif @endforeach @endforeach @endforeach]
		};
		gantt.config.columns = [ {name:"text", label:"Actividad / Tarea", width:"*", tree:true }, {name:"start_date", label:"Fecha inicio", align: "center" }, {name:"duration", label:"Duracion", align: "center" } ];
		gantt.config.work_time = true;
		gantt.config.details_on_create = false;
		gantt.config.scale_unit = "day";
		gantt.config.duration_unit = "day";
		gantt.config.autosize = true;
		gantt.config.drag_resize = false;
		gantt.config.drag_move = false;
		gantt.config.drag_links = false;
		gantt.config.drag_progress = false;
		gantt.config.details_on_dblclick = false;
		gantt.ignore_time = function(date){
			if(date.getDay() == 0 || date.getDay() == 6)
				return true;
		};
		gantt.init("gantt_here");
		gantt.templates.task_cell_class = function (task, date) {
			if (!gantt.isWorkTime(date))
				return "week_end";
			return "";
		};
		gantt.parse(demo_tasks);
	</script>
@endsection