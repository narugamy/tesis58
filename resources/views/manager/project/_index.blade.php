@extends('manager.layout.layout')
@section('styles')
	<link rel="stylesheet" href="{{ asset('plugins/select2/select2.min.css') }}">
	<link rel="stylesheet" href="{{ asset('plugins/select2/select2-bootstrap.min.css') }}">
	<style>
		tr td:last-child {
			display: grid;
			grid-template-columns: 1fr 1fr;
		}
	</style>
@endsection
@section('container')
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<a href="{{ route('manager.index') }}">Home</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<span>Proyectos</span>
				</li>
			</ul>
			<div class="page-toolbar">
				<div class="btn-group pull-right">
					<a data-url="{{ route('manager.project.create') }}" class="btn green btn-sm btn-outline btn-ajax"> Crear</a>
				</div>
			</div>
		</div>
		<h1 class="page-title"> {{ $title }}
		</h1>
		@if(session('success'))
			<div class="alert alert-success fade in">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				{{ session('success') }}
			</div>
		@elseif(session('improper'))
			<div class="alert alert-danger fade in">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				{{ session('improper') }}
			</div>
		@endif
		<div class="portlet green box">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-globe"></i>Lista
				</div>
				<div class="tools"></div>
			</div>
			<div class="portlet-body">
				<div style="margin-bottom: 6em;">
					<div class="col-md-4 col-sm-6">
						<div class="form-group form-md-line-input form-md-floating-label">
							<input type="date" class="form-control edited text-center" name="start_from" id="start_from" data-rule-required="true" required>
							<label for="start_from">Fecha inicio (Desde)</label>
						</div>
					</div>
					<div class="col-md-4 col-sm-6">
						<div class="form-group form-md-line-input form-md-floating-label">
							<input type="date" class="form-control edited text-center" name="start_to" id="start_to" data-rule-required="true" required>
							<label for="start_to">Fecha inicio (Hasta)</label>
						</div>
					</div>
					<div class="col-md-4 col-sm-12">
						<div class="form-group form-md-line-input form-md-floating-label">
							{{ Form::select('signaling', ['red' => 'Rojo', 'yellow' => 'Amarrillo', 'green' => 'Verde'], null, ['id' => 'signaling', 'required' => '', 'placeholder' => 'Seleccione una opcion', 'data-rule-required' => 'true', 'class' => 'form-control edited']) }}
							<label for="signaling">Semaforización</label>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="col-md-4 col-sm-6">
						<div class="form-group form-md-line-input form-md-floating-label">
							<input type="date" class="form-control edited text-center" name="end_from" id="end_from" data-rule-required="true" required>
							<label for="end_from">Fecha fin (Desde)</label>
						</div>
					</div>
					<div class="col-md-4 col-sm-6">
						<div class="form-group form-md-line-input form-md-floating-label">
							<input type="date" class="form-control edited text-center" name="end_to" id="end_to" data-rule-required="true" required>
							<label for="end_to">Fecha fin (Hasta)</label>
						</div>
					</div>
					<div class="col-md-4 col-sm-12">
						<div class="form-group form-md-line-input form-md-floating-label text-center">
							<button class="btn red btn-reload"> Limpiar
								<i class="fa fa-trash"></i>
							</button>
							<button class="btn green btn-datatable"> Buscar
								<i class="fa fa-check"></i>
							</button>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<table class="table table-striped table-bordered table-hover dt-responsive data-table" width="100%" id="sample_1">
					<thead>
					<tr>
						<th class="all order-now">N°</th>
						<th class="all">Codigo</th>
						<th>Nombre</th>
						<th>Fecha Inicio</th>
						<th>Fecha Fin</th>
						<th>Precio Base</th>
						<th>Precio Real</th>
						<th>CPI</th>
						<th>SPI</th>
						<th class="all acciones">Opciones</th>
					</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="ajax" role="dialog">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-body">
				</div>
			</div>
		</div>
	</div>
@endsection
@section('scripts')
	<script src="{{ asset('js/datatables.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('plugins/select2/select2.full.min.js') }}" type="text/javascript"></script>
	<script type="text/javascript">
		let datatable = $(document).find('.data-table').DataTable({
			"serverSide": true,
			"processing": true,
			"ajax": {
				"url": "{{ route('manager.project.table') }}",
				"type": "get",
				"data":function(data) {
					data.start_from = $("#start_from").val();
					data.start_to = $("#start_to").val();
					data.end_from = $("#end_from").val();
					data.end_to = $("#end_to").val();
					data.signaling = $("#signaling").val();
				}
			},
			"columns": [
				{data: 'DT_RowIndex', orderable: false, searchable: false},
				{data: 'code'},
				{data: 'name'},
				{data: 'start_new'},
				{data: 'end'},
				{data: 'price_base'},
				{data: 'price_real'},
				{data: 'cpi'},
				{data: 'spi'},
				{data: 'link'},
			],
			"aLengthMenu": [[5, 10, 15, -1], [5, 10, 15, 'All']], "language": {
				"buttons": {
					copyTitle: 'Copiado en porta papeles', copySuccess: {
						_: '%d lineas copiadas', 1: 'una linea copiada'
					}
				},
				"aria": {
					"sortAscending": ": Actilet para ordenar la columna de manera ascendente",
					"sortDescending": ": Actilet para ordenar la columna de manera descendente"
				},
				"infoFiltered": "(filtrado  de un total de _MAX_ registros)",
				"lengthMenu": `<span class="seperator"></span>Mostrar _MENU_ registros`,
				"sProcessing": "Procesando...",
				"info": `<span class="seperator"></span>Mostrando registros del _START_ al _END_`,
				"infoEmpty": "Mostrando registros del 0 al 0",
				"emptyTable": "Ningún dato disponible en esta tabla",
				"search": '<i class="fa fa-search"></i>',
				"paginate": {
					"previous": '<i class="fa fa-angle-left"></i>', "next": '<i class="fa fa-angle-right"></i>'
				},
				"zeroRecords": "No se encontraron resultados"
			}, "dom": "Blfrtip", buttons: [{
				extend: 'print',
				className: 'dt-button btn dark',
				text: `<i class="fa fa-print"></i>`,
				exportOptions: {columns: ':not(.acciones)'}
			}, {
				extend: 'copy',
				className: 'dt-button btn red',
				text: `<i class="fa fa-clipboard"></i>`,
				exportOptions: {columns: ':not(.acciones)'}
			}, {
				extend: 'pdf',
				className: 'dt-button btn blue',
				text: `<i class="fa fa-file-pdf-o"></i>`,
				defaultStyle: {alignment: "center"},
				exportOptions: {columns: ':not(.acciones)'},
				orientation: 'landscape',
				customize: function (doc) {
					let colCount = [];
					t.find("tbody tr:first-child td:not(.row-acction)").each(function () {
						if ($(this).attr('colspan')) {
							for (let i = 0; i < $(this).attr('colspan'); i++) {
								colCount.push('*');
							}
						} else {
							colCount.push('*');
						}
					});
					doc.styles.tableBodyOdd = {alignment: "center"};
					doc.styles.tableBodyEven = {alignment: "center"};
					doc.content[1].table.widths = '*';
				}
			}, {
				extend: 'excel',
				className: 'dt-button btn yellow',
				text: `<i class="fa fa-file-excel-o"></i>`,
				exportOptions: {columns: ":not(.acciones)"},
			}, {
				extend: 'colvis', className: 'dt-button btn purple', text: `<i class="fa fa-list-ul"></i>`
			}], responsive: true
		});
		$(document).on('click', '.btn-datatable', function () {
			datatable.draw();
		});
		$(document).on('click', '.btn-reload', function () {
			$('.edited').each(function (index, value) {
				$(this).val('');
			});
			datatable.draw();
		});
	</script>
@endsection