@extends('manager.layout.layout')
@section('styles')
	<link rel="stylesheet" href="{{ asset('plugins/select2/select2.min.css') }}">
	<link rel="stylesheet" href="{{ asset('plugins/select2/select2-bootstrap.min.css') }}">
@endsection
@section('container')
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<a href="{{ route('manager.index') }}">Home</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<a href="{{ route('manager.project.index') }}">Proyectos</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<span>Indice de desempeño del costo</span>
				</li>
			</ul>
		</div>
		<h1 class="page-title"> {{ $title }}
		</h1>
		@if(session('success'))
			<div class="alert alert-success fade in">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				{{ session('success') }}
			</div>
		@elseif(session('improper'))
			<div class="alert alert-danger fade in">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				{{ session('improper') }}
			</div>
		@endif
		<div class="portlet green box">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-globe"></i>Lista
				</div>
				<div class="tools"></div>
			</div>
			<div class="portlet-body">
				<table class="table table-striped table-bordered table-hover dt-responsive data-table" width="100%" id="sample_1">
					<thead>
					<tr>
						<th class="text-center all">Codigo</th>
						<th class="text-center">Fecha Inicio</th>
						<th class="text-center">Fecha Fin</th>
						<th class="text-center">Fecha de cierre</th>
						<th class="text-center">Progreso</th>
						{{--<th class="text-center">N° de dias realizados</th>
						<th class="text-center">N° de dias Panificados</th>--}}
						<th class="text-center">Costo Real</th>
						<th class="text-center">Valor Ganado</th>
						{{--<th class="text-center">Valor Planificado</th>--}}
						<th class="text-center all">Indice del Desempeño del costo</th>
						<th class="text-center all">Leyenda</th>
					</tr>
					</thead>
					<tbody>
					@foreach($tasks as $task)
						@if($task->counter)
						<tr>
							<td class="text-center">{{ $task->code }}</td>
							<td class="text-center">{{ \Carbon\Carbon::parse($task->start_new)->format('d/m/Y') }}</td>
							<td class="text-center">{{ \Carbon\Carbon::parse($task->end_new)->format('d/m/Y') }}</td>
							<td class="text-center">{{ ($task->approved)? \Carbon\Carbon::parse($task->approved)->format('d/m/Y'): '' }}</td>
							<td class="text-center">{{ round($task->progressive/100,2) }}</td>
							{{--<td class="text-center">{{ $task->diff_new }}</td>
							<td class="text-center">{{ $task->diff }}</td>--}}
							<td class="text-center">${{ round($task->ac,2) }}</td>
							<td class="text-center">${{ round($task->ev,2) }}</td>
							{{--<td class="text-center">{{ round($task->pv,2) }}</td>--}}
							<td class="text-center">{{ round($task->indice,2) }}</td>
							<td class="text-center">
								@if((float)round($task->indice,2) > 1)
									Por debajo del Costo planificado
								@elseif((float)round($task->indice,2) < 1)
									Por encima del costo planificado
								@else
									En el Costo planificado
								@endif
							</td>
						</tr>
						@endif
					@endforeach
					</tbody>
				</table>
				<p>Precio por dia : {{ $price_day }}</p>
				<p>Indice del desempeño del costo : {{ $mean }}</p>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="ajax" role="dialog">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-body">
				</div>
			</div>
		</div>
	</div>
@endsection
@section('scripts')
	<script src="{{ asset('js/datatables.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('plugins/select2/select2.full.min.js') }}" type="text/javascript"></script>
	<script type="text/javascript">
		let datatable = $(document).find('.data-table').DataTable({
			"aLengthMenu": [[10, 20, 30, -1], [10, 20, 30, 'All']], "language": {
				"buttons": {
					copyTitle: 'Copiado en porta papeles', copySuccess: {
						_: '%d lineas copiadas', 1: 'una linea copiada'
					}
				},
				"aria": {
					"sortAscending": ": Actilet para ordenar la columna de manera ascendente",
					"sortDescending": ": Actilet para ordenar la columna de manera descendente"
				},
				"infoFiltered": "(filtrado  de un total de _MAX_ registros)",
				"lengthMenu": `<span class="seperator"></span>Mostrar _MENU_ registros`,
				"sProcessing": "Procesando...",
				"info": `<span class="seperator"></span>Mostrando registros del _START_ al _END_`,
				"infoEmpty": "Mostrando registros del 0 al 0",
				"emptyTable": "Ningún dato disponible en esta tabla",
				"search": '<i class="fa fa-search"></i>',
				"paginate": {
					"previous": '<i class="fa fa-angle-left"></i>', "next": '<i class="fa fa-angle-right"></i>'
				},
				"zeroRecords": "No se encontraron resultados"
			}, "dom": "Blfrtip", buttons: [{
				extend: 'print',
				className: 'dt-button btn dark',
				text: `<i class="fa fa-print"></i>`,
				exportOptions: {columns: ':not(.acciones)'}
			}, {
				extend: 'copy',
				className: 'dt-button btn red',
				text: `<i class="fa fa-clipboard"></i>`,
				exportOptions: {columns: ':not(.acciones)'}
			}, {
				extend: 'pdf',
				className: 'dt-button btn blue',
				text: `<i class="fa fa-file-pdf-o"></i>`,
				defaultStyle: {alignment: "center"},
				exportOptions: {columns: ':not(.acciones)'},
				orientation: 'landscape',
				customize: function (doc) {
					let colCount = [];
					t.find("tbody tr:first-child td:not(.row-acction)").each(function () {
						if ($(this).attr('colspan')) {
							for (let i = 0; i < $(this).attr('colspan'); i++) {
								colCount.push('*');
							}
						} else {
							colCount.push('*');
						}
					});
					doc.styles.tableBodyOdd = {alignment: "center"};
					doc.styles.tableBodyEven = {alignment: "center"};
					doc.content[1].table.widths = '*';
				}
			}, {
				extend: 'excel',
				className: 'dt-button btn yellow',
				text: `<i class="fa fa-file-excel-o"></i>`,
				exportOptions: {columns: ":not(.acciones)"},
			}, {
				extend: 'colvis', className: 'dt-button btn purple', text: `<i class="fa fa-list-ul"></i>`
			}], responsive: true
		});
		$(document).on('click', '.btn-datatable', function () {
			datatable.draw();
		});
		$(document).on('click', '.btn-reload', function () {
			$('.edited').each(function (index, value) {
				$(this).val('');
			});
			datatable.draw();
		});
	</script>
@endsection