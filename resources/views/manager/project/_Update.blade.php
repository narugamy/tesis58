<div class="portlet light bordered wizzard portlet-form">
	<div class="portlet-body form">
		{{ Form::open(['route' => ['manager.project.update', $project->code], 'class' => 'form-horizontal form-modal', 'method' => 'put']) }}
		@include('manager.project._From_Update')
		{{ Form::close() }}
	</div>
</div>