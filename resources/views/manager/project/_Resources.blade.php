@extends('manager.layout.layout')
@section('styles')
	<link rel="stylesheet" href="{{ asset('plugins/chart/Chart.min.css') }}">
@endsection
@section('container')
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<a href="{{ route('manager.index') }}">Home</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<a href="{{ route('manager.project.index') }}">Proyectos</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<span>Recursos</span>
				</li>
			</ul>
		</div>
		<h1 class="page-title"> {{ $title }}</h1>
		<canvas id="bar-chart" width="800" height="200"></canvas>
	</div>
@endsection
@section('scripts')
	<script src="{{ asset('plugins/chart/Chart.bundle.min.js') }}" type="text/javascript"></script>
	<script type="text/javascript">
		const tasks = [@foreach ($project->phases as $phase) @foreach ($phase->activities as $activity) @foreach ($activity->tasks as $task) "{{ $task->name }}", @endforeach @endforeach @endforeach];
		const color = Chart.helpers.color;
		const ctx = document.getElementById("bar-chart").getContext('2d');
		const myBarChart = new Chart(ctx, {
			type: 'bar',
			data: {
				labels: tasks,
				datasets: [{
						label: "Precio de recursos por Tarea",
						backgroundColor: [@foreach($project->phases as $phase)@foreach ($phase->activities as $activity)@foreach ($activity->tasks as $task)"{{ $task->color['color_1'] }}", @endforeach @endforeach @endforeach],
						data: [@foreach($project->phases as $phase)@foreach($phase->activities as $activity)@foreach($activity->tasks as $task){{$task->money}}, @endforeach @endforeach @endforeach]
					},
					{
						label: "Cantidad de recursos por Tarea",
						backgroundColor: [@foreach($project->phases as $phase)@foreach ($phase->activities as $activity)@foreach ($activity->tasks as $task)"{{ $task->color['color_2'] }}", @endforeach @endforeach @endforeach],
						data: [@foreach($project->phases as $phase)@foreach($phase->activities as $activity)@foreach($activity->tasks as $task){{$task->count}}, @endforeach @endforeach @endforeach]
					}]
			},
			options: {
				elements: {
					rectangle: {
						borderWidth: 2,
					}
				},
				responsive: true,
				legend: {
					position: 'right',
				},
				title: {
					display: true,
					text: 'Chart.js Horizontal Bar Chart'
				}
			}
		});
	</script>
@endsection