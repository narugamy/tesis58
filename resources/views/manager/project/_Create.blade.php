<div class="portlet light bordered wizzard portlet-form">
	<div class="portlet-body form">
		{{ Form::open(['route' => 'manager.project.create', 'class' => 'form-horizontal form-modal']) }}
		@include('manager.project._From_Create')
		{{ Form::close() }}
	</div>
</div>