<div class="form-wizard">
	<div class="form-body">
		<ul class="nav nav-pills nav-justified steps">
			<li>
				<a href="#tab1" data-toggle="tab" class="step">
					<span class="number"> 1 </span>
					<span class="desc">
						<i class="fa fa-check"></i> Principal
					</span>
				</a>
			</li>
		</ul>
		<div id="bar" class="progress progress-striped" role="progressbar">
			<div class="progress-bar progress-bar-success active"></div>
		</div>
		<div class="tab-content portlet-body">
			<div class="tab-pane active" id="tab1">
				<div class="form-body">
					<div style="display: grid; grid-template-columns: 1fr 1fr;grid-gap: 1em;">
						<div class="form-group form-md-line-input form-md-floating-label">
							{{ Form::select('client_id', $clients, $project->client_id, ['id' => 'client_id', 'required' => '', 'placeholder' => 'Seleccionar una', 'data-rule-required' => 'true', 'class' => 'form-control edited']) }}
							<label for="client_id">Cliente</label>
						</div>
						<div class="form-group form-md-line-input form-md-floating-label">
							{{ Form::select('type_id', $types, $project->type_id, ['id' => 'type_id', 'required' => '', 'placeholder' => 'Seleccionar una', 'data-rule-required' => 'true', 'class' => 'form-control edited']) }}
							<label for="type_id">Tipo</label>
						</div>
					</div>
					<div class="form-group form-md-line-input form-md-floating-label">
						<input type="text" class="form-control edited" name="name" id="name" data-rule-required="true" required data-rule-minlength="3" data-rule-maxlength="191" minlength="3" maxlength="191" value="{{ $project->name }}">
						<label for="name">Nombre</label>
					</div>
					<div style="display: grid; grid-template-columns: 1fr 1fr;grid-gap: 1em;">
						<div class="form-group form-md-line-input form-md-floating-label">
							<input type="date" class="form-control edited" name="start" id="start" data-rule-required="true" required value="{{ $project->start }}">
							<label for="start">Fecha inicio</label>
						</div>
						<div class="form-group form-md-line-input form-md-floating-label">
							<input type="date" class="form-control edited" name="end" id="end" data-rule-required="true" required value="{{ $project->end }}">
							<label for="end">Fecha fin</label>
						</div>
					</div>
					<div class="form-group form-md-line-input form-md-floating-label">
						<input type="number" class="form-control edited" name="price_base" id="price_base" data-rule-required="true" required min="0" step="0.01" value="{{ $project->price_base }}">
						<label for="price_base">Precio Base</label>
					</div>
					<div class="form-group form-md-line-input form-md-floating-label">
						<textarea type="text" class="form-control edited" name="description" id="description" data-rule-required="true" required>{{ $project->description }}</textarea>
						<label for="description">Descripción</label>
					</div>
					<div class="form-group form-md-line-input form-md-floating-label text-center">
						<button class="btn green button-submit"> Actualizar
							<i class="fa fa-check"></i>
						</button>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function () {
		$(document).find("#resources").select2({
			theme: "bootstrap",
			width: "100%"
		});
	});
</script>