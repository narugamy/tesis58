@extends('manager.layout.layout')
@section('container')
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<a href="{{ route('manager.index') }}">Home</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<a href="{{ route('manager.project.index') }}">Proyectos</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<a href="{{ route('manager.phase.index', $activity->phase->project->code) }}">Fases</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<a href="{{ route('manager.activity.index', $activity->phase->code) }}">Actividad</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<span>Mensages</span>
				</li>
			</ul>
			<div class="page-toolbar">
				<div class="btn-group pull-right">
					<a data-url="{{ route('manager.activity.message.create', $activity->code) }}" class="btn green btn-sm btn-outline btn-ajax"> Crear mensaje</a>
				</div>
			</div>
		</div>
		<h1 class="page-title"> {{ $title }}
		</h1>
		@if(session('success'))
			<div class="alert alert-success fade in">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				{{ session('success') }}
			</div>
		@elseif(session('improper'))
			<div class="alert alert-danger fade in">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				{{ session('improper') }}
			</div>
		@endif
		@foreach($messages as $message)
		<div class="{{ ($message->user_id === $auth->id) ? 'col-sm-6':'col-sm-offset-6 col-sm-6' }} col-xs-12 margin-bottom-40">
			<div class="portlet {{ ($message->user_id === $auth->id) ? 'green':'red' }} box">
				<div class="portlet-title">
					<div class="caption">Usuario: {{ $message->user->full_name }}</div>
					<div class="tools"></div>
				</div>
				<div class="portlet-body">
					<h3>Titulo: {{ $message->name }}</h3>
					<p>Descripcion: {{ $message->description }}</p>
					@if($message->archive)
					<a href="{{ url($message->archive->route) }}" download="{{ $message->archive->name }}" class="btn btn-circle red-pink"><i class="fa fa-download"> {{ $message->archive->name }}</i></a>
					@endif
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		@endforeach
	</div>
	<div class="modal fade" id="ajax" role="dialog">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-body">
				</div>
			</div>
		</div>
	</div>
@endsection