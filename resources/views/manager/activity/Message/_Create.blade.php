<div class="portlet light bordered wizzard portlet-form">
	<div class="portlet-body form">
		{{ Form::open(['route' => ['manager.activity.message.create', $activity->code], 'class' => 'form-horizontal form-file']) }}
		@include('manager.activity.Message._From_Create')
		{{ Form::close() }}
	</div>
</div>