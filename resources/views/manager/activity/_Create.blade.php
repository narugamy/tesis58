<div class="portlet light bordered wizzard portlet-form">
	<div class="portlet-body form">
		{{ Form::open(['route' => ['manager.activity.create', $phase->code], 'class' => 'form-horizontal form-modal']) }}
		@include('manager.activity._From_Create')
		{{ Form::close() }}
	</div>
</div>