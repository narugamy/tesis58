<div class="portlet light bordered wizzard portlet-form">
	<div class="portlet-body form">
		{{ Form::open(['route' => ['manager.activity.update', $phase->code, $activity->code], 'class' => 'form-horizontal form-modal', 'method' => 'put']) }}
		@include('manager.activity._From_Update')
		{{ Form::close() }}
	</div>
</div>