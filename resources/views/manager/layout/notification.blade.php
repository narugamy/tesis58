<li>
	<ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">
		@foreach($notifications as $notification)
			<li>
				<a href="{{ $notification->route }}">
					<span class="time">{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $notification->created_at)->diffForHumans() }}</span>
					<span class="details">
						<span class="label label-sm label-icon label-success">
							<i class="fa fa-comment"></i>
						</span>
						{{ $notification->message->name }}
					</span>
				</a>
			</li>
		@endforeach
	</ul>
</li>