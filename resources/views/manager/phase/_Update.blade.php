<div class="portlet light bordered wizzard portlet-form">
	<div class="portlet-body form">
		{{ Form::open(['route' => ['manager.phase.update', $project->code, $phase->code], 'class' => 'form-horizontal form-modal', 'method' => 'put']) }}
		@include('manager.phase._From_Update')
		{{ Form::close() }}
	</div>
</div>