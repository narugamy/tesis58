<div class="portlet light bordered wizzard portlet-form">
	<div class="portlet-body form">
		{{ Form::open(['route' => ['manager.phase.create', $project->code], 'class' => 'form-horizontal form-modal']) }}
		@include('manager.phase._From_Create')
		{{ Form::close() }}
	</div>
</div>