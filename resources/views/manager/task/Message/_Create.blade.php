<div class="portlet light bordered wizzard portlet-form">
	<div class="portlet-body form">
		{{ Form::open(['route' => ['manager.task.message.create', $task->code], 'class' => 'form-horizontal form-file']) }}
		@include('manager.task.Message._From_Create')
		{{ Form::close() }}
	</div>
</div>