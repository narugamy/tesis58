@extends('manager.layout.layout')
@section('container')
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<a href="{{ route('manager.index') }}">Home</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<a href="{{ route('manager.project.index') }}">Proyectos</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<a href="{{ route('manager.phase.index',$phase->project->code, $phase->code ) }}">Fases</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<a href="{{ route('manager.activity.index',$phase->code) }}">Actividad</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<span>Actualizar</span>
				</li>
			</ul>
		</div>
		<!-- END PAGE BAR -->
		<!-- BEGIN PAGE TITLE-->
		<h1 class="page-title"> {{ $title }}
		</h1>
		<div class="form-wizzard">
			@if(session('success'))
				<div class="alert alert-success fade in">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					{{ session('success') }}
				</div>
			@elseif(session('error'))
				<div class="alert alert-danger fade in">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					{{ session('error') }}
				</div>
			@endif
			@if(count($errors) > 0)
				<div class="alert alert-danger fade in">
					<ul>
						@foreach($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif
			<div class="portlet light bordered wizzard portlet-form">
				<div class="portlet-body form">
					{{ Form::open(['route' => 'manager.phase.create', 'class' => 'form-horizontal form-submit']) }}
					@include('manager.task._From_Create')
					{{ Form::close() }}
				</div>
			</div>
		</div>
	</div>
@endsection