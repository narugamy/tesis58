<div class="portlet light bordered wizzard portlet-form">
	<div class="portlet-body form">
		{{ Form::open(['route' => ['manager.task.resource.update', $task->code, $container->id], 'class' => 'form-horizontal form-modal', 'method' => 'put']) }}
		@include('manager.task.resource._From_Update')
		{{ Form::close() }}
	</div>
</div>