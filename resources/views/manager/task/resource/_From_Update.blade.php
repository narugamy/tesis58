<div class="form-wizard">
	<div class="form-body">
		<ul class="nav nav-pills nav-justified steps">
			<li>
				<a href="#tab1" data-toggle="tab" class="step">
					<span class="number"> 1 </span>
					<span class="desc">
						<i class="fa fa-check"></i> Principal
					</span>
				</a>
			</li>
		</ul>
		<div id="bar" class="progress progress-striped" role="progressbar">
			<div class="progress-bar progress-bar-success active"></div>
		</div>
		<div class="tab-content portlet-body">
			<div class="tab-pane active" id="tab1">
				<div class="form-body">
					<div class="form-group form-md-line-input form-md-floating-label">
						{{ Form::select('resource_id', $resources, $container->resource_id, ['id' => 'resource_id', 'required' => '', 'placeholder' => 'Seleccionar una', 'data-rule-required' => 'true', 'class' => 'form-control edited']) }}
						<label for="resource_id">Recurso</label>
					</div>
					<div class="form-group form-md-line-input form-md-floating-label">
						<input type="number" class="form-control edited" name="number" id="number" data-rule-required="true" required min="0" step="0.01" value="{{ $container->number }}">
						<label for="number">Cantidad</label>
					</div>
					<div class="form-group form-md-line-input form-md-floating-label">
						<input type="number" class="form-control edited" name="price" id="price" data-rule-required="true" required min="0" step="0.01" value="{{ $container->price }}">
						<label for="price">Precio</label>
					</div>
					<div class="form-group form-md-line-input form-md-floating-label text-center">
						<button class="btn green button-submit"> Actualizar
							<i class="fa fa-check"></i>
						</button>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</div>