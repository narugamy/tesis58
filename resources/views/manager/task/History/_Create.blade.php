<div class="portlet light bordered wizzard portlet-form">
	<div class="portlet-body form">
		{{ Form::open(['route' => ['manager.task.history.create', $task->code], 'class' => 'form-horizontal form-modal']) }}
		@include('manager.task.History._From_Create')
		{{ Form::close() }}
	</div>
</div>