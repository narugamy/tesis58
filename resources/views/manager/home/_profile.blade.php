@extends('manager.layouts.layout')
@section('styles')
	<link href="{{ asset('css/app_admin.css') }}" rel="stylesheet" type="text/css"/>
@endsection
@section('container')
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<a href="{{ route('manager.index') }}">Home</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<span>Profile</span>
				</li>
			</ul>
		</div>
		<h1 class="page-title"> {{ $title }}
		</h1>
		<div class="row">
			<div class="col-md-12">
				<div class="">
					<div class="portlet light profile-sidebar-portlet">
						<div class="profile-usertitle">
							<div class="profile-usertitle-name"> Bienvenido(a): {{ strtoupper(Auth::user()->full_name()) }}</div>
							<div class="profile-usertitle-job"> Administrador del sistema</div>
						</div>
						<div class="profile-usermenu"></div>
					</div>
				</div>
				<!-- END PROFILE CONTENT -->
			</div>
		</div>
	</div>
@endsection