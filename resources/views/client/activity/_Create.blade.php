<div class="portlet light bordered wizzard portlet-form">
	<div class="portlet-body form">
		{{ Form::open(['route' => ['client.phase.create', $project->code], 'class' => 'form-horizontal form-modal']) }}
		@include('client.phase._From_Create')
		{{ Form::close() }}
	</div>
</div>