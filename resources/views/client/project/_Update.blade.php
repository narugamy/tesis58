<div class="portlet light bordered wizzard portlet-form">
	<div class="portlet-body form">
		{{ Form::open(['route' => ['client.project.update', $project->code], 'class' => 'form-horizontal form-modal', 'method' => 'put']) }}
		@include('client.project._From_Update')
		{{ Form::close() }}
	</div>
</div>